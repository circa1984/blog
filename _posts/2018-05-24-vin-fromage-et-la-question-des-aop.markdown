---
layout: post
title: Vin, fromage, et la question des AOP
date: '2018-05-24 14:34:00'
desc: Ces appellations ont parfois un demi-siècle. Parfois moins. Elles sont parfois modifiées, parfois jamais (ou pas encore). Mais que représentent-elles vraiment ?
---

Vous avez sans doute croisé des articles mentionnant cette pagaille dans l’AOP du Camembert de Normandie ces dernières semaines.

D’autant que depuis que de grands chefs ont lancé une pétition, le feu est encore plus intense. Une vraie bataille se livre à propos du lait cru, du terroir (en l’occurrence normand), du savoir-faire, de la prédation industrielle dans l’agriculture, et de tout un tas de vraies trucs, dont il est clair qu’il faut parler. Avec véhémence s’il le faut.

Depuis quelques mois, je tisse une relation de confiance avec mon confrère commerçant, qui officie dans une cave à vin juste à coté de ma fromagerie. Nous avons le même âge ou presque, et surtout la même vision des choses sur ces sujets. Lui via le vin, moi à travers le fromage, nous avons des perspectives et des envies pour nos métiers et leurs filières respectives. Nous avons des questions aussi, des avis bien tranchés sur ces discussions d’AOP, de prédation, de saccage, de pagaille.

Mais pour ma part (et je pense qu’il dirait la même chose) ce ne sont pas forcément des avis de corporation, ou de fédération. Ma vision des choses n’est en l’occurrence pas entièrement celle du fromager. En réfléchissant sur ces questions, j’ai essayé de voir plus loin que ce Camembert, même si je l’aime bien, quand il est au lait cru, et de Normandie.

* * *

### AOP et exigence

L’AOP, [Appellation d’Origine Protégée](https://www.inao.gouv.fr/Les-signes-officiels-de-la-qualite-et-de-l-origine-SIQO/Appellation-d-origine-protegee-Appellation-d-origine-controlee), héritière européanisée de l’AOC nationale, est un sceaux apposé sur un produit. Il garantit, en fonction de sa dénomination, un procédé de fabrication et une zone géographique dont la précision, la nomenclature et l’envergure reposent sur un cahier des charges rédigé préalablement par la représentation des producteurs concernés, et déposé auprès des autorités compétentes.

Je reprends : l’AOP c’est donc une étiquette qui nous dit que tel produit à été fabriqué de telle manière à tel endroit, et pas autrement. C’est les gens qui fabriquent le produit qui ont décidé de tout, et ils ont mis tous les détails dans un cahier, envoyé dans des bureaux de ministères et à l’UE. Quand ils veulent changer, ils discutent ensemble et envoient les mises à jour dans les mêmes bureaux.

Ces appellations ont parfois un demi-siècle. Parfois moins. Elles sont parfois modifiées, parfois jamais (ou pas encore). Elles sont aussi plus ou moins précises… Sur la zone géographique, ça va, on a toujours tracé des cartes précises, au fossé près. Sur la méthode de fabrication, c’est une autre histoire. Et lorsque certaines AOP peuvent aller jusqu’à mentionner l’altitude minimale de pâturage de races de vaches précises (et pas d’autres !), de telle à telle date, telle ou telle forme et taille de moule, ou je ne sais quel nombre incroyables de détails (hyper importants du reste), d’autres ne mentionnent même pas la race de vache locale, le traitement du lait (pasteurisé ? cru ? ce qu’on veut comme on veut ?).

Il faut pourtant bien saisir que plus un cahier des charges est précis, plus il demande de l’exigence. Une exigence qui ne peut être que valorisante et bénéfique à tout point de vue dans la chaîne : de l’écosystème de production (la richesse écologique du terroir, la qualité de vie du troupeau, celle de l’humain) au circuit de distribution et de vente.

Un fromage au lait cru, produit par une race autochtone en pleine santé, élevée en extensif, dans un écosystème riche de diversité, avec une vraie bonne recette, est obligatoirement une bombe gustative et gourmande. Sauf que ça, toutes les AOP ne le garantissent pas. Et on trouve des fromages sans label qui se débrouillent même carrément mieux.

* * *

### De la modernité même de l’AOP, des classements, et des labels

Du classement des Grands Crus en vin, datant de 1855, jusqu’à certaines AOP au cahier des charges vide de chez vide, la question de la modernité se pose grandement. C’est vrai, depuis 1855, on a peine vécu la révolution industrielle, puis l’arrivée de l’agriculture chimique, de la mondialisation… Sans compter les changements écologiques, géographiques, climatiques, et puis humains. Comment peut-on encore considérer le classement des Grands Crus de 1855 comme d’actualité ? Comment peut-on envisager sérieusement les prix que ce classement donne ? Un peu de sérieux…

Une vision moderne nous oblige à regarder en face l’état de l’agriculture, du commerce, de l’écologie, et l’ensemble de ce que je nomme souvent sur ce blog « les urgences » qui sont devant nous. Des urgences qui, comme je l’ai déjà développé par ailleurs, doivent être priorisées devant absolument toute autre chose. Elles ne se régleront que lorsque nous comprendrons, en tant que filières, confrères ou acteurs-trices de ces réseaux économiques, que notre façon de fonctionner est tellement archaïque qu’elle en est ridicule.

Je serai toujours le premier à défendre terroirs et savoir-faire, conservation patrimoniale, locale, spécifique. Et c’est pour cette raison précise qu’il me parait fou de s’embourber dans la protection de choses qui n’ont pas de sens. Il y a aujourd’hui, en vin notamment (et je suis persuadé que c’est également le cas en fromage), des humains derrière les produits qui ont compris ces enjeux et qui sont prêts à perdre leur label AOP ou IGP au profit d’une production plus moderne : vin naturel, bio, biodynamique, voire les trois. Ces transitions de mode de production dits (très maladroitement) conventionnels à d’autres (bio, biodynamiques, nature) sont extrêmement importants. Ils écoutent la terre, les écosystèmes sur lesquels ils se greffent, les cépages, et l’équilibre que tout cela donne ensemble avec maitrise et exigence. Il y a une grande vague de fraicheur à ce titre dans le vin, et c’est tant mieux.

* * *

### La réponse à tout : l’exigence locale

L’équivalent du vin naturel dans le fromage, c’est le lait cru. La sauvegarde, la promotion, et l’encouragement de la production de lait cru (et produits disons dérivés) devrait être un objectif primordial de politique agricole en ce qui concerne la filière du lait. Parce que le lait cru instaure d’emblée, dès la base, l’exigence minimale nécessaire sur l’ensemble de la chaine. Et il doit être accompagné d’autres critères, qui pour le coup, à mon humble avis de blogueur, n’ont rien à voir avec les critères plus ou moins vagues des AOP.

Des critères évidemment en lien avec le respect le plus absolu de l’écosystème de production :

- écologiquement : garantir et encourager la diversité végétale et animale des terres travaillées (ce qui exclut de fait les produits destructeurs, genre agents chimiques, etc)
- agriculturellement : garantir et encourager le bien être des troupeaux qui ne seraient composés que d’animaux autochtones et locaux (exclusion de fait de l’élevage intensif de races délocalisées)
- humainement : garantir et encourager le bien être de l’humain (prix fixé unilatéralement par la ferme, limitation de la taille des parcelles, …)

Les AOP, si elles ne se mettent pas à jour sur ces standards là, ne sont finalement rien d’autre que des faire-valoir commerciaux. Et c’est d’ailleurs super triste à écrire quand on est fromager. Mais je n’ai jamais trop changé d’avis sur la question. et lors de mon examen, à la question de la constitution d’une gamme pour une boutique, jamais la présence ou la mise en avant des AOP n’avait été un argument pour moi. Lorsqu’on m’a questionné sur ce point précis, j’ai répondu ce que je répondrais encore aujourd’hui : un autocollant AOP ne garantit pas obligatoirement les qualités gustatives, de production, de raisonnement agricole, que je classe prioritaires dans la confection de la gamme de ma boutique parfaite.

Mais rien n’est jamais parfait. Alors pour le moment nous avons des AOP, des IGP, et un Classement de Grands Crus. Ils défendent pour certains (beaucoup) des intérêts commerciaux d’avantage que des terroirs. Des milieux agricoles suspendus à la protection géographique. Rien de glorieux. Ne faisons pas juste semblant que c’est super, parce que ça ne l’est pas. Nous pourrions travailler pour avoir une agriculture plus locale et directe, y compris en fromage. Raisonnable, creusant moins la dette écologique, amplificatrice de la richesse des écosystèmes de nos terroirs…

A ce titre, il faut connaitre les AOP quali. Bien se renseigner sur le cahier des charges des produits que nous achetons est PRIMORDIAL. Comme regarder les ingrédients, ou bien le prix au kilo, etc. Que cela devienne un reflex de consommation. Qu’elle aussi, la consommation, devienne « nature », « bio-dynamique ».

* * *

### Le cas du Camembert alors, on en fait quoi ?

On le défend bec et ongles évidemment ! Parce que si personnellement je trouve cet AOP imparfaite, je préfèrerais presque (attention, provoc…) la voir disparaitre que la voir se faire rabaisser pour des enjeux commerciaux de grands groupes industriels.

Mon vœu ? Que l’on puisse produire du Camembert très haute qualité, au lait cru, avec un cahier des charges reprenant les considérations énumérées précédemment, partout dans le monde. Qu’on ne puisse l’acheter que localement, par des réseaux directs et courts tant par le nombre d’intermédiaires que de kilomètres. Que l’exigence de la production rejoigne les enjeux et les urgences actuelles.

Ce Camembert de Normandie ne doit pas accepter le sort qu’on veut lui réserver. Je ne suis pas certain du tout que ce soit en baissant l’exigence de sa production que nous répondons intelligemment à la demande (du marché ou des estomacs). Nous pouvons y répondre en faisant de sa production (et de celle des autres produits AOP par exemple) des cahiers des charges open-source, et géographiquement libres. Le Camembert ne pourrait être alors qu’un produit extrêmement qualitatif. Mais accessible partout sans acculer les producteurs d’une zone restreinte, et sans la prédation qui accompagne cela. Sans encourager l’intensif, Sans les pousser à bout, sans leur demander encore et toujours de produire plus plus plus. Ça ne sert à rien. Même économiquement, à terme c’est zéro.

La défense actuelle du Camembert de Normandie est essentiellement basée sur la ligne de l’identité territoriale, normande et même nationale. Sur la qualité de sa production au lait cru aussi. Bref, sur ce qu’il représente, symboliquement, de la filière. Un combat super noble, que je ne peux que rejoindre… Jusqu’à ce que les gens qui le portent s’arrêtent après la victoire, et que je ronge mon frein, avec mon camarade caviste, en pensant à ce que nous pourrions pousser plus loin. On y arrivera. Rome ne s’est pas faite en un jour (et les bufflonnes italiennes ne sont pas arrivées par magie européenne…).

_PS : aucune des photos de cet article ne montre un produit AOP !_

_NB : toutes les photos de cet article montrent des produits d’exception !_
