---
layout: post
title: Comme un flic qui crie ACAB
date: '2020-08-20 10:59:14'
desc: Ça fait un bail que d'instinct je me disais qu'il y avait une distance entre les grandes valeurs de notre Constitution, de notre République, et tout ces papiers officiels qui en forment la base, et la réalité du terrain. Eh ouais, l'Universalisme, l'égalité, la fraternité, c'est à la carte. La liberté n'en parlons pas.
---

C'est une période si particulière. Tout ce qui nous entoure a comme la tête à l'envers. Tous les grands concepts de notre société ont eux aussi la tête à l'envers. On dirait l'ante-Grand-Soir, un Grand Soir à l'envers, une ante-fête, où on découvre avec des palpitations que ce en quoi en croyait n'est pas croyable, pas en l'état, et certainement pas pour tout le monde.

Ça fait un bail que d'instinct je me disais qu'il y avait une distance entre les grandes valeurs de notre Constitution, de notre République, et tout ces papiers officiels qui en forment la base, et la réalité du terrain. Eh ouais, l'Universalisme, l'égalité, la fraternité, c'est à la carte. La liberté n'en parlons pas.

### DIS LES TERMES

Faudrait déjà savoir ce qu'on entend pas là. Notamment avec la liberté et l'universalisme, qui me paraissent être les plus importants dans notre paradigme de société actuel.  
L'Universalisme, c'est la vocation universelle d'une chose : un concept, un droit, un devoir qui prendrait une dimension d'universalité. Ce désormais fameux "pour tous".  
La Liberté, c'est déjà plus galère. Parce que chez les libéraux, la liberté est une notion qui renvoit à une certaine forme de propriété, avec l'adage fantastique "la liberté des uns s'arrête où commence celle des autres". La liberté, c'est un terrain, avec des bordures, des haies, et sur lequel chacun pourrait donc faire ce qu'il veut vu que c'est à lui (il ne manque en fait que le titre de propriété et le règlement des contentieux sur la liberté d'expression devant notaire ahah). Or, pour une autre partie de la vision politique, il existe un paradigme où la liberté est un lien. Un lien entre les membres d'une société, et qui, à mesure qu'il se renforce et se tisse, participe à l'épanouissement et l'émancipation de chacun et de tous. En gros, la liberté est commune, comme une ressource dont tout le monde jouit gratuitement, et qu'il convient de préserver pour les autres autant que pour soi. Dans l'anarchisme, et chez les libertaires, la liberté est proche d'un moyen (une force même), donc, plus que d'une chose (une propriété).

Dans notre société libérale à donf, autant dire qu'il y a une vraie variation entre l'idéal constitutionnel des droits qui forment ce qui est censé défendre la liberté et l'égalité, devant la réalité de la société où le patrimoine en liberté (et en égalité) n'est pas le même pour tous... Que penser alors de l'universalité de ces choses ? Et que penser donc de l'Universalisme ?

### ACAB ACAB ACAB

Depuis quelques mois, les communautés racisés montent au créneau, en critiquant justement ce gouffre entre l'idéal constitutionnel de liberté et d'universalité, et la réalité de leur terrain, de leurs vies. Clairement, on peut se poser la question au regard de nombreux rapports du Défenseur des Droits ou d'études sociologiques, de comment et pourquoi les racisés soumis à la même Constitution Française, à la même fierté de l'idéal Universaliste, n'en vive pas la même pratique que nous, les blancs, sans aucun doute moins enclins aux préjudices du racisme systémique.

Au milieu de ces questions, je me sens comme un keuf qui crierait ACAB, complètement déchiré entre mes idéaux et la réalité de celles et ceux qui font société avec moi, autour de moi.

Je n'ai pas d'idéal libéral. J'ai un idéal de liberté qui colle davantage au paradigme libertaire et anarchiste, où je m'épanouie parce que je sais que l'autre s'épanouie aussi. Clairement, et depuis gamin, je ne suis jamais aussi content que quand les autres, autour de moi, le sont aussi. Ca m'a valu des étiquettes de naïf, de type qui se faisait arnaquer, parce que je donnais et donne toujours beaucoup, mais moi, ca m'a toujours plu, et fonctionner autrement n'est pas DU TOUT un mode opératoire qui me convient (pour avoir essayé...).

Alors bien sûr, l'idéal Universaliste de notre Constitution me parait indéboulonnable (lui...), mais je pense qu'en tant que société, nous devrions considérer que ces choses là ne sont jamais acquises. Jamais. Il y a toujours quelque part un _endroit social_ où elles ne font pas loi, elles ne sont pas activées, elles ne sont pas réelles. Ces endroits là, il est du rôle de chacun, et particulièrement des militants politiques, de faire en sorte de les raccrocher pour de vrai, au même seuil de réalité que les autres. Une société où tout le monde ne vit pas la même réalité est par essence une société anti-universaliste, liberticide et inégalitaire. C'est donc clairement une société injuste et violente.

Du coup, autant se le dire franchement, entre nous... S'indigner que celles et ceux qui subissent les injustices et les violences en première ligne ne veuillent plus entendre parler de cet Universalisme dont tout le reste jouit avec gargarisme, c'est quand même un sacré caprice non ?
