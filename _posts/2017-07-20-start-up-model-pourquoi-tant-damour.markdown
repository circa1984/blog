---
layout: post
title: Start-up model, pourquoi tant d’amour ?
date: '2017-07-20 10:42:00'
desc: Je suis absolument fasciné par cette démesure autour des start-ups, tant ça me parait absurde de passion et story-tellings foireux.
---

Je suis absolument fasciné par cette démesure autour des start-ups.

Cet amour soudain, si passionné et vif qu’on dirait le pitch d’une mauvaise série pour ado. Servie sur plateau d’argent par notre président et son gouvernement, assaisonné par nos médias (dé)favoris, le start-up model relève du magique, du miracle, de la panacée si on les écoute.

D’autant qu’ils s’accompagnent un peu partout, et notamment sur Médium, de story-telling d’entrepreneurs. Ils y vendent leurs histoires, partagent leurs doutes, et surtout confient leur fabuleux destin auquel ce choix incroyable de « suivre sa voix intérieure » les a mené.

Bon OK, j’écris sur un ton un peu moqueur mais je n’ai pas mieux de mon coté : j’ai été à leur place et le suis encore aujourd’hui. Je monte des trucs, je lance des aventures. Je ne peux donc pas les blâmer ! Mais je ne peux pas non plus m’empêcher de parler du modèle choisi…

* * *

### Start-up, les Bisounours en enfer

C’est très bizarre, parce que si on écoute les grands médias et la sphère politique, on dirait que le monde de la start-up est uniformément génial. Tous les projets y sont cools, les gens aussi. Propres sur eux, un peu à la marge, _cocky_ mais sympas. Et puis cette adrénaline, ce truc satisfaisant d’être fondateur, d’assumer les étapes initiales d’un projet, de risquer. _Woah, amazing_. La Silicon Valley a l’air d’être un paradis terrestre où tout brille de 1000 feux !

Mais bon, je suis sûr que je ne suis pas le seul hein ? À me dire que son épanouissement au travail doit quand même pas mal dépendre de ses collègues (avoir un single-speed ne rend pas forcément sympa), de son boss, de l’organisation des rapports hiérarchiques, de la proportionnalité entre responsabilités / charge de travail / salaire, etc etc. Normal quoi.

Parce que finalement, start-up, c’est juste un nom qui signifie qu’une boite démarre. Ça sous-entend aussi un peu qu’il peut s’agir d’une innovation… Menfin toutes les innovations ne mènent pas forcément au succès, encore moins à un succès d’intérêt général ou collectif…

Récemment, on commence même à voir des articles apparaître, qui modèrent les descriptions Bisounours… Des articles qui sondent plus en profondeur cette nouvelle économie du gig, du contrat au projet comme dirait l’autre, de la pige, de l’éphémère. C’est révélateur de trois choses :

- la propagande ne fonctionne qu’un temps et le concret finit toujours par éclore
- le choix de son modèle d’entreprise n’a jamais été aussi décisif qu’aujourd’hui
- les innovations subissent aussi le syndrome de Stockholm

Cette période charnière que nous vivons en ce moment concrétise tout cela. Elle rend plus explicite et visible les défauts de notre société, et permet de fait, si on le veut un peu, de se projeter dans des modèles plus constructifs et moins parasitaires.

* * *

### Les mauvaises habitudes

Alors que je préparais cet article depuis quelques temps, hier soir un contact Facebook a posté un article sur mon mur, exposant l’exemple parfait de ce dont je voulais parler. Je ne nommerais pas l’entreprise dont il était question, mais il s’agissait de cultures de fruits en milieu urbain, initialement sur Paris, avec un business model de start-up rincé par des levées de fonds. J’adore le concept, j’adore sa faisabilité et ce qu’il ouvre comme champ des possibles.

Mais pourquoi un business model capitaliste et libéral ? Pourquoi vouloir changer la société de consommation en reprenant les modèles économiques qui la construisent, la forgent et la renforcent continuellement, au lieu de s’inscrire dans la création d’une société de contribution ? Je ne trouve pas vraiment de réponse. Si ce n’est que l’esprit Silicon Valley est envahissant au possible, et qu’il est de plus en plus dur de conserver son quant-à-soi là aussi. J’ai encore moins de réponse lorsque l’étape globalisation du projet se met en route. L’incompatibilité du capitalisme global avec les innovations d’intérêt public est-elle à démontrer ? Est-ce que l’exemple de l’industrie pharmaceutique ne suffit pas ?

Je suis ultra-pour ces initiatives alternatives de production alimentaire en milieu urbain, j’ai d’ailleurs postulé moi-même dans une de ces start-ups « locavoristes »… Mais je redoute toujours leur recherche de profitabilité financière. Et surtout, leur désir d’expansion internationale… Internet possède à ce titre une guirlande d’histoires qui pourraient servir de leçons si nous savions les tirer et les comprendre plus vite.

* * *

### Privilégier les modèles où le gain privé n’est pas le but principal

Si ça ne tenait qu’à moi, les innovations autour de l’écologie, de l’alimentation, de la santé ou encore de l’éducation (domaines qui à mon sens font parti du bien collectif et relèvent uniquement de l’intérêt général) devraient être conçues et montées sur des modèles à but non-lucratif. Comme pour les monnaies locales administrées par des associations comme j’en ai parlé [dans cet article](https://circa1984.net/echelle-locale-et-monnaie-locale/).

Parce que souvent ces initiatives et ces innovations ont un pouvoir de changement suffisamment élevé pour se refuser à une absorption certaine dans la société de consommation. Elles ouvrent les portes d’un changement ou de glissement vers une société inclusive et contributive. Cette capacité révolutionnaire peut permettre d’imaginer une durabilité du projet, y compris économiquement, sans avoir obligatoirement à le « vendre ».

C’est une histoire de paradigme général et de projection dans un monde ou un autre. Et malheureusement, je crois que dès lors que les projections impliquent le gain individuel et l’internationalisation, le risque d’y perdre la petite révolution possible à la base est immense.

Je ne vois (pour le moment ? éclairez moi !) que deux solutions, en théorie :

- Soit on se base sur un statut non-lucratif, associatif
- Soit on refuse la tentation de dépasser la sphère locale, ce qui signifie que nous optons pour un modèle de petite entreprise spécifique à un territoire

Et, chose fabuleuse, aucun de ces deux modèles n’interdit de concevoir son projet de façon _open-source_. Une manière d’aborder la collaboration internationale beaucoup plus saine et de déjouer les prédations et les colonisations.

* * *

### Si entreprendre c’est l’avenir, les modèles « open », contributifs et inclusifs sont le futur.

A mon humble avis, entreprendre, quelque soit la forme choisie, est la meilleure façon aujourd’hui de modéliser la société de demain, via l’innovation et la preuve par l’action. La start-up, le making, le problem-hacking, sont des démarches souvent porteuses de solutions à des problèmes de société urgents. L’écologie et la consommation alimentaire en sont des domaines super parlants.

Mais si nous voulons mettre ces innovations au service d’un changement de paradigme, les diriger vers une société inclusive, équitable, raisonnée, bienveillante, alors nous ne pouvons pas les concevoir avec les outils qui font la société d’aujourd’hui. Et ici encore, je ne peux pas me résoudre à oublier l’échelle globale, qui nous cause tant de torts. Et je ne peux donc pas me imaginer non plus que ces initiatives fructueuses finissent par adopter des modèles économiques qui finissent toujours par devenir, d’une façon ou d’une autre, parasitaires, prédateurs, oppressants.

Je développe en ce moment quelques projets liés à ce blog et aux sujets que j’y aborde. Et je m’attache, grâce aux savants conseils prodigués avec bienveillance par celles et ceux qui les suivent autour de moi, à ne pas m’engager dans ces business models là. J’essaie de garder à l’esprit que ce sera mieux autrement. Ça ne peut que l’être. Et je crois que le futur, y compris le mien, tient aussi à ce genre de détails.

Comme je dis souvent, on verra bien.
