---
layout: post
title: Où est passée la responsabilité ?
date: '2017-10-13 11:02:00'
desc: Je ne sais pas vous, mais je trouve que depuis de longues décennies, quelque chose en rapport avec le fondement même de la Justice a disparu de la circulation. Impossible de trouver, nulle-part, dans un sens où un autre, en bien ou en mal, une once de responsabilité.
---

Je ne sais pas vous, mais je trouve que depuis de longues décennies, quelque chose en rapport avec le fondement même de la Justice a disparu de la circulation.

Impossible de trouver, nulle-part, dans un sens où un autre, en bien ou en mal, une once de responsabilité.

Les faits marquants à ce sujet s’enchaînent comme si c’était un collier de perles. Au delà même des grandes abstractions de nos institutions (le casier vierge pour les élu-e-s est un bien bel exemple), il y a une ambiance de « foutu pour foutu » avec la responsabilité. Pourtant, les constats, les tragédies, les urgences et les nécessités s’accumulent à vitesse grand V… Où est le dièse ?

* * *

### Partout, la responsabilité est un trou noir

Quelque soit l’endroit de la société où on pointé son regard, on ne trouvera pas de responsabilités assumées. Les conséquences catastrophiques de décisions unilatérales ne trouvent jamais aucun coupable reconnu responsable explicitement. Je pense aux bavures policières en manifestation, où comme par hasard on ne remonte jamais la chaîne de commandement. Je pense aux politiques urbanistiques des grandes villes, qui ont donné les ghettos que nous connaissons, entre autres. Je pense aux publicités éhontément racistes dont aucun des créateurs ou validateurs ne verra jamais un tribunal. Je pense aussi aux guerres d’ingérence et aux comportement belliqueux de nombre de nos ministres. Je pense à bien trop d’exemples pour un seul blog…

Et de l’autre coté de la médaille, les réussites, les vraies, ne trouvent que trop rarement les lauriers pour celles et ceux qui en sont à l’origine. Et là je pense à tous les lanceurs d’alerte dont le choix a mené à des actions concrètes, avec des conclusions d’intérêt général. Mais je pense aussi à celles et ceux qui n’ont pas eu la même chance et qui sont toujours piégés par (a minima) cette phobie organisée de la responsabilité.

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/ruooxBTydtE?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

Une phobie qui agis comme un trou noir et qui a contaminé l’ensemble de la société. A tel point que nous non plus nous n’adressons plus cette question comme nous devrions le faire. Nous en avons perdu l’usage, l’habitude, le reflex. En tant que groupe de citoyens, nous ne questionnons même plus la responsabilité de quelque chose d’aussi abscon que notre comportement électoral : quelles sont les conséquences réelles de mon vote ? Aujourd’hui, demain, à la fin du mandat. Qu’est-ce que mon vote a validé d’avance ? Trou noir…

### Le local pour cultiver ce reflex de responsabilité

Construire à l’échelle locale de l’action collective et individuelle, c’est retrouver le goût de la responsabilité : celui qui transcende l’idée pour la rendre concrète, et celui qui évalue la part de chacun dans l’échec ou la réussite et leurs conséquences.

Il s’agit donc une fois encore d’investir une échelle où il est possible facilement d’avoir les tenants et aboutissants des situations. Et de fait pouvoir suivre la chaîne de conception et d’action, et reconnaître à qui reviennent les victoires ou les échecs. Il s’agit une fois encore de réappropriation : celle de la contribution collective, du risque, des conséquences.

Parce qu’en faisant l’expérience des conséquences, nous apprenons immédiatement à en éviter le négatif, individuellement et collectivement. Il est impossible de s’en détourner. Il n’y aucune institution, aucun système, aucune administration qui opacifie le processus. Tout est transparent et lisible. Tout est récoltable et transmissible comme leçon pour la suite.

* * *

### Avec la responsabilité vient la justice

La Justice, sociale, pénale, civile, ne peut être équilibrée et forte que lorsque le concept de responsabilité est acquis de chacun et tous. Le consensus collectif sur ce que la responsabilité représente pour le groupe et l’individu est un garant de l’équité, de la justesse, et de la justice.

Un peu comme si nous voulions mieux respirer sans planter quelques arbres, bâtir un avenir plus juste sans se réapproprier la responsabilité me semble vain. Me semble aussi perdu d’avance l’idée de le faire à grande échelle : [croire en la constitution d’autorités globales pour réguler quoi que ce soit…](https://m.facebook.com/story.php?story_fbid=487195571652434&id=100010859856042) Modèle obsolète, inadapté aux urgences sociales, écologiques, économiques actuelles. Particulièrement en matière de justice et d’équité.

Aujourd’hui symptomatique du manque de Justice dans nos sociétés, il est en réalité à placer AVANT dans la chaîne. La responsabilité est une condition, inéluctable, commune, indispensable, de la vie dans une société équilibrée et juste. Un commun qu’il faut semer à nouveau, partout. Parce qu’avec lui viennent des questions très simples, mais très graves, que l’on peut résumer simplement ainsi : l’état actuel des choses ici et ailleurs, est-il vraiment un hasard ou un dessein ?

_Pas beaucoup de photos dans cet article… c’est pour éviter de mettre celles des ministres que je juge coupables, vous les connaissez j’espère. ;)_
