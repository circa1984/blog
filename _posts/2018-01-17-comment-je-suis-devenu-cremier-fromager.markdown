---
layout: post
title: Comment je suis devenu crémier-fromager
date: '2018-01-17 12:25:00'
desc: Clairement, il ne s’agit pas d’une vocation de gamin, et encore moins de cette passion qui m’aurait poursuivie toute ma vie avant que je décide enfin de l’assouvir, après cette crise de conscience « typique » des trentenaires… Parlons reconversion.
---

Clairement, il ne s’agit pas d’une vocation de gamin, et encore moins de cette passion qui m’aurait poursuivie toute ma vie avant que je décide enfin de l’assouvir, après cette crise de conscience « typique » des trentenaires…

Il ne s’agit finalement que d’une reconversion chanceuse. Rien de plus, rien de romantique, rien d’émotionnel. Je suis devenu fromager parce que j’avais besoin d’un salaire fixe et d’une stabilité pro. Par contre, je n’imaginais pas 10% des choses que j’allais apprendre, sur les produits, la filière, l’écologie, les animaux, la biologie, les gens, moi-même.

* * *

### **Toi aussi, deviens fromager du jour au lendemain**

Avant je bossais dans la culture, la musique. Une sorte de couteau-suisse qui oscillait entre produire des morceaux, réaliser des disques, des clips, manager des projets, ou encore conseiller des gens ou des labels.

Ça oui, c’est romantique. C’était passionnel, fusionnel, mais pas un kopeck stable. Encore moins serein. Encore moins pérenne. Et arrive ce moment dans la vie où on a besoin de l’inverse. Parce que si je n’ai jamais été à la recherche du confort, je ne pouvais pas non plus mettre en danger celui des miens. (Et puis parallèlement, l’ensemble de l’industrie commençait sérieusement à m’user…)

Alors j’ai d’abord fait des petits jobs, intéressants surtout humainement. S’occuper des handicapés dans leur correspondance en Gare de Lyon, c’était top. Apprendre de ses collègues, et écouter les histoires des gens, en racontant aussi les siennes. Ma misanthropie en a pris un coup. Tant mieux.

Et puis la boite en question m’a refourgué un poste bien trop opposé à mes valeurs et ma vision… Alors en deux semaines, je suis devenu fromager. Le temps de trouver l’organisme formateur sur internet au taf (là on est le 25 octobre 2015), d’imprimer un CV sur l’imprimante du taf, de quitter le taf plus tôt pour aller le déposer, de rompre ma période d’essai, et d’attaquer (le 2 Novembre) en fromagerie. Ça s’est vraiment passé comme ça. Promis.

* * *

### **Des bancs d’école plus cools que je pensais**

Je bossais à Paris et faisais l’aller-retour tous les jours, du mardi au dimanche. Quand je n’avais pas école le lundi… C’était clairement éreintant, d’autant que le métier sait être physique et intense.

Mais je découvrais vraiment une sorte de nouvelle vie. Les gens de la musique et de la culture avait fini de me déprimer, j’avais du mal à garder mon sang-froid et à comprendre où cette culture et cette industrie allait. Là, avec cette formation, les choses étaient limpides et logiques. Et j’ai eu la chance de tomber sur une équipe formatrice top, tant dans ma boutique rue Mouffetard avec mon responsable et ses seconds, qu’à l’école IFOPCA avec les profs. Et je ne mentionne même pas mes camarades de classe : la crème de la crème.

Je me suis extrêmement vite pris aux jeux : celui de la vente et celui de l’apprentissage. La vente me permet encore aujourd’hui de reconnecter avec un réel qui n’existe presque plus dans la musique. C’est d’autant plus épanouissant quand on a en face de soi des gens gourmands et intéressés. Et puis ce que j’apprenais à l’école était dingue. Je n’ai jamais été autant passionné par la géographie, la biologie, et investi dans la cause animale que depuis ma reconversion.

* * *

### **Nouveau terrain d’études (On se refait pas !)**

Quand j’étais dans la musique et la culture, je ne pouvais pas me retenir d’analyser ce qui fonctionnait et ce qui ne fonctionnait pas, et le pourquoi du comment. J’avais rédigé des synthèses expliquant ma vision anti-subvention, offert mes services aux labels en besoin de conseils et de vision… C’était clairement hyper intéressant.

Je n’ai pas été déçu par la filière fromagère et laitière, qui elle aussi est très prenante pour qui veut l’analyser. En apprenant les AOP, les races, les typicités, les terroirs, j’ai pas juste appris à vendre du fromage. J’ai aussi appris pourquoi le fromage en lui-même existait. Comment il était arrivé là, quand et pourquoi tel ou tel format. Comment et pourquoi telle méthode de fabrication a été employée ici et pas là bas, etc. Captivant.

C’est évidemment en m’intéressant de près à ces questions que j’ai réfléchi au locavorisme, à l’échelle locale, et que j’ai pu construire une critique qui me semble bienveillante et positive à l’égard notamment du veganisme.

C’est aussi avec cette formation que j’ai enfin pu intégrer le monde des buveurs de bière et de vin. Je ne buvais quasiment pas d’alcool avant de commencer cette reconversion. Avec cours d’accords mets-fromages et vins-fromages, j’ai pu gouter et parler de choses grandioses. Et bon… Il m’est toujours impossible de toucher à une Heineken ou un vieux cubis de rouge, je rechigne plus du tout à m’enfiler une bonne bière ou un bon vin.

J’ai étendu la palette de mes gouts, découvert plein d’arômes fous, et j’ai continué d’apprendre gustativement. J’ai aussi appris du vocabulaire, des mots, et les gestes et outils du métier. Tout ça fait que je pratique aujourd’hui un métier qui me plait à fond. Dont certains aspects me fascinent et me passionnent.

Et à ce titre, sur les conseils de la personne la plus sage du monde (ma femme), je tiens un compte Instagram dédié à tout cela. Fromages, accords, et autres. C’est par là que ca se passe : [FROMAGEFROMAGES](https://www.instagram.com/fromagefromages/)

* * *

### **Du vrai sens du terme « parcours professionnel »**

MAIS BON… ne vous y méprenez pas, je ne suis pas non plus à fond sur le fromage en permanence. Bien au contraire. Hors les murs du magasin, ma vie n’a rien à voir avec tout cela. Le sport, ma nouvelle PlayStation (merci ma femme !), nos amis, le cinéma, ça ce sont mes vrais hobbys. Quant à mes passions, celles qui vraiment m’agitent, ce qui est écrit dans les articles de ce blog en dit long. Et ça suffit amplement.

Alors s’il vous plait, n’imaginez pas cette reconversion comme romantique et passionnée. Elle fut pragmatique et chanceuse. N’imaginez pas non plus que je ne pense qu’au fromage, et que je suis comblé de joie quand on m’envoie pour la 74ème fois de la journée (j’exagère à peine) cet article qui parle du voleur de St Nectaire, ou celui qui dit que le fromage est une drogue, etc etc… Je vous l’assure, on a clairement des tas d’autres choses à discuter, et rien ne me fait plus plaisir que de recevoir un article en lien avec les thématiques « de fond » abordées sur ce blog. (Mais on peut carrément discuter de tout cela autour d’un bon plateau hein…)

Cette reconversion est un vrai _step up_ dans ma carrière pro. Je comprends maintenant tout ce concept de parcours professionnel. Et si je me dis que je ne finirais peut-être pas fromager dans ma vie, l’idée d’avoir, en deux ans seulement, découvert tant de savoirs et de savoir-faire ne me stresse pas quant à la perspective d’y faire une décennie. On verra bien… Tout peut aller très vite.
