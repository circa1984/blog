---
layout: post
title: La Diversité comme clef de voûte, part 1
date: '2017-02-08 18:04:00'
desc: Comme j’essaie surtout sur ce blog de rendre en mots la réalité la plus franche et naturelle de ce que je pense, j’ai finalement choisi l’image de la clef de voûte pour parler de la diversité.  
---

« De la diversité », « Manifeste de la diversité », « Diversité j’écris ton nom », « Éloge de la diversité »…

Tous les titres foireux me sont passés par la tête pour cette série d’articles… Mais comme j’essaie surtout sur ce blog de rendre en mots la réalité la plus franche et naturelle de ce que je pense, j’ai finalement choisi l’image de la clef de voûte pour parler de la diversité.  
Parce que c’est effectivement elle qui explique toute l’articulation de mes engagements (ne serait-ce qu’affectifs, quand ils ne sont pas -encore ?- convertis ou convertibles en action). J’ai mis du temps à le comprendre, il m’a fallut une série de déclics survenus ces derniers mois… Avant cela, c’était un peu plus froid, on me disait d’ailleurs souvent que mes « analyses » manquaient de poésie, de fantasmes…

Eh bien elle est là, toute la poésie. Et bizarrement, cette diversité n’est pas celle qu’on pourrait imaginer… Je vais donc essayer de décrire les enjeux qui me tiennent à cœur et qui y sont liés, dans le sens des causes ou dans celui des conséquences. Et dans ce premier article, je vais commencer par la diversité des écosystèmes, biologique, végétale, animalière, de la planète. La [_biodiversité_](https://fr.wikipedia.org/wiki/Biodiversit%C3%A9).

* * *

### Un documentaire déclic : Racing Extinction

J’en parle à loisir à celles et ceux qui cherchent quelque chose de bien à regarder, et même aux autres. Tant ce docu m’a frappé par sa forme, son ton, sa justesse, et son absence de compromission ou de paradoxe (ce que je pourrais reprocher à celui de Di Caprio – [Before The Flood](https://www.youtube.com/watch?v=KY9iqeiyyBM) – où il m’a donné l’impression d’aller faire la morale au monde entier…). Il m’a aussi frappé par son constat sans appel, et la sensation d’urgence qu’il a provoqué dans ma tête. je n’aurais sans doute pas ouvert ce blog si je ne l’avais pas vu, puisque ce blog me sert de journal dans mes démarches personnelles (Google, locavore, etc).

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/MwxyrLUdcss?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

Ce documentaire a accéléré très fortement mes réflexions sur la politique, les échelles d’actions, les comportements sociaux vis à vis de l’alimentation, de l’éducation, de la transmission, et bien d’autres trucs. Et il m’a fournit la réalité d’une notion qui aujourd’hui est ma clef de voûte : la diversité.

Dans ce documentaire, il est statué que d’ici un tout petit nombre de décennies, la planète ne pourra plus supporter la vie, tant la biodiversité globale se sera éteinte… Et demandant donc de comprendre implicitement que non seulement nous DEVONS arrêter de pousser les espèces à leur extinction, mais nous DEVONS ÉGALEMENT  « réactiver » la biodiversité et en encourager le fonctionnement. Et par DEVOIR, il est entendu non pas une morale, mais une nécessité de survie en tant qu’espèce : nous pourrons avoir accumulé autant de richesses que nous le voulons, notre aventure s’arrêtera là, avec tout le reste, comme tout le reste.

* * *

### « Solutions locales pour désordre global »

C’est le nom d’un autre documentaire, qui avait donné du grain à moudre à ma vision « locale » de la capacité de chacun d’agir réellement (à l’opposé donc du concept de _citoyen du monde_ où on s’imagine, entre autre, appartenir à tout, partout, et qui se traduit souvent par très peu de concret si ce n’est un très gros « ethnocentrisme » et la justification de l’ingérence… mais c’est un autre débat…).

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/3q_xzQ7pRi4?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

Dans celui-ci, il s’agit de comprendre d’avantage non pas comment nous devons construire de nouvelles solutions, mais plutôt comment nous devons assumer qu’elles ont toujours été là, et que c’est un certain type d’organisation sociale (en l’occurrence la société de consommation) qui les a remplacées pour d’autres méthodes, à se taper la tête dans les murs tant elles ne portent que le morbide.

Il ne s’agit pas pour moi de les définir dans cet article, ni de définir la biodiversité… Ces deux docus là le montrent suffisamment bien : défendre le fait que sans cette diversité là, nous courrons à notre perte, par une mécanique naturelle contre laquelle même le capitalisme, pourtant chef d’œuvre d’adaptation et d’opportunisme, ne pourra pas trouver de solutions. Et que parier sur le long terme (par la confiance par exemple que je pourrais avoir en un candidat ou un autre à changer les choses, alors que tout le système avec lequel il va devoir dealer, si par un bienheureux hasard il n’en est pas lui même un produit, va se dresser face à lui – et ce n’est pas peu dire !) pour résoudre ces problématiques est une erreur terrible de conséquences…

* * *

### Leçons tirées ?

Ce que j’ai appris de ces documentaires et des idées qui ont trotté dans ma tête ensuite, jusqu’à cet article et les suivants, c’est qu’individuellement nous ne sommes pas faits (en l’occurrence écologiquement et biologiquement) pour vivre égoïstement, et nous ne sommes pas non plus faits pour épouser l’ensemble de la planète avec notre seul nombril, pour la guérir de toutes ses plaies. Nous avons deux pieds et deux bras, et l’échelle que ces 4 outils forment est locale certes, mais puissante.  
Alors pour ma part, j’essaie pour le moment de devenir locavore, et de réduire mon empreinte de consommation…

A l’échelle de l’espèce, nous ne pouvons pas survivre sans vivre **avec** le reste. Sans partager l’espace et le temps **avec** le reste, sans partager les ressources, les projections, **avec** le reste. Et surtout, nous ne pouvons pas DU TOUT concevoir nos modèles pour notre génération, pour « soi » : c’est même contre-nature en réalité.

Si le combat parait colossal, ou déjà perdu (effectivement les voyants clairement sont à l’écarlate), il y a pourtant des initiatives prises, d’ailleurs principalement dans des zones extrêmement sinistrées et laissées abandonnées par la société de consommation (notamment Detroit, et ses [nouveaux potagers urbains](http://www.rfi.fr/emission/20150411-2-detroit-agriculture-urbaine-alimentation-jardins/)…), qui montrent une voie.

Comme d’habitude, le salut viendra de ceux qui n’ont plus aucun confort, et qui sont contraints pragmatiquement de renouveler les choses radicalement, en se réappropriant ce qui a été confisqué il y a longtemps.

_A suivre : sapio-diversité (part 2), demo et socio-diversité (part 3)__, …_
