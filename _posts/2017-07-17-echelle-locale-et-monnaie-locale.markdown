---
layout: post
title: Échelle locale et monnaie locale
date: '2017-07-17 10:38:00'
desc: La semaine dernière, j’étais en vadrouille fromagère. Entre deux producteurs de fromages de chèvres et quelques verres de bons vins, une commerçante du coin avait sur son comptoir un prospectus à propos de la Gâtinelle. Une monnaie locale, dont la conception et la justification viennent consolider ma foi en cette échelle.
---

La semaine dernière, j’étais en vadrouille fromagère de la Touraine au Poitou. Entre deux producteurs de fromages de chèvres et quelques verres de bons vins, une commerçante du coin avait sur son comptoir un prospectus à propos de la Gâtinelle.

Une monnaie locale, dont la conception et la justification viennent consolider ma foi en cette échelle.

* * *

### La Gâtinelle, monnaie locale de la Gâtine

C’est aussi le nom d’une coiffe locale, et d’une race de vache… Mais là on parle de monnaie locale (ou monnaie locale complémentaire pour être exact). C’est une association basée à Chatillon-sur-Thouet qui est en charge de l’édition de cette monnaie propre à la région du Gâtinais. Cela veut tout dire. Contrairement à une entreprise, une association n’a pas de velléités d’enrichissement propres, elle ne vise que la pérennisation de son projet dans le temps. Et par nature a pour mission de garantir _de facto_ l’équilibre entre tous les partis concernés.

Un projet qui est très concret et facilement compréhensible :

- Faire valoir la richesse du territoire en étant acteur local.
- Contribuer à l’essor économique et à la promotion de la région via les acteurs locaux partenaires.
- Réinvestir dans les initiatives économiques locales (cycle vertueux).

Quelque chose de simple et de logique. Orienté naturellement vers la dynamique du local, le circuit-court, avec un soupçon d’intérêt commun et de collaboratif. A une échelle, j’en ai grandement parlé sur ce blog, qui permet de rester flexible et de ne pas s’embourber.

Avec cette monnaie locale, l’utilisateur / payeur contribue directement à ce qui est produit sur place. Il gratifie les efforts des producteurs, des investisseurs, des travailleurs des filières concernées. Sans se couper du reste… Puisque évidemment, il est hors de question pour les partenaires de refuser la monnaie européenne…

* * *

### La monnaie locale, rouage essentiel du futur

Il est pour ma part difficile d’imaginer un futur où de telles initiatives resteraient à la marge. Impensable de me dire que nous allons continuer avec ces monnaies globales, perturbées, inflexibles, vérolées. Si dures à manœuvrer qu’un coup de barre un côté fait basculer bien des gens par dessus bord…

La monnaie locale est un des instruments d’une société plus fluide, qui s’adapte pleinement à ses besoins territoriaux et locaux. De fait, elle ne souffre d’aucun changement brusque, et n’impose rien de bouleversant : elle est capable de suivre le mouvement, de l’assouplir même. La dimension associative, en attendant peut-être d’éventuels statuts spécifiques de même nature, permet de conserver l’intérêt collectif au centre du projet.

Il existe aujourd’hui plus de 2000 monnaies/devises locales dans le monde. Une grosse trentaine en France, dont on trouve le répertoire (une carte Open Street Map, merci les gars !) et la charte de création [sur ce site web](http://monnaie-locale-complementaire-citoyenne.net/france/). Certaines sont numériques, d’autres ont des pièces ou des billets, les deux parfois. Cela représente autant d’organisations humaines, locales, dynamiques qui permettent en bout de chaîne la vie de projets locaux. Leur durabilité aussi.

* * *

### Monnaie locale et crypto-monnaie

Voilà un duo gagnant à mon sens, sur tous les plans. D’une part, des monnaies transnationales comme le bitcoin ou l’ethereum. Elles permettent des échanges internationaux aisés, pratiques, comme des devises globales (je pense à l’€) le font un peu maladroitement aujourd’hui. Elles seraient le standard de transactions d’un endroit du monde à un autre, avec fluidité et sans effet de change.

D’autre part des monnaies locales, adaptées aux économies territoriales et aux diversités tant technologiques que culturelles, qui font le rapport d’un groupe social à sa monnaie. Des monnaies plus ou moins dématérialisées, qui, comme nous l’avons vu plus haut dans l’article, appuient et dynamisent les économies de l’échelle locale.

* * *

### Changement d’époque, changement d’outils

Nous sortons des époques nationales, où les monnaies et les frais de change rendaient les transactions internationales relativement complexes. La mise en place de l’€ représente aujourd’hui la pire éventualité quant à la globalisation d’une monnaie (mal conçue, calée sur l’économie d’un pays, imposée à XX autres qui suivent à marche forcée…). Mais elle répondait (certes mal) à une demande naturelle : celle de pouvoir commercer internationalement et avec d’avantage de fluidité.

Nous entrons (en tout cas ce serait à mon humble avis pas mal…) dans une époque de local. Ou les outils économiques se créent à nouveau en fonction de cette échelle. En fonction des besoins humains qui y sont liés par proximité.

Si les monnaies locales en France sont dites « complémentaires », ce n’est pas pour rien. Pour l’heure elles sont malheureusement complémentaires d’autres monnaies qui elles sont totalement bancales… Mais on peut souhaiter sans être irréaliste que leur alliance avec les crypto-monnaies sans territorialité serait optimale.

La combinaison de ces deux outils, à ces deux échelles, représente enfin l’avènement de solutions adaptées et modernes. Espérons que rien de politique ne se mettra sur leur chemin… (suivez mon regard…)
