---
layout: post
title: Revue de Net ep5 – notifications off
date: '2017-08-11 15:38:00'
desc: Depuis fin juillet, j’ai désactivé toutes les notifications push, top bar et led lumineuse sur mon smartphone (exception faite pour les appels et sms). Meilleure idée de ma vie !
---

Depuis fin juillet, j’ai désactivé toutes les notifications push, top bar et led lumineuse sur mon smartphone (exception faite pour les appels et sms).

J’ai pris cette décision un peu à l’instinct. Un peu dans le prolongement de mon minimalisme digital / numérique. C’est vrai, je sens qu’au fond le fonctionnement des objets à écran n’est pas naturellement le bon. Sur plein d’aspects, qui m’apparaissent de plus en plus préhensiles, leur usage est même presque plus toxique qu’autre chose.

Sans vouloir non plus tout quitter, tour fermer, abandonner ou supprimer mes comptes, désinstaller des applications ou opérer un downgrade en achetant un téléphone à l’ancienne, j’ai trouvé cette option là : mettre en mute les notifications.

Alors depuis cette décision, je cherche des articles ou des vidéos de personnes ayant réfléchi au sujet. Pour me permettre d’étayer ma propre réflexion, en attendant de rédiger sur ce blog un premier bilan, dans un bon mois. Cette revue de net va donc recenser deux liens qui touchent à ce sujet.

* * *

### [EN] Turn off your push notifications. All of them. – par David Pierce

Un article en anglais, publié chez [Wire](https://www.wired.com), et directement lié au thème. L’article revient sur nos utilisations des smartphones, notre « servilité ». En exposant aussi pourquoi les smartphones eux-mêmes ne sont pas un problème, ou encore la différence entre notifications _push_ et _pull_. Parfait, clair, lisible. [Cliquez ICI pour la lecture !](https://www.wired.com/story/turn-off-your-push-notifications/)

* * *

### [TED] Why our screens make us less happy – par Adam Alter

Encore un TED ! Parce que bon, ces vidéos sont souvent géniales, qu’on se le dise ! Ici, Adam Alter, psychologue spécialisé dans nos relations aux technologies et aux écrans, raconte comment les écrans en général nous rendent moins heureux (oui, je sais, je ne fais que traduire le titre). C’est une problématique plus globale, dont celle des notifications fait partie (comme un des symptômes). Vidéo ci dessous !

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/0K5OO2ybueM?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>
* * *

Rendez vous en Septembre, pour un feedback sur ma nouvelle utilisation des applications et du téléphone, sans notif !
