---
layout: post
title: Hacker qui ne sait pas coder ?
date: '2017-03-16 20:36:00'
desc: Il est un trait naturel qui n’est ni anodin, ni unique. Et si on le retrouve en commun chez les hackers, on le retrouve aussi dans des pans entiers de cultures, ou de contre-cultures. De quoi parle-je au juste ?
---

Suite à [mon article sur la sapio-diversité](https://circa1984.net/la-diversite-comme-clef-de-voute-part-2-la-sapio-diversite/), je sentais le besoin monter de mieux définir mon propre profil, au delà de la courte description sur ce site…

D’ailleurs elle dit quoi ? Elle résume sommairement mon parcours universitaire (inachevé du reste, soyons clair) et dans les grandes lignes mon parcours pro… Elle trace aussi rapidement mon « profil » de multi-potentialiste, dont je parle également ici, mais dont l’étiquette n’est pas forcément pile poil…

Bref, ça fait looooongtemps que je me dis que j’ai tout du hacker sauf le codage… Et puis en me rapprochant un peu de gens qui eux savent coder, j’ai commencé à me rendre compte que la condition élémentaire du hacker n’était pas le code…

* * *

### Je ne porte pas de sweat à capuche

Le cliché en vigueur c’est le sweat capuche, une certaine paranoïa sur les bords, une passion pour les expressions techniques et pour l’ordinateur. Au delà de çà, on a aussi les caricatures : le vilain méchant pirate qui est toujours russe ou chinois, le geek que personne comprend mais qu’on aime bien quand même, etc. Clichés et stéréotypes sortis d’articles, de bouquins, de séries, ou de films…

En vrai, j’ai demandé à quelques concernés, et voici leur réponse quasi-unanime, et bien résumée dans [cet article](http://blogs.lesechos.fr/intelligence-economique/paroles-de-hacker-a5543.html) parut dans Les Échos et écrit par Eric Filiol :

> _« Le terme de hacker désigne toute **personne capable d’analyser en profondeur un système** – que ce système soit technique comme un ordinateur ou un téléphone, mais également humain, social, législatif – de sorte à en comprendre les mécanismes les plus intimes, en privilégiant le résultat sur la méthode (contrairement souvent à l’approche académique). »« **Le monde hacker est capable de remettre en question** – chose constatée dans les dernières conférences de hacking entre autres choses – **la plupart des mécanismes (…) actuels,** en particulier ceux fondés sur une vision purement académique. »_

On est loin de quelque chose de foncièrement précis, on est loin d’une déconnexion complète d’avec le réel, on est également loin de quelque chose de purement informatique quand bien même l’expression la plus connue des hackers soit le logiciel. Hacker est un fonctionnement global.  
(Si vous voulez en savoir plus sur Eric Filiol =\> [PAR ICI](https://www.youtube.com/watch?v=Fn_dcljvPuY) )

* * *

### Un trait naturel avant tout ?

Le hacker se définirait donc par sa propension à détourner naturellement une réflexion, un objet, un outil, de son usage premier ou de sa conclusion habituelle. Mais qu’est-ce qui l’amène à naturellement produire cette réaction / action ? Sans aucun doute l’influence de tous les facteurs de la sapio-diversité dont j’ai parlé ici.

Ce trait naturel n’est ni anodin, ni unique. Et si on le retrouve en commun chez les hackers donc, on le retrouve aussi dans des pans entiers de cultures, ou de contre-cultures.

Je prendrais l’exemple qui me vient le plus naturellement, le hip hop, et notamment les producteurs de musique. Ceux là même qui lorsqu’ils étaient DJ ont détournés l’usage de lecture de vinyle en inventant le scratch. Ceux qui ont détourné les samplers et drum machines pour composer des morceaux entiers, riches, arrangés, séquencés. Des détourneurs. Des débrouillards.

Je me souviens aussi des danseurs, qui eux détournent les lieux publics (à Toulouse, c’était un hall au sol marbré dans les couloirs du métro à la gare) pour en faire des salles d’études, d’entraînement, de transmission.

Je ne fais pas ce rapprochement tout seul. C’est un camarade, [Daily Laurel](https://medium.com/@dailylaurel), qui me l’a soufflé. Sa fascination pour les beatmakers et pour le hack suffit à convaincre de la similitude d’attitude et de positionnement entre les deux.

* * *

### Hacker n’importe quoi et tout apprendre

A ce titre, il est un bon exemple de hacker qui n’est pas centré sur le code ou le programme ou le piratage ou je ne sais quoi d’informatique. Lui son truc, c’est le [bio-hacking](https://en.wikipedia.org/wiki/Biohacking).

Parce que finalement, on peut hacker n’importe quoi, parce qu’on peut trouver une utilisation différente de l’initiale a chaque objet ou outil. Daily Laurel lui fait dans le biohacking avec son [BioHackingSafari](http://biohackingsafari.com/). Il fouine et trouve de par le monde des gens qui détournent l’usage commun de la biologie et du monde biologique.

Un exemple concret de méthodes et d’actions via lesquelles la compréhension et l’innovation sont possibles par la pratique. Il y a donc une dimension d’autodidaxie prégnante dans ces types de hacks. Dans le hack tout court.

* * *

### Quant à moi…

Mes différentes carrières ont toujours été menées en autodidacte. J’ai appris sur le tas, compris assez vite, synthétisé, et essayé de transmettre… Avec plus ou moins de réussite.

J’ai surtout appris quasiment tout sans tuto, et a posteriori en discutant avec des spécialistes (ce que je ne suis pas du tout), je me rendais compte que j’utilisais des logiciels ou des outils de manière complètement inhabituelle et pas vraiment comme les concepteurs avaient prévu.

De la même façon, ma manière de disséquer un sujet, d’actualité ou de société, est souvent décalée. C’est d’ailleurs parce qu’on me l’a encore fait remarqué hier que j’ai eu l’idée de faire cet article. J’ai plutôt tendance à tourner la problématique en question dans tous les sens pour avoir une vision moins linéaire, plus décentrée, et que je trouve plus réelle.

Donc même si je n’utilise jamais ce mot pour me décrire, je ne pense pas être si éloigné que ça d’un hacker. Je ne sais pas coder, mais naturellement je me décale, je détourne l’usage commun et premier des outils, je crée les miens, sur-mesure, en comprenant profondément leur impact…

Je ne fais pas « exprès », mais ça me va bien. Parfois un peu de mal à me faire comprendre mais ça c’est un autre sujet.
