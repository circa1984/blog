---
layout: post
title: La fable de la petite échelle du lait cru
date: '2019-10-22 06:30:00'
desc: Pas plus tard que ces deux dernières semaines, une production artisanale que j'ai au téléphone tous les jeudi souffre d'une contamination à la salmonelle. Ces produits n'arrivent jamais chez moi dans cet état. Et ça c'est un petit miracle que je voulais vous raconter.
---
Dans mon métier de fromager, je vois de tout passer concernant la santé. Notre produit est un produit préférablement vivant : le fromage au lait cru. Avec lui viennent des tas de fantasmes. Dans le bon comme dans le mauvais sens, il est l'idole de celles et ceux qui n'y voit que le Bien, et il est le diable pour celles et ceux qui ont peur de sa richesse bactériologique.

Alors, nous, les fromagers qui vendont ces bricoles, on est le cul entre deux chaises, pris entre deux feux. Mais on a surtout des fournisseurs qui eux dealent au quotidien, plus près encore que nous de ces problématiques, avec les risques et la beauté du produit.

Pas plus tard que ces deux dernières semaines, une production artisanale que j'ai au téléphone tous les jeudi souffre d'une contamination à la salmonelle. Ces produits n'arrivent jamais chez moi dans cet état. Et ça c'est un petit miracle que je voulais vous raconter.

## Lait Cru, la vie la vraie

Mais alors, c'est quoi le lait cru ? Eh bien, c'est très simple, c'est le lait tel qu'il sort de la vache. Cru, parce qu'il n'a subi aucun procédé thermique ou chimique de stérilisation quelconque (UHT, pasteurisation, thermisation, microfiltrage, etc). Il est donc nature peinture le lait cru, riche de tout ce que le terroir et l'humain derrière le troupeau ont pu filer à la vache pour la nourir. Au top quand cette nourriture est saine, au crado quand cette nourriture est crado aussi (comme nous quoi, sans vous faire le dessin...).

Plus poétiquement, le lait cru est le produit d'un écosystème tripartite : un humain, un animal, un terroir. Assemblés comme des Avengers pour fabriquer du lait. Ce lait cru est donc le témoignage de ce que vivent les trois parties de cet écosystèmes, de leur harmonie, et de leur apports intrinsèques à l'équilibre du trio. C'est un produit formidable en ce sens. Un témoignage, tout ouvert à qui sait lire la biologie et la bactériologie. La source, pour qui s'y prend par la gourmandise.

Alors lorsqu'un produit devient sensible, contaminé, tout cet écosystème est mis à mal : il faut soigner les bêtes, vérifier que le terroir soit toujours sain, passer au peigne-fin les habotudes de l'humain (protocole HACCP par exemple), bref, la totale. C'est un sacré stress. Ca peut être encore plus fou lorsque les produits finis (bouteilles de lait, crèmerie, fromages) sont en rayon, chez nous les fromager-ère-s. Pire encore, quand ils sont chez les client-e-s...

Mais je vous rassure, je recois davantage d'alertes sur les produits pasteurisés et industriels que je n'en recois sur les produits au lait cru... Parce que la recherche du productivisme absolu propre à l'industriel l'amène plus près encore de la faute qu'il ne le serait naturellement à échelle artisanale.

## Artisanat, modèle économique, prévention

En fait, l'entreprise qui me fournit le dit produit-contaminé, je lui parle toutes les semaines depuis quelques années. Alors c'est sans hésiter qu'elle me confiait ses déboires : une contamination salmonelle qui fait que le lait n'est pas transformable. C'est terrible pour eux, parce que le lait, c'est leur vie, c'est leur pain et leur frigo plein, leurs factures réglées, la totale quoi. Pour moi, ce n'est pas grave, j'ai soit du stock, soit d'autres choses. Et mes clients comprendront, sans sourciller.

En face de cet exemple, j'ai recu un jour un papier des services vétérinaires, pour le retrait de tel Brie fabriqué par telle grosse laiterie appartenant à tel gros industriel. Un truc cracra vraiment balaise. Je n'en vends pas, je n'etais pas concerné. Mais des tas de confrères et de consoeurs ont dû retirer les produits de la vente. Des tas de ce Brie ont même dû finir dans les frigo de leurs clientèles... On ne les a pas au téléphone eux. On ne leur parle pas. Et de toute facon, ils ne prévoient pas... Rappeler la marchandise a un coût, mais ca vaut le coût pour eux.

Parce que pour les artisans qui m'appellent, le rappel de produits contaminés, c'est ingérable, impensable, en terme de trésorerie. Des entreprises familiales, incapables de gérer le rappel de leur marchandise sur XX fromageries livrées... Impossible. Alors on teste PREALABLEMENT. A toutes les étapes. C'est le modèle économique, petit ou moyen, familial, humain, qui contraint/permet cela. C'est la taille, locale, gérable, qui contraint/permet cela.

## Petite échelle primordiale

Sur ce blog, j'ai parlé d'échelle locale un nombre incalculable de fois... Ici, on est dans le même thème. La taille humaine de l'échelle économique vient démontrer qu'elle regorge de solutions naturelles à des problèmes qu'elle ne sohaite pas se créer en amont. Des problèmes qui, lorsqu'ils existent, peuvent même jouer avec la santé publique. Des vrais de vrais problèmes.

Cet entreprise artisanale de fabrication de fromage, elle va remonter la pente tranquillement parce qu'elle ne va pas souffrir de scandale sanitaire et de la mauvaise pub inhérente. Elle ne va pas dépenser une fortune à rappatrier et détruire ses produits. Non. Elle va juste se contenter de soigner son bétail, rigoureusement éradiquer la salmonelle chez l'éleveur et informer la source si externe, et recommencer à produire ses fromages lorsque les services habilités donneront leur feu-vert, une fois les prochains tests passés.

C'est tout con. Mais je trouvais cet exemple criant de vérité. Combien la taille d'une entreprise est determinante à tout point de vue. Et combien l'artisanat et la proximité entre un produit et son écosystème sont importants pour que ces problèmes ne deviennent pas ingérables et dangereux. Longue vie à ce GAEC, longue vie à l'artisanat, et longue vie au lait cru !
