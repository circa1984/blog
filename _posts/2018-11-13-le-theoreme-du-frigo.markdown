---
layout: post
title: Le théorème du Frigo
date: '2018-11-13 08:00:00'
desc: Un peu comme avec la sémantique de la patate, je vais essayer de vous parler de cet exemple du frigo pour parler de quelque chose de plus large.
---

Un peu comme avec la [sémantique de la patate](https://circa1984.net/semantique-de-la-patate/), je vais essayer de vous parler de cet exemple du frigo pour parler de quelque chose de plus large.

On présente le frigo comme l'espace de stockage parfait pour nos aliments. Ma question est toute simple : est-ce que c'est réellement le cas ? Et si oui, comment on fait tous les humains pré-révolution industrielle pour conserver leurs aliments ?

Non parce qu'en vrai, un frigo c'est un trou noir à ressource énergétique, donc remettre en question, même par séquences ou étapes, sa place et sa dimension dans nos vies est un passage obligatoire, dès maintenant !

* * *

### Pourquoi le frigo ?

Cette question m'est venue en tête par l'intermédiaire de [Geoffrey Dorne](https://graphism.fr/), un camarade qui réfléchit sensiblement au thèmes que moi, et notamment l' "après" effondrement, la transition, etc. Nous en étions à discuter passionnément de résilience, et il est clair que le frigo est un exemple flagrant du projet.

Quand on le regarde comme ça, et qu'on réfléchit au pourquoi du comment, on se dit que le frigo est né en réponse au besoin de stockage d'aliments dans des zones privées d'espace : les villes. L'urbanisation à outrance et l'exode rural n'en finissant jamais, il fallait bien stocker son garde manger quelque part. Et pouf ! Le frigo ! Non ?

Oui, presque. En vrai, le concept a toujours existé, mais n'a jamais fait appel à autant de ressources qu'aujourd'hui. Les systèmes de conservations du froid pré-industriels se servaient surtout soit de la conservation de la température d'une pièce (type cave ou cellier), soit alternaient les couches de paille et de glace (prélevées sur les points d'eau en hiver - avant le réchauffement climatique quoi...), dans des trous permettant l’évacuation de l'air chaud par ventilation.

Et puis le frigo a aussi une dimension industrielle, avec notamment la chaine du froid qui permet le voyage d'aliments frais d'un bout du monde à l'autre... Mais ça, c'est un projet qu'on résout avec le locavorisme, n'est-pas ?

* * *

### Cellier et urbanisme

Alors c'est sûr, aujourd'hui c'est vachement pratique un frigo. TOUT LE MONDE en a un ! Il est plus ou moins plein en fonction des courses qu'on a fait, des invités qu'on a eu, et puis surtout d'où on achète son miam, et de la connaissance qu'on a quant à sa conservation. Oui, j'ai appris il y a ans qu'on pouvait conserver les œufs hors du frigo, que les légumes n'avaient pas besoin d'y être non plus. Il reste quoi à mettre dedans alors ? Bah le beurre... j'en mange pas... Du lait ? Je n'en bois quasiment pas... De la viande ? Jamais chez moi. Bon, voilà, mon frigo est quasiment tout le temps vide. D'ailleurs, je vous laisse 30sec, je vais le débrancher tout de suite.

Parce que si je me souviens bien, mon grand-père, qui entretenait un immense jardin qui nourrissait bien du monde dans un joli trou paumé en Aveyron, il mettait pas toute sa récolte au frigo.Il y avait certes un gros congélo dans une cave, mais avec quoi... trois glaces, des grosses pièces de viande, et basta. Par contre, il y avait deux ou trois caves et celliers littéralement remplis de patates, de pommes, de coings, d'oignons, ... Maintenant que je m'en souviens, la température s'y régulait comme par magie (gros murs en pierre, sous l'habitation, très peu d’électricité, pas de courant d'air).

C’était le king du cellier en fait ! Le cellier, c'est cette pièce toute bête qu'on a complètement oublié de mettre dans 90% des appartements des grandes villes parce qu'on a préféré coller la main d’œuvre. AH, la beauté de l'urbanisme quand on y pense.

Parce que clairement, les grandes villes du monde reposent pour beaucoup sur un système ancien mais remarquable de traitement des eaux. Je pense que la ville d'aujourd'hui et de demain devrait logiquement être celle de la conservation des aliments (sinon de leur culture, au moins partielle). Or, l'architecture de Paris par exemple, l'état de ses appartements, des cages à poule, le prix de ses loyers, et tout le toutim, ça nous fait une grosse écharde-poutre dans le pied (jusqu'à la cuisse !).

* * *

### Apprendre à limiter sa consommation du frigo

A titre individuel, reconstruire une ville entièrement est impossible. Cela nécessitera le temps suffisant aux pouvoirs publics (ou autres décideurs circonstanciels) pour comprendre l'enjeu et se déterminer face à lui. Néanmoins, nous pouvons chacun dans notre coin mieux réfléchir à notre utilisation du frigo. Et en particulier à son dimensionnement. A ce titre, j'ai demandé sur les réseaux sociaux qu'on m’envoie des photos de frigos ouverts, voici le résultat (que je trouve probant !) :

Est-ce qu'il est réellement utile d'avoir des tanks à froid aussi balaise dans nos cuisines ? C'est une question qui, même en ayant repositionné mon alimentation depuis des années, a tardé à me venir en l'état. J'aurais préféré me la poser plus tôt. Et être capable aujourd'hui que je déménage, de mieux construire le projet. Peu importe, le temps, individuel, est un allié (je préfère le considérer ainsi - n'en déplaise aux plus pessimistes des collapsologues, que je salue au passage !).

J'ai demandé sur les réseaux sociaux à ce que vous m'envoyiez des photos de frigos ouverts. Le résultat est probant je trouve : nous ne sommes pas égaux dans la connaissance de ce qu'il est nécessaire d'y ranger ou pas.

Nous ne sommes pas non plus égaux devant la quantité et la multiplicité des choses que nous consommons. C'est une excellente illustration de la nécessité primordiale de compréhension des régimes alimentaires avant tout. Ce n'est pas grave, on part toutes et tous de quelque part, et l'important c'est plutôt l'arrivée.

Parce que finalement, consommer majoritairement végétarien et local, c'est réduire le remplissage de son frigo. Avoir une alimentation saine, c'est réduire le remplissage de son frigo. Et on peut même pousser plus loin : faire son beurre, ses boissons, ses yaourts, soi même, c'est réduire le remplissage de son frigo. OUI, je sais, ça prend du temps. Ça prend du temps quand on ne considère pas cela comme une priorité. Sinon c'est du temps passé à faire un truc intéressant, instructif, et nécessaire : on ne perd pas du temps là !

J'ai donc pour projet de limiter le remplissage de mon frigo au maximum. Et transformer encore plus de produits moi même. Apprendre ces choses là pour les transmettre aux suivants ensuite, et qu'à leur tour ils-elles soient capables de réduire leur usage du froid électrique... Et je compte bien regarder comment font [celles et ceux qui apprennent directement à vivre SANS frigo](http://www.lefigaro.fr/conso/2017/09/16/20010-20170916ARTFIG00013-elle-a-decide-de-vivre-sans-frigo-depuis-un-an.php) !
