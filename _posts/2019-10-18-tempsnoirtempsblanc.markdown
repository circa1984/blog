---
layout: post
title: Temps noir, temps blanc
date: '2019-10-18 17:39:42'
desc: Il n'y a pas qu'en physique que le temps est une donnée relative. En sociologie (de blog !) aussi. En matière de lutte des races, principalement et particulièrement.
---

Il n'y a pas qu'en physique que le temps est une donnée relative. En sociologie (de blog !) aussi. En matière de lutte des races, principalement et particulièrement.

Il y a quelques semaines, j'avais [un échange](https://twitter.com/circa1984net/status/1169971750228111367?s=20) avec la réalisatrice Amandine Gay à propos de l'hypothèse suivante : dans nos rapports de races, la tension du temps et ses conséquences sont des éléments déterminants et pourtant complètement absents des discussions.

Dans la première mouture de cet article, je cherchais à raconter des exemples que je n'ai pas vécu. Relecture (double, externe et concernée) et correction (nécessaire): voici un article qui ne parle que de mon propre vécu personnel et proche.

_Le contenu de cet article est strictement de l'ordre de l'hypothèse sociologique et sociographique. Il n'est pas issu d'une enquête de terrain ni de résultats statistiques. Il reflète les sensations partagé-e-s (les miennes et celles de mes contacts racisés) sur un sujet en réflexion personnelle. Aucune conclusion ou généralisation ne pourra en être tirée comme une réalité concrète._

## Elasticité du temps

Je ne sais pas si vous avez déjà subi une agression, mais on peut s'accorder à dire que dans ces moments là, le temps est élastique. Il s'allonge ou se raccourcit en fonction de qui on est dans l'action : l'agresseur vit un temps très court tandis que la victime vit un temps à rallonge, dont elle aimerait bien s'extraire. Le temps est donc une donnée élastique, subjective et pas du tout absolue. Un objet qui se meut différemment et qui ne porte pas les mêmes conséquences selon l’angle qu’on lui porte.

C'est un paramètre important, ce temps là. Il conditionne en effet des choses indispensables : la prise de recul, la maturité, la réflexion, la sagesse. Et puis il joue aussi sur la décision, la prise de parti, et même l'action. Il est presque un tiers, inextricable, des équations de rapport de classes et/ou de races, ou même d'interactions individuelles. Le sens du timing, pour prendre un exemple, est ainsi une qualité, tant dans le milieu du travail que dans celui de la séduction. Le temps est gravement important, autant donc que son élasticité.

Dans les rapports de races et ses luttes, le temps ne déroge pas à sa propre règle de condition omniprésente et élastique. Il est là-encore partie prenante de ce qui se passe. Il déclenche des choses (réactions, actions, sensations, émotions) et joue sur les équilibres (individuels, collectifs, sociétaux). Et parfois, souvent, quasiment tout le temps à vrai dire, son élasticité coûte d'avantage qu'elle ne rapporte. Pourquoi ? Parce que les rapports de races ne sont pas sains, et ne l'ont jamais été. Tout puissant qu'il est, le temps lui-même n'est qu'un paramètre exhausteur de ce qui se passe dans ce contexte terrible. Au racisme systémique qui régit notre société, le temps et son élasticité sont des alliés fidèles, qu'il convient de remettre en question.

## Deux cas, deux mesures

_Je vais essayer de créer quelques représentations graphiques des vécus différents sur des situations précises. Ce blog se voulant low tech / bas impact, c'est un défi !_

_Essayons donc de dessiner la mesure ressentie du temps noir et du temps blanc sur des exemples donnés. De trouver les contours de la tension générée par le temps en fonction d'où on se situe, et d’appréhender la forme que prend le temps en tant que violence supplémentaire._

### CAS 1 - premier contact

La compréhension de l'existence des races sociales, et les échos sensibles, sera notre premier exemple. Ce moment où, blanc-he-s ou racisé-e-s, nous comprenons que nous ne sommes pas pareil-le-s aux yeux des autres et aux yeux de la société. Et les échos sensibles de ce moment : notre compréhension des enjeux individuels, celle des enjeux collectifs, le déterminisme systémique que cela représente, ainsi que la déconstruction du système qui créé et reproduit, génération après génération, le racisme.

4 lignes temporelles pour illustrer cet exemple. **T** déterminera ici l'instant T de la prise de conscience du racisme. La première ligne temporelle ( **1** ) sera celle du sujet blanc conscient, sensible, et à volonté d'allié-e. La seconde ( **2** ) sera celle du sujet blanc conscient, dans le déni (qui préfèrera ne pas vouloir comprendre davantage). La troisième ( **3** ), celle du sujet blanc conscient et pro-racisme. La quatrième ( **4** ) sera donc celle du sujet racisé.

_Le dégradé invoque ici le degré puissance de la violence ressentie (nul : &nbsp; / fort : ▓)._

**1** ) • &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **T** ▓▒▒▒▒░░░░░▒░░░░░▒░░░░░▒░░░•

**2** ) • &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **T** ▒▒░░░░░░░░░░ •

**3** ) • &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; **T** ▓░░░ &nbsp; •

**4** ) • ▓ **T** ▓▓▒▒▒▓▓▓▓▓▒▒▓▒▒▓▒▒▓▒▒▓▒▒▓▒▒▓▒▒▓•

D'abord, dire que les racisé-e-s prennent conscience du racisme plus tôt que les blanc-e-s est un doux euphémisme. Inutile donc de revenir sur ce point là. Ce qui importe à mon sens ici, c'est de voir combien la tension du temps chez les racisé-e-s est toujours totale ou quasi-totale, dès l'impact. Avec pour effet la création d'une situation d'urgence, basée à la fois sur des ressentis émotionnels quotidiens (racisme du quotidien), et sur l'analyse de la situation vécue (lutte des races, conceptualisation de la cause, organisation communautaire, etc). Cette situation d'urgence est d'une extrême violence.

Pour ce qui est du temps blanc, les différences avec lesquelles chacun-e d'entre nous, blanc-he, gère et s'engage après l'instant du constat font que nous ne sommes pas une masse sociographique uniforme et unique. Si l'unicité de nos privilèges de race ne fait aucun doute, ces différences majeures montrent tout le travail que nous avons à faire, à l'intérieur même de notre communauté, pour appréhender le concept de race sociale, puis de racisme systémique / privilèges-préjudices.

Les différences d'élasticité des temps noir et blanc sont donc créatrices de violence intime (nerveuse, psychologique, ...) pour les racisé-e-s, mais aussi de ces violences sournoises qui rendent les rapports de races compliqués : lorsqu'un nouveau sujet conscient décide de devenir un sujet allié, et qu'un-e racisé-e doit encore prendre sur lui/elle pour tout réexpliquer, par exemple. Ce moment qui synthétise combien l'enfer des racisé-e-s est (aussi et entre autres) pavé des bonnes intentions des blanc-he-s.

A ce titre, et presque en aparté de ce billet, se pose la question de la création d'outils pédagogiques par la communauté blanche pour la communauté blanche, sur le sujet du racisme systémique, des idoles foireuses, des références (livres, films, etc) sur le sujet, pour éviter de polluer le temps des autres communautés au delà des [300 secondes prescrites par Blackiavel ici](https://twitter.com/napilicaio/status/1041590581439422464).

### CAS 2 - le couple mixte

Cette analyse du temps ne déroge pas à celle du couple mixte, en tout cas du mien : parfaite mise en relief de comment elle (ma femme) et moi vivons les choses par nos prismes raciaux respectifs. Elle parce que noire, moi parce que blanc.

Sur le premier tracé, 1, nous retrouverons donc la ligne temporelle de ma femme, face aux choses extérieure ou intérieure à notre vie de couple qui lui rappellent sempiternellement le "mixte" de notre couple, et sa couleur à elle. En 2, ma ligne temporelle à moi, suivant les mêmes caractéristiques. Les lignes temporelles démarrent au début de notre couple et **T** est le moment de notre mariage, l'été dernier.

**1** ) • ▓▓▓▒▒▒▓▓▓▓▓▒▒▓▒▒▓▒▒▓ **T** ▒▒▒▒░▒▒░▒▒•

**2** ) • ▒▒▒▓▒▒▒▒▓▒▒▓▒░░░░░░▒ **T** ▓░░░▒░░░░░•

Clairement, nous ne vivons pas la chose de la même façon. Que les exemples soient exogènes à notre vie à deux, provenant soit de nos familles, de nos cercles proches (amicaux, boulot, ...), ou du quotidien urbain (croiser des gens, des regards, ...), ou intra-couple, entre nous (réflexes conditionnés, a priori, différences culturelles, ...), la conclusion reste finalement la même : en dernière instance, c'est TOUJOURS de ma femme dont il s'agit. Et cette dernière phrase est SI DIFFICILE à tourner et à écrire, qu'il va me falloir un paragraphe entier pour l'expliquer...

En ce qui concerne les "agressions exterieures", je ne les avais jamais vécues avant d'être avec une femme noire. C'est à dire jamais avant ma femme. J'ai donc entendu les remarques, les conseils, les attaques, les insultes, les emerveillements, et autres, propres à notre couple mixte, dès le moment où notre couple est né. Mais pour être tout à fait honnête, il ne s'agit, _in fine_, jamais de moi ! Il s'agit, toujours, sempiternellement, de ma femme. C'est ELLE qui est noire. Moi je ne vis que le collatéral, finalement. Se faisant, mon combat n'est pas celui de l'acceptation de mon couple mixte, mais bien celui de l'acceptation de ma femme. C'est RADICALEMENT différent. D'autant que pour nous, affectivement, notre couple mixte ne veut rien dire, il n'est clairement pas issu d'une volonté (fétiche quelconque, vision idéaliste ébahie du métissage, etc), comme l'est par exemple notre mariage (deux choses différentes, l'une de l'ordre du sentiment, l'autre de l'ordre de la construction de projet en commun).

C'est ainsi que comme vous le voyez sur le graphique, la ligne temporelle **1** est nettement plus active que la **2**. Simplement parce que ma femme recoit tout, en permanence. Il ne s'agit jaais de moi, isolé, seul. C'est soi elle, soit nous. Elle tout le temps donc. Le/la racisé-é est, dans notre système de pensée, dans notre système social, familial, dans nos schémas, et dans tous les exemples, la personne vers qui vont tous les exemples possibles. C'est vers elle que se tournent les regards, que vont les insultes. Envers sa culture que les remarques se portent, son accent pour les blagues. De la part des blanc-he-s qui sont autour du couple (quelque soit le niveau de proximité), mais aussi des racisé-é-s lorsque le couple mixte n'est pas quelque chose digne de leur goût (le célèbre : "la soeur qui se met avec les blancs pour l'argent", et j'en passe).

Sur ma ligne temporelle, **2** donc, la violence subit n'a rien à voir, en intensité et en latence, avec celle de ma femme. Il est même arrivé qu'elle en ressente VIA moi. Parce que mon retard à la compréhension fait qu'elle doit gérer frustration et attente avant que je ne serve, enfin, de bouclier là où je le dois et comme je le dois. Mon objectif sur ces sujets est donc de faire perdre le moins de temps possible à ma femme, parce que le temps en question, est autant de violence.

## Déconstruisons

Le temps est un donc un exhausteur de violence pour les racisé-e-s. Imaginons, à chaque cas de figure rencontré au quotidien, la ligne temporelle de la personne en face de nous. Quand elle refuse de nous expliquer quelque chose, c'est que c'est sans doute la XXXème fois qu'on le lui demande et que nous pouvons trouver l'info par nous-mêmes... Imaginons-nous, le temps gagné à ne pas vivre les mêmes préjudices, dans nos carrières pro, dans notre rapport à l'administration. Imaginons la balance du temps noir et du temps blanc, comme celle des préjudices et des privilèges de chacun-e.

Mais ca ne s'arrête pas là, en réalité. Pour nous, blanc-he-s, c'est aussi un signe palpable de ce que nous avons à faire de mieux. C'est un curseur qui doit nous permetre de saisir là où nous devons déconstruire et reconstruire nos rapports aux autres races sociales. Et c'est à mon sens un enjeu aussi grand que l'enjeu écologique. Nous ne réussirons pas l'un sans l'autre, et de toute façon, le boss de fin, dans les deux cas, est le même.

Les questions que me posent ce billet sont principalement tournées vers la documentation et la pédagogie. Je n'imagine pas vraiment d'évolution sur ce sujet sans supports, sans wikis, sans bases de données, en collectif, participatif, ouvert, et rigoureux. Des endroits et des plateformes qui rassemblent des informations, sur des concepts, de la sémantique, sur les fausses idoles et celles tombées par leur imperfection enfin à jour, sur les livres à lire sur tel sujet, sur les tweets qui expliquent ceci ou cela, etc. Un travail de titan, de documentaliste, de longue haleine. Qu'il faudra bien que nous fassions.
