---
layout: post
title: Privilégié
date: '2019-08-20 13:38:54'
desc: Il y a des mois que cet article est en suspens. En fait, c'est même seulement aujourd'hui que je crée le post sur ce blog. Il ne s'agissait à la base que d'une idée et d'une envie. Un truc que je sentais nécessaire à bien des égards, pour aller plus loin sur ce blog et pour stabiliser mes idées à ce sujet.
---

Il y a des mois que cet article est en suspens. En fait, c'est même seulement aujourd'hui que je crée le post sur ce blog. Il ne s'agissait à la base que d'une idée et d'une envie. Un truc que je sentais nécessaire à bien des égards, pour aller plus loin sur ce blog et pour stabiliser mes idées à ce sujet.

Mais ce n'est pas super facile d'aborder tout ça d'où je parle. Et bien que ma femme comme certains amis m'ont plutôt encouragé à le faire, c'est finalement [une tribune du joueur NBA Kyle Korver](https://www.theplayerstribune.com/en-us/articles/kyle-korver-utah-jazz-nba) qui aura fini de me convaincre de me jeter à l'eau.
Comme quoi, on peut même apprendre d'un wasp américain ! _(ce sera le seul trait d'humour de cet article !)_

###D'OÙ JE PARLE
Je parle d'ici, homme blanc, la trentaine. Je n'ai pas vraiment rencontré la mixité et la diversité avant la moitié de mon adolescence voire la petite vingtaine. Si j'écris cet article aujourd'hui c'est avant tout parce que me marier avec une femme noire militante a initié avec fracas la déconstruction de ma pensée pour que je sois capable de faire ce trajet presque solo aujourd'hui. Eh oui, même après de longues années de sciences sociales, c'est d'abord et avant tout la patience, l'opiniâtreté, le self-control, la pédagogie et l'amour d'une femme concernée qui a réussi à me faire passer du déni à la compréhension de mon statut de privilégié.

Il y a encore quelques années, je croyais encore fort à un certain universalisme, à cette vision rêvée de la France invariable devant la couleur, la religion ou le genre de ses citoyen-ne-s. J'ai rarement eu autant tort. Et c'est d'avoir autant eu tort à ce moment qui me fait réfléchir à fond sur ce sujet aujourd'hui.

Et quand je repense à toutes les blagues terribles que je faisais, je me dis qu'elles sont bien là où elles sont : derrière moi, en témoignage d'où je viens, de ce que j'ai été, balises de ce que je ne souhaite plus diffuser et encore moins transmettre. C'est très inconfortable de se souvenir de ces trucs. Très inconfortable, et c'est très bien ainsi, disons-le.
###LE PRIVILEGE
Dans sa tribune, Kyle Korver raconte une anecdote fondatrice pour lui. Je vous laisse aller lire, elle est inrésumable... Pour ma part, je n'en ai pas vraiment d'aussi pertinente, c'est beaucoup plus diffus pour moi. Il est possible que s'il était arrivé la même chose à mes potes noirs ou maghrébins/arabes, j'aurais sans doute eu la même réaction à écrire aujourd'hui. Je pense que le reflexe que Korver a eu est conditionné, ce dont certain-e-s arrivent à se sentir coupables avec le recul. D'autres non, mais c'est un autre sujet...

Un des trucs importants dans sa tribune : Korver confie qu'il a compris que sa couleur de peau lui permettait d'avoir le privilège de "sortir" de cette situation de racisme systémique : de pouvoir se fondre dans la masse dominante blanche de peau, d'en être dans tous les cas un vecteur et de jouir du confort inhérent à cette couleur de peau. Ce sentiment là, je le connais. Lui aussi, quand on essaie de faire son mieux, il est extrêmement inconfortable.

Il y a quelques années, lorsque nous commencions à nous fréquenter, je découvrais le racisme à l'égard des couples mixtes, ou plutôt, disons le pour ce que c'est en réel (et que j'ai mis X années à réaliser) : le racisme contre les femmes noires qui sont avec un blanc. Des hommes noirs insultaient ma femme (de se vendre à un blanc), et les hommes blancs d'y aller de leurs blagues racistes (sur le mariage pour les papiers)... Ça a duré son temps. Et c'est vrai que moi, j'aurais pu ne pas vivre cette situation, ce genre de situations. J'aurais pu en "sortir", "opt out" comme dit Korver. Et c'est d'ailleurs arrivé, malgré moi mais quand même, cette fois où les types nous avaient remarqués sur le quai de la gare, et avaient ensuite prise à parti ma compagne en l'insultant dans le train, tandis que je rentrais chez moi calmement sans rien connaître de tout ça...

###EXPRESSION ET SYSTÉMISME
Comment le privilège pourrait mieux s'exprimer concrètement ? Parce qu'en vrai, le reste du temps, je n'en suis pas témoin direct. Le racisme systémique, et ce qu'il créé de privilèges / préjudices en fonction de la couleur de peau, est avant-tout une machine maquillée, dont la déconstruction est lente et difficile.
C'est un ensemble de fins mécanismes. Pas juste des insultes, des cris de singes dans les stades de foot, des blagues de mauvais gout dans les repas de famille, ou des sketches de Michel Leeb. C'est la discrimination à l'embauche, le taux d'emprisonnement, la ségrégation urbanistique ou scolaire (liées l'une à l'autre), les contrôles de polices et les bavures, le traitement médiatique, la violence symbolique et notamment sémantique, et j'en passe...
Mais c'est aussi ces choses latentes, rampantes, dont nous sommes toutes et tous, que nous le voulions ou pas, coupables. La pérennisation des clichés, la sempiternelle question "tu es de quelle origine ?", les blagues lourdes qu'on croit drôles ou nouvelles, le [mot black au lieu de noir](https://www.arteradio.com/son/61657765/noir_pas_black), l'Histoire de notre pays et la façon dont on la raconte, qu'on fantasme et dont on se gargarise.
Trop d'exemples...

Autant de creux, de trous noirs entre un monde de préjudices et un monde de privilèges. Un bon exemple récent, le [traitement médiatique de l'addiction de Michèle Laroque à l'opium](https://twitter.com/BFMTV/status/1161268163775909889), clairement traité pour susciter la compassion de la ménagère parce que M. Laroque est riche, bourgeoise, blanche.

Je serai incapable de tout lister puisque je ne suis pas concerné. Je ne VIS pas tout cela dans ma chair. J'en récolte quelques bribes par procuration, auprès de celles et ceux qui autour de moi, subissent cela. Mais c'est mon empathie et non la couleur de ma peau qui génère l'écho. Deux choses radicalement différente. La seule chose que ma couleur de peau me permet d'être devant le racisme systémique, c'est un témoin par procuration.
###PRENDRE SA PLACE
Cette place là, je comprends chaque jour davantage qu'elle est un luxe que nous, les blancs, devons nous accaparer en opposition à la place de vecteur semi-conscient qui nous est habituellement réservée. Parce que d'ici, nous voyons nos congénères comme nous sommes, et qu'il n'y a qu'en partant d'ici que nous pourrons avancer dans le bon sens. Et parce qu'ici la déconstruction est possible.

Comme Kyle Korver, maintenant il faut que nous cherchions à savoir où nous en sommes. Je vais reprendre ou presque ses mots très justes :

Le fait que des racisé-e-s n'aient pas la même place que les blanc-he-s est **injuste**.

Le fait que notre société encourage la perpétuation des clichés racistes est **injuste**.

Le fait que le récit de notre Histoire de France soit arrangé et faussé, pour disculper une société entière, est **injuste**.

Le fait que nous refusions de faire tomber nos idoles racistes et d'en accepter les contrastes est **injuste**.

Le fait qu'on crie au racisme anti-blanc, sans comprendre que ça relativise des siècles d'oppression systémique, est **injuste**.

Le fait que les racisé-e-s subissent des politiques urbanistiques ciblées est **injuste**.

Le fait que les racisé-e-s subissent les directives et actions des forces de l'ordre, de quelque corporation que ce soit, plus férocement que les blanc-he-s est **injuste**.

Le fait que notre pays continue sa prédation coloniale sur l'Afrique est **injuste**.

Le fait que nous les blanc-he-s ayons tant de mal à en terminer avec le déni de nos privilèges est **injuste**.

Le fait que les inégalités entre races sociales soient si profondément ancrées dans nos institutions, nos administrations et notre société est **injuste**.

Encore une fois, liste non exhaustive. L'injustice est générale, mécanique. Une horlogerie de maitre.

Alors voilà. Lorsque nous aborderons ce sujet entre nous, en repas de potes, de famille ou sur le réseaux sociaux, au travail, n'importe où, je parlerai d'ici, avec ces idées-là fort dans ma tête. Je parlerai de cette place que j'ai prise, qui n'est pas toujours la plus confortable, mais c'est encore celle où je me sens le plus digne et utile en tant que blanc.
###FAIRE MIEUX
Je sais qu'après tant de temps à ne pas être à la bonne place, il faut que je fasse mieux. Prendre conscience d'un problème c'est bien, mais est-ce que ça suffit ? Est-ce que c'est suffisant pour tout le monde ?
Alors je me questionne souvent, sur ce que je peux faire mieux. Extraire la mécanique de ce racisme de mon conditionnement.

Je dois déconstruire beaucoup d'abord. Et pour ça, il faut écouter. À fond. Lire et écouter celles et ceux qui parlent. Sans parler soi-même nécessairement.

Mais le plus important de tout, sans aucun doute, c'est d'être un garde-fou auprès des gens comme moi. Se tenir ensemble responsables de nos paroles et de nos actes, et se rendre des comptes à nous-mêmes. Comme le dit Kyle Korver, notre inaction peut créer des espaces d'insécurité pour les concerné-e-s. Réduire ces espaces est notre affaire.

Toutes ces discriminations, ces injustices, ces préjudices que vivent les racisé-e-s viennent évidemment de notre Histoire commune. Celle des colonisations, de la prédation économique, culturelle et sociale, des idéologies racialistes, ...

Si nous sommes *coupables* de cette Histoire ? Sans doute que non.
Si nous sommes *responsable* de sa continuation ? Bien sûr que oui.
Faire mieux c'est essayer de rompre ce cycle, cette dynamique.

Les efforts sont considérables et nombreux, mais l'intérêt général prime. Et parler d'intérêt général sans la plus grande dose d'équité et d'égalité, ça ne sert à rien. Le rapport privilèges / préjudices doit être gommé de nos comportements individuels. Et pour se faire, il doit être absent de notre système social.
Ce n'est pas une mainte affaire. Mais c'est l'affaire de toutes et tous.
Je suis blanc hétéro marié et en CDI. Il y a du pain sur la planche.
