---
layout: post
title: Notificiations OFF
date: '2017-11-06 12:17:00'
desc: Il y a quelques temps, j’ai remodelé mon usage du smartphone, en commençant par les notifications. Je ne me rendais pas compte de combien elles étaient intrusive, entre autres. Mais je voulais, pour changer ahah, perpétuer mon (trop petit) effort minimaliste et me réapproprier l’usage de mes applications et de mon téléphone.
---

Il y a quelques temps, j’ai remodelé mon usage du smartphone, en commençant par les notifications. Je ne me rendais pas compte de combien elles étaient intrusive, entre autres.

Mais je voulais, pour changer ahah, perpétuer mon (trop petit) effort minimaliste et me réapproprier l’usage de mes applications et de mon téléphone.

C’est chose faite. Et après quelques semaines d’utilisation, voici mes premières conclusions.

* * *

### Nuisance et concentration

Il faut comprendre dès le début que régler en off toutes les notifications de toutes les applications (sauf tel et SMS) n’aura que très très peu d’incidence sur le temps passé sur ces apps. Pour ma part, je consulte autant Mastodon, FB, IG… qu’avant. Peut-être un peu moins Twitter et Pinterest… Menfin, rien de significatif.

Ceci dit, d’avoir coupé les notifications a modifié en profondeur la nature du temps que j’y passe. Et si avant je venais sur ces applications par reflex conditionné de répondre à la notifications, j’y viens aujourd’hui a l’envie où à la curiosité de voir ce qui mes contacts ont publié.

Mon téléphone ne vibre plus que pour les appels et SMS. Son écran ne s’allume plus sans que j’appuie le bouton prévu à cet effet. La barre d’affichage est vide, même quand j’ai XX mentions sur X apps. J’y vais quand je le choisis. Pas quand elles me le demandent par signal sonore ou lumineux. De fait, j’attrape mon téléphone à des moments différents. Je m’en sers différemment, et l’ordre et l’ordination de mes habitudes est différent lui aussi.

Ma concentration est accrue, parce que les nuisances sont réduites. C’est terrible à dire, mais j’ai enfin retrouvé une utilisation active de mon smartphone, là où j’estime avoir plutôt eu une consommation soumise. Il est redevenu pour moi l’outil qu’il doit à mon sens être : intelligent, adaptable, fluide, disponible mais pas intrusif.

* * *

### Limiter le nombre d’applications ?

Pour le coup, après avoir essayé cette option complémentaire, je n’y ai pas trouvé quelconque effet positif, ou même un intérêt quel-qu’il soit. Effectivement, il y a des applications que j’ai remplacé ou enlevées par paquets, MAIS, je ne vois pas l’intérêt de ne plus rien avoir sur son smartphone… Sur ce point là, je lis beaucoup autour de moi des posts à propos de déconnexion des réseaux sociaux sur smartphone, et notamment FB. Pour ma part, j’ai préféré opter pour l’option où je configure l’application pour en être le moins _victime_.

Et si les notifications ont grandement aidé, je ne me suis pas arrêté là, ni pour FB (qui sert d’exemple ici) ni pour d’autres. J’ai pris le temps de quitter toutes les pages et groupes : je ne suis pas sur FB pour cela. Je ne suis pas l’actualité de mes groupes favoris de cette façon (à vrai dire, je ne la suis pas du tout… ahah). Je ne suis pas outre mesure non plus un grand loquace dans les groupes sur FB, j’ai toujours favorisé d’autres moyens pour fédérer ou participer. Ensuite j’ai également veillé à diminuer drastiquement le nombre de contacts que j’y avais, sautant de quasiment 3000 à quelque chose autour de 500. Et enfin, j’ai pris le temps de me désabonner et de m’abonner (en priorisant) à celles et ceux dont le contenu m’intéresse. FB devient donc un feed personnalisé, au mieux en tout cas, même si clairement, ça reste l’enfer de l’algorithme…

* * *

### Nouvelles habitudes, enfin naturelles et harmonieuses

J’ai retrouvé l’habitude de saisir mon téléphone par envie de faire quelque chose avec : la curiosité dont je parle plus haut, ou souvent l’envie de prendre une photo ou d’écrire mes articles, les relire, sans être sempiternellement interrompu par une bulle Messenger avec un sigle rouge et un morceau de message, un popup ou une énième vibration qui m’interromps…

Mes nouvelles habitudes correspondent désormais à ce que j’attends qu’un hypothétique smartphone parfait me fournisse : qualité d’appareil photo optimale + confort de lecture et de frappe. Bon, je dis hypothétique parce qu’avec mon OnePlus One, je suis évidemment un peu largué face à la nouvelle concurrence, au moins sur l’aspect photographique… mais bon, vous avez compris où je voulais en venir, n’est-ce pas ?

Et la hiérarchie de mon usage des applications à sensiblement évolué en ce sens. Si je passe toujours la majeure partie de mon temps sur les réseaux sociaux (encore que la qualité de ce temps est évolué comme je le disais plus haut), une place s’est libérée pour des applications « utiles » : WordPress, VSCO, et puis Pocket, dans lequel je charge mes futures lectures pour consultation ultérieure, notamment dans le métro.

Avec ces quelques mois à vraiment me pencher sur cette réappropriation technologique, j’ai compris que je m’occupais mal de ma vie numérique, de mon comportement vis à vis de la technologie et qu’il me fallait mieux faire. Aujourd’hui je maîtrise mieux mes outils. Non pas que j’en sois désormais le maître, mais ils ne sont plus anxiogènes comme ils peuvent l’être naturellement, sans qu’on s’en aperçoive du reste… Et ma réflexion continue, par le biais de personnes plus avancées que moi sur le sujet. Je pense à [J. Zimmerman](https://mamot.fr/@jz), [Ploum](https://mamot.fr/@ploum) et d’autres sur Mastodon, ou [G. Champeau](https://www.facebook.com/gchampeau) sur FB.
