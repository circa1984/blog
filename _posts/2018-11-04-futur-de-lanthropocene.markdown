---
layout: post
title: Futur de l'Anthropocene
date: '2018-11-04 08:00:00'
desc: La capacité de l'homme à se faire, en tant qu'espèce et ensemble de sociétés, une force géologique capable de modifier la composition de la surface de la planète ou encore les écosystèmes est à mon avis un fait indéniable. Comme la longueur de cette phrase.
---

La capacité de l'homme à se faire, en tant qu'espèce et ensemble de sociétés, une force géologique capable de modifier la composition de la surface de la planète ou encore les écosystèmes est à mon avis un fait indéniable. Comme la longueur de cette phrase.

Quelles sont les questions que cela soulève ? Quels sont les futurs envisageables en fonction des solutions théoriques possibles ? Je ne suis pas aussi bon que mon camarade Ploum pour créer de l'anticipation futurologique. Je vais donc me contenter de regarder ce qui sort de la collision entre échelle locale et futur de l’anthropocène.

* * *

### L'Ère du global et de la destruction :

Depuis la sortie de la seconde guerre mondiale, l'industrialisation datant alors d'un demi siècle déjà, s'est mue en globalisation. Les marchés se sont agrandi, en taille (mondialisation) et en pouvoir (centralisation). Les ressources de là bas servent ici, mais plus là bas. Les modèles s'uniformisent. Et les échelles plus petites, notamment les nations, se spécialisent, devenant à moyen terme infirmes, dépendantes, obsolètes, et incapables de se renouveler assez vite.

Pendant ce temps là, la richesse est créée. Redistribuée c'est un tout autre débat. Elle est en tout cas le centre de tout. Même la monnaie voit son rôle modifié. Elle qui devait être un garant n'est plus qu'un placebo à la dette.

L’écologie quant à elle, c'est à dire notre faculté de vivre sur cette planète en organisant les richesses qu'elle nous offre, atteint le niveau 0 : celui du marketing. Elle n'est plus rien d'autre qu'un argument, dans des grandes campagnes de [green washing](http://www.greenwashing.fr/definition.html).

L'humain global a tout bouffé. Il devait être porteur de paix. On se retrouve avec des guerres partout, une planète dans le chaos. Les flux migratoires s'intensifient, les espèces disparaissent, les écosystèmes crèvent. Global central merci, mais tu as fait ton temps. Ton influence sur l’anthropocène est une catastrophe.

* * *

### L'ère locale, et le futur décentralisé :

Souvent, les solutions qui réfèrent à un changement de paradigme sur ces sujets sont classées au rayon alternatives. Et même si j'ai moi même une catégorie de blog intitulé de la sorte, je ne suis pas super d'accord avec l'usage de ce terme, que je trouve encore imprécis... Dans la mesure où, in fine, elles ne proposent pas une innovation pure et simple, mais plutôt un recours aux méthodes et aux savoirs qui ont déjà porté leurs fruits, avec mise à jour système. À de rares exceptions près évidemment.

Nous sommes donc en face d'options, résilientes ou non, qui sont des alternatives aux modèles existants, ou mieux, des innovations (sociales, technologiques...). Un peu comme à la croisée des chemins où de nombreux choix sont à faire, et tous sont cruciaux !

Je l'ai déjà écrit en long et en large sur ce blog, mais l’échelle locale est naturelle pour l'humain. C'est l’échelle de la capacité. Celle où l'humain est capable de s’épanouir, d'avoir une influence individuelle conséquente sur l'écosystème auquel il appartient, de participer directement au bien de ce qui l'entoure, de le comprendre. Le local est l'échelle des initiatives vivantes, parce que peu obstruées par la distance, l'administration, la complexité technocratique verticale ou horizontale. C'est clairement l'échelle la plus naturelle pour une coordination humaine (hors internet) complète.

Alors lorsque j'imagine un futur plus brillant que les prévisions actuelles, il est nécessairement produit sur la base suivante :

- [stigmergie](https://fr.wikipedia.org/wiki/Stigmergie) : la capacité de nous coordonner entre nous, entre entités aussi, sera déterminante. Une certaine dose d'anarchisme, et de compréhension de ce que cela implique, fluidifie grandement l’implémentation d'une stigmergie la plus efficiente possible.
- [résilience](https://fr.wikipedia.org/wiki/R%C3%A9silience_(%C3%A9cologie)) : il faudra là de la modestie collective et individuelle pour accepter de reculer pour mieux sauter ensuite. La résilience nécessite ce petit retour en arrière pour retrouver l'harmonie nécessaire. Rien à voir évidemment avec le conservatisme, puisque le souhait de tout résilient est d'avancer, mieux et plus sereinement.
- décentralisation : plus les entités seront locales, plus nous éviterons de centraliser les choix, les gouvernances, les savoirs, mieux nos sociétés se porteront. L'adaptation n'est jamais aussi complète que quand elle s'extraie d'un cadre globalisé obligatoire où tout est homogénéisé.

* * *

### Anthropisation et anthropocène

Là où je souhaite réellement que nous comprenions l’[anthropocène](http://anthropocene.info/short-films.php) collectivement, je souhaite aussi que nous comprenions les dégâts de l'[anthropisation](https://fr.wikipedia.org/wiki/Anthropisation). C'est à l'échelle écologique la même chose que l'ethnocentrisme colonialiste à l'échelle des peuples et des races sociales.

Plus nous augmenterons notre empreinte sur les écosystèmes, plus nous les dérèglerons, et en subiront secondairement (mais à pleine puissance) les conséquences. La vraie urgence c'est d'augmenter l'anthropocène positive, par réduction de l'anthropisation. Et ça ne passe que par un mélange très humain de résilience, d'autonomisme, de combat contre l'esprit bourgeois que nous portons toutes et tous, aussi.

A l'échelle individuelle, qu'est-ce que cela signifie ? Eh bien peut-être reconquérir les savoir-faire d'avant l'industrialisation, à tout point de vue : faire son pain, ses produits ménagers, limiter les dimensions de son espace de vie, réparer les choses, acheter acheter moins, et surtout d'occasion... La liste est longue.

Elle est d'autant plus longue que nous partons d'extrêmement loin. Devant la sensation d'avoir un mur devant soi, infranchissable et incassable, il faut s'armer de patience et de témérité. Pour ma part, je sais bien que je n'y arriverai jamais pleinement, complètement. Mais si je peux apprendre trois bricoles et demi à transmettre à mes enfants qui en apprendrons trois de plus, c'est déjà une petite victoire !
