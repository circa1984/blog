---
layout: post
title: J’ai testé Solus OS (et je l’ai adopté)
date: '2017-06-28 20:14:00'
desc: Je suis tombé sur Solus OS. En quelques jours, j’ai été conquis. Voici un rapide tour du propriétaire.

---

On peut le dire : non, Linux n’est pas un truc facile au début. Quand on vient d’ailleurs, c’est déroutant. Désarmant même. Si pour ma part je peux être séduit par l’idée de devoir taper un peu de code pour installer des trucs, j’imagine mal que ce soit le cas de tout le monde…

Cela dit, je suis tombé sur [Solus OS](https://solus-project.com/). En quelques jours, j’ai été conquis. Voici un rapide tour du propriétaire.

* * *

### Chez Solus OS, ils se vendent bien les coquins !

J’ai entendu parler de cette distribution (entendez « version de ») Linux par quelques contacts sûrs qui en disaient du bien sur les réseaux sociaux. J’ai donc sauté le pas. C’est pas que j’étais gêné ou pas à mon aise sur Ubuntu (en l’occurrence 17.04, cf [mon article sur le minimalisme digital](https://circa1984.net/2017/02/03/minimalisme-digital/)). Je sentais juste que je pouvais trouver « mieux » ailleurs. Ubuntu était mon entrée dans le monde linuxien, je ne pouvais de toute façon pas m’arrêter à lui, et j’avais envie de tester, en bon petit geek du dimanche que je suis.

Sur son site web, très bien foutu et très clair du reste, Solus OS décrit parfaitement bien ce qu’il propose. Quelque chose de fluide, cohérent, conçu pour une utilisation domestique. [Sur cette page là](https://solus-project.com/solus/about/) figurent les arguments qui m’ont conquis : Solus OS est designé pour les touches à tout, avec de la bureautique, de la création de contenu multimédia, une (petite) plateforme de jeux, et de quoi satisfaire les devs. PAR-FAIT.

On a le choix entre trois versions. En effet, Linux propose globalement plusieurs interfaces, et Solus en profite. Ici, c’est Budgie (je dirais très Mac), Mate (présenté comme traditionnel, Windowsien), et Gnome, mon petit favori en matière d’interface Linux. J’ai donc téléchargé la version Gnome, et _si on passe sur les problèmes liés au fait que je sois un boulet qui aime installer ses trucs un samedi soir alors qu’il est KO technique et qu’il se lève tôt le dimanche matin pour bosser,_ l’installation est un jeu d’enfant !

* * *

### La distribution elle même, et ses features :

Typique de Gnome, le bureau est extrêmement épuré. Je l’ai légèrement affiné avec quelques extensions ([très faciles à installer via Firefox](https://extensions.gnome.org/) notamment) pour obtenir le résultat ci dessous.

A gauche, un dock avec les applications favorites. En haut au centre la date, et en cliquant, l’agenda et les notifications. A droite, on retrouve les boutons d’arrêt de l’ordinateur, la gestion du réseau, du son, ainsi que l’extension Internet Radio (chanmée !) et autres. Il y a normalement en haut à gauche un menu « Activités » qui ne me sert à rien et que j’ai donc enlevé…

Du coté de l’interface des applications, les utilisateurs de smartphones (surtout ceux sur Android, qui est basé sur Linux…) ne seront pas dépaysés : de grandes et lisibles icônes, qu’on consulte soit dans leur ensemble (« Toutes »), soit par la fréquence d’utilisation. Et la petite barre de recherche qui va bien…

Et comme vendu sur le site, Solus s’avère effectivement user-friendly, pratique, sans non plus dissoudre les choix et options et faire de nous des bébés… Le meilleur exemple est le Software Center, nettement mieux foutu que sous Ubuntu. Il résume en fait la proposition de Solus OS, sa volonté de concilier les usages techniques, créatifs, et bureautiques.

On y trouve donc une base triée par grands thèmes, un outil de recherche précis, ainsi que des propositions via les partenaires (coucou Bitwig \<3). La base est très riche, et il ne faut pas hésiter à utiliser la fonction de recherche.

* * *

**En bref, pour conclure :**

Super efficace, sans plantage aucun, gérant parfaitement mon dual boot, Solus OS m’a convaincu grandement, en quelques jours. Je vous la recommande chaudement si comme moi vous pouvez sauter d’un film à la création d’une prez, en passant par de la créa (web ou image)… Versatile et accessible, j’ai découvert grâce à lui des logiciels open-source qui pourraient me permettre (toujours en cours de test…) de me passer de la suite Adobe… Et c’est pas peu dire !

Si Solus OS vous tente, ça se passe ici : [https://getsol.us/home/](https://getsol.us/home/)
