---
layout: post
title: Les Sneakers
date: '2017-02-28 18:42:00'
desc: La culture des sneakers est le symbole de l’incompréhension de masse pour le concept de matérialisme. Pourtant, j'en porte tous les jours.
---

Je suis commerçant, j’ai un métier physique, je suis debout toute la journée, je marche, je porte du poids… Il y a deux ans de cela, quand j’étais dans une branche plus numérique et sédentaire, je ne portais quasiment que des boots… Aujourd’hui c’est impossible : pour le bien de mon dos, de mes jambes, je suis passé aux sneakers (le « nouveau » mot pour dire basket).

Cela dit, j’y suis allé à reculons, tant ce marché symbolise à peu près tout ce que je déteste dans l’industrie mondialisée et agressive : les délocalisations, les conditions de travail des ouvriers, le travail des enfants, l’empreinte carbone, les animaux, et puis également le fait que ces chaussures sont souvent d’avantage courtisées pour leur valeur symbolique que leur valeur réelle…

Mais j’ai dû me positionner… et si ce n’est évidemment pas facile, c’est en tout cas très révélateur de bien des choses pour celles et ceux qui comme moi essaient de mettre du sens dans leur mode de consommation.

* * *

### Sneakers et matérialisme

Si j’en adore le confort dans mon usage quotidien, il y a quelque chose que je n’ai jamais compris dans la culture des sneakers. Elle même provenant du hip hop, une culture que j’aime encore croire révolutionnaire et anti-conformiste (coucou l’utopie !), la culture des sneakers est le symbole de l’incompréhension de masse pour le concept de _matérialisme_.

On résume aujourd’hui le matérialisme en disant qu’on accorde trop d’importance aux choses et aux objets (ici, les chaussures), mais en réalité ce n’est absolument pas le cas, puisque ça sous-entendrait que nous leur accordons un sens matériel alors que les sneakers ont bien (trop) souvent uniquement un sens symbolique (de richesse, d’appartenance, de mode… Il y a des gens qui spéculent sur des paires de chaussures !). Mais, le matérialisme n’est il pas plutôt de prêter à un objet un sens strictement usuel, utilitaire, et donc, la palisse…, matériel) ?

Alors évidemment, le marketing n’aide pas… Il est difficile d’ingérer autant de campagnes publicitaires multi-formats sans en venir à la quasi-idolatrie de l’objet. J’y suis moi même un peu sensible… Sinon, je n’achèterais pas de jolies sneakers, mais plutôt des sabots Croc’s !  
Mais avec un peu de distance, j’arrive à me rappeler mes engagements naturels : comment éviter de succomber de trop à l’appel du symbole, et de refuser de voir la réalité derrière l’objet (condition de travail, matières animales, etc).

### Sneakers, éthique, et tarifs

Mon engagement en mouvement, sur l’éthique, se place de deux cotés quand il s’agit des sneakers : d’une part les conditions de travail des ouvriers (et notamment le travail des enfants), et d’autre part la cause des animaux, qui sont dans les premiers rangs des victimes de ces industries…

J’en suis donc à jongler entre des paires estampillées « vegan » ou « PETA approved », et des paires ou marques qui fabriquent dans des pays (de préférences peu éloignés) où le code du travail est décent (la liste s’amenuise de jour en jour…). Pas évident DU TOUT !  
Et trouver les deux, sans égratigner non plus les notions de confort et d’esthétique (on est en 2017, tout cela DEVRAIT être possible !) ??? IMPOSSIBLE.

Et d’ailleurs, j’en discutais l’autre jour avec un ami, grand amateur de sneakers lui même : il est très compliqué d’imaginer une marque française produisant en France des sneakers de toute beauté sans faire de mal à une seule espèce animale !

Il faudrait pour que cela soit possible, un ajustement drastique de la fiscalité des entreprises françaises ou une contrainte d’impôts pour toutes celles (étrangères) qui commercialisent sur le sol français, une relance de l’industrie textile (des ateliers, des fournisseurs, des producteurs…), une taxation des produits importés (pour éviter les différences de prix en magasin), et autres bricolettes qui ne déboucheraient en plus que sur un hypothétique succès face aux marques déjà très implantées dans l’inconscient collectif !

* * *

### Appel à toutes les unités

A ce titre, et en attendant que la petite utopie développée juste au dessus prenne forme (LOL), je lance un appel (bouteille à la mer) :

- vous êtes sneakers head
- vous avez un peu de temps
- vous êtes sensible à la cause animale et à un droit du travail digne de ce nom

Postez en commentaires vos liens vers des nouveautés, des actualités, des paires, des bons plans, qui correspondent aux critères discutés dans cet article ! Un référencement s’impose, et il est difficile pour les marques et les distributeurs de le faire… Donc n’hésitons pas à le faire nous mêmes !

A vot’ bon cœur !
