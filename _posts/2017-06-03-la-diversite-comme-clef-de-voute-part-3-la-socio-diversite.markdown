---
layout: post
title: 'La Diversité comme clef de voûte, part 3 : la socio-diversité'
date: '2017-06-03 17:22:00'
desc: Après avoir discuté biodiversité et sapio-diversité, il me parait nécessaire de compléter ces deux aspects, l’un extra et l’autre intra, par l’échelle de la société et des sociétés. Cet article va donc parler de socio-diversité.
---

Il y a quelques temps, j’ai abordé pendant deux billets des concepts liés à la diversité. La biodiversité d’abord, sorte de curseur du bien-vivre sur cette planète ou témoignage des urgences qui n’attendent que nous. Et puis la sapio-diversité, puzzle complexe de ce que chacun et tous ont de différent et de commun dans la construction et la structuration de leur savoir, de leur réflexion, de leur évolution intellectuelle…

Il me parait donc nécessaire de compléter ces deux aspects, l’un extra et l’autre intra, par l’échelle de la société et des sociétés. Cet article va donc parler de socio-diversité.

* * *

### Socio-diversité, sur deux niveaux

On parle de deux choses, distinctes mais juxtaposées et concomitantes : l’échelle d’un groupe social défini (une société entière, comme la société française par exemple), et l’échelle globale, c’est à dire le tissu de toutes les sociétés humaines.

Pour l’échelle du groupe, il ne s’agit pas là de ne parler que de classes sociales. Ce serait presque (lol) trop simple et surtout peu représentatif de la diversité réelle d’une société. La notre, en France, quoiqu’on pourrait (oser) étendre le constat à l’ensemble ou presque des sociétés occidentales, est devant des faits sociétaux, des réalités, qui demandent d’extrapoler encore d’avantage le phénomène de classe. Quand bien même on conserverait un angle structuraliste dans l’approche… De quoi parle-t’on ici alors ? De diversité de sexe, de genre, de race, de classe, de chance. Des gros mots. Qu’on ose parfois à peine prononcer sans pincettes. Tout du moins quand on a pas encore conscience de leur effet principal : la création de préjudices et de privilèges inhérents à chaque « place » au milieu de toute cette équation.

#### 1 – La « petite » échelle : bref exemple de la société française

La société française, si on la photographie dans l’instant actuel, reflète son histoire récente (dernier siècle + siècle en cours) et ce qui reste de plus impactant dans l’ensemble de son histoire culturelle, territoriale et structurelle. Nous avons à faire avec les fruits hérités de décennies d’ « évolution », plus ou moins lente, selon l’endroit où nous portons le regard.

Pour schématiser, nous pourrions utiliser les étiquettes en vogue : multiculturelle, mixte, plurielle, riche. Ce que cache ces étiquettes (souvent d’avantage propagandistes qu’autre chose, d’un coté ou de l’autre d’ailleurs…) ne m’intéresse pas. Mais ce qu’elles tentent de montrer ou de désigner oui.

Une sorte de mosaïque en 3D, avec différentes couleurs, différentes formes, et différents reliefs. Mais tout cela en mouvement, en changement, en transition. Soit par un travail quasi géologique (intrinsèque), soit par celui de l’artisan (politique en l’occurrence, systémique).

Ces rythmes de mouvements ont pour conséquence principale de restreindre ou d’augmenter la capacité d’adaptation confortable de chaque pièce dans la mosaïque. Si le rôle du système artisan est déterminant sur les grandes orientations, celui ci influe aussi sur l’activité intrinsèque et géologique de la mosaïque. Il crée des changements de confort, donc des tensions et des frustrations dont l’intensité (et sa gestion) révèlent pleinement le jeu des privilèges et des préjudices.

Une image concrète et réaliste : je suis un trentenaire blanc en couple, en CDI – et mon salaire me permet de vivre convenablement – chrétien disons silencieux, résidant un quartier très calme de la proche banlieue parisienne. Il me semble difficile d’imaginer que mon sort (c’est à dire ma place dans la mosaïque, les mouvements que je subis en occupant cette place, et les frustrations qui en découlent) est égal ou pire à celui, par exemple, d’une jeune femme musulmane (pratiquante) issue de l’immigration vivant en banlieue éloignée, au chômage, célibataire. Nous ne vivons pas le même confort, nous ne partageons pas le même parcours, nous ne subissons pas les mêmes violences et les mêmes oppressions, et notre accès au changement de place dans la mosaïque ne requiert pas la mème quantité d’efforts. Des contingences élémentaires qui forment à la fois nos vies, mais aussi nos esprits (sapio-diversité, on y revient).

Nos différences sont clefs. Elles sont autant de facteurs décisifs dans ce qui pourra être épanouissant pour l’un et terrible pour l’autre. Cette décision, quasi Damoclès, n’appartenant globalement qu’aux pouvoirs publics (artisan de la mosaïque, vous avez capté), nous pouvons et devons questionner leur responsabilité, dès lors qu’un groupe subit d’avantage qu’un autre… Mais ce n’est pas mon sujet ici (même en période post-électorale)…

#### 2 – L’échelle globale : la mise à plat anthropologique

A l’échelle du monde, l’ensemble des sociétés présentes à l’heure actuelle sur la planète tisse un canevas impressionnant de diversité tant de structures que de projets. Et évidemment d’effets sur les femmes et hommes les constituant.

J’ai eu la chance, par mon cursus scolaire anthropologique, d’avoir eu accès à une partie de cette diversité. Et via mes professeurs et mes cours, de comprendre l’enjeu de sa préservation. Tout comme l’UNESCO se charge de préserver le patrimoine des créations matérielles, il est essentiel à mon sens de recenser et de cultiver cette diversité.

Dans une politique mondiale généralement plutôt orientée vers une normalisation et un alignement tant des valeurs que des manières et des procédés, ce vœu en devient presque pieu. Pourtant, comme pour la préservation de la biodiversité et la compréhension de la sapio-diversité, c’est une des clefs, je trouve, dans la transmission d’un monde « correct » aux générations qui arrivent.

Mon challenge personnel vis à vis de cela, c’est de me considérer dans l’incapacité de juger quelconque structuration de société autre que celle dont je suis issu. Une forme de modestie, accompagnée de bienveillance, à l’égard d’abord des autres mœurs et coutumes, et du rythme qui régit les sociétés dans le monde.

Ce n’est pas DU TOUT un challenge facile.

* * *

### Le concept de richesse sociale

J’y crois. Mais j’y mets beaucoup de mais. D’abord parce que c’est généralement vendu comme un produit _marketé_, avec derrière des projets politiques qui ne sont pas outre mesure sur ma ligne : je pense par exemple au regroupement familial de Bouygues et Giscard, dont les patrons applaudissaient à l’époque la manne financière que ramènerait une main d’œuvre si corvéable et bon marché, et dont la gauche surfe encore aujourd’hui sur la « mixité » culturelle que cela a créé dans les « quartiers ». (J’utilise les guillemets pour mettre en exergue ces mots politiques peu digestes)

La « mixité » d’ailleurs, est une idée tant vendue (quand elle n’est pas directement imposée) qu’on en a perdu le sens éthymologico-culinaire (je me marre tout seul en écrivant ce mot-valise) : mixer c’est hacher puis amalgamer. Je n’ai du reste rien contre le concept en lui même, finalement. Mais en tant que projet politique, on est in fine à l’opposé de la conservation de la diversité. Et là où la diversité est un signe de richesse, la mixité comme projet politique est donc de facto un signe d’appauvrissement (particulièrement par le bas). Et vouloir l’imposer demeure extrêmement violent.

Et nous arrivons très vite dans un débat qui malheureusement a été hijacké par les néo-libéraux globalistes d’un coté, de droite comme de gauche (ou les deux en même temps, désormais), et de l’autre par les identitaires (plus ou moins rasés de près d’ailleurs). Tellement malsain d’abord, et puis surtout borné et lesté par des propositions / solutions essentiellement « absurdes » (en tout cas selon moi… c’est un autre débat), d’un coté comme de l’autre des deux camps.

* * *

### La diversité sans être ni un citoyen du monde, ni un identitaire

La seule chose commune à ces deux camps là, c’est le nombrilisme. Cette faculté exceptionnelle de considérer que SOI est un exemple pour tous, un summum créé soit par le culte de l’identité enraciné (le fantasme de la race française ou européenne, élue, pure, appuyée par les mythologies), soit par celui du « mieux émotionnel » (le célèbre devoir moral de Jules Ferry à propos de la colonisation, dont découle aujourd’hui les longues tribunes éditoriales et les actions gouvernementales interventionnistes). On pourrait même dire _anthropocentrisme de société_ : l’homme d’une société donnée est au centre de l’Univers, et tout (les autres hommes, les autres sociétés) dépend de lui.

Quand je pense à ça, j’ai souvent l’image de l’agent Smith dans la trilogie Matrix. La duplication à l’infini d’un modèle unique (par la force ou par des portes dérobées) et uniformisant. Moi, ça me colle un cafard terrible…

<figure class="kg-card kg-image-card"><img src="/content/images/2018/11/b_1_q_0_p_0-jpg.gif" class="kg-image"></figure>

Mais finalement, éviter d’être dans un camp ou l’autre est assez « simple », quoique pas forcément aisé… Parce que c’est plus naturel qu’on pourrait le croire, et dénué de propagande quelconque, mais que cela requiert un certain goût pour l’inconfort : oui, il y a ce moment où on a cru que j’étais un dangereux nationaliste (on est d’ailleurs de plus en plus à s’entendre dire l’expression à la mode : _« tu fais le jeu du FN »_), un autre où j’étais un hippie démoniaque (quelle idée aussi, de dire que les papous ne sont sans doute pas plus malheureux sans la technologie), ou bien un scientifique malsain dénué de poésie (que c’est froid d’imaginer un monde où le fantasme mythologique ne guide pas l’action collective d’une société)… Bon, moi je suis fromager, pour info. Basta…

Il est d’abord nécessaire de s’absoudre des élans émotionnels que la plupart des actualités journalistiques provoquent en chacun de nous. Se détourner du logiciel avec lequel nous avons appris à construire notre pensée, en choisir un libre. Ça n’est clairement pas facile. D’autant qu’avec Facebook et Twitter agissant comme des amplificateurs, sans parler des habituels médias presse (TV, presse, sites web), l’ambiance devient rapidement anxiogène et suffocante.

Cela va de pair avec l’acceptation de son échelle d’action individuelle: le proche, le local, et le présent. Et du sens réel du mot action : je ne crois pas aux pétitions, [je ne crois pas au vote](http://circa1984.net/non-merci-je-vais-mabstenir/), je ne crois plus au modèle d’action et d’activisme proposé par le système. En revanche, créer ex-nihilo, faire tout avec rien, inventer, en se mettant au service de son échelle et de l’intérêt général, alors là, bien des choses ont un effet remarquable.

La socio-diversité, sa compréhension et sa préservation, sont autant de voies pour moi de devenir agissant et modeste (et sur ce deuxième point, je pars avec un capital relativement mince… Ahah). Questions basiques :  
– Qui suis-je pour juger réellement de l’impact positif ou négatif de la politique d’un dirigeant sur un pays qui n’est pas le mien ?  
– Qui suis-je pour éprouver de la pitié (ou de l’extase !) à l’égard de sociétés rudimentaires (j’emploie le mot de Clemenceau dans sa réponse à Ferry, je le trouve tellement pertinent !) où chacun sait cultiver, éduquer, transmettre (et parfois soigner) mais où la TV n’existe pas ?

Mettre en marche de petites choses libres et ouvertes, avec des gens qui s’y retrouvent aussi, et permettent ainsi d’amalgamer des solutions et leurs adaptations possibles partout : **c’est pile ça** , ma conception de la (création de) richesse.
