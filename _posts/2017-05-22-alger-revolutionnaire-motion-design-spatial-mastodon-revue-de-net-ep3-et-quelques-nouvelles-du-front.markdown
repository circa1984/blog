---
layout: post
title: Alger révolutionnaire + Motion Design spatial + Mastodon => Revue de Net ep3
date: '2017-05-22 15:49:00'
desc: J’ai croisé deux trois bricoles sur le net qui valent le détour. D’un documentaire Arte à un court métrage en motion-design, un régal.
---

Bon, il se passe un tas de trucs en ce momentum post-électoral, mais personnellement, je n’y trouve aucun intérêt…

J’ai même désinstallé l’app FB de mon téléphone, ce qui représente grosso modo 70% de mon temps sur cette plateforme, libéré pour d’autres choses. L’ambiance y était trop angoissante, anxiogène. On est clairement mieux sur Mastodon… mais je reviendrais là dessus plus bas dans ce post…

J’ai croisé deux trois bricoles sur le net qui valent le détour. D’un documentaire Arte à un court métrage en motion-design, un régal.

* * *

### ALGER, la Mecque des révolutionnaires (1962-1974), sur ARTE+7

Si on peut considérer qu’Arte+7 est de loin le meilleur pourvoyeur de contenu streaming et replay, ce reportage / documentaire en est le plus bel exemple. Mise à part une voix off fade à souhait, le documentaire met en lumière comment cette ville, et toute l’Algérie fraîchement indépendante, est devenue le nouveau centre mondial des luttes révolutionnaires.

Une leçon de géographie passionnante, de géopolitique aussi, et une fenêtre inspirante sur l’histoire de la révolution (et de la collaboration entre les peuples et les mouvements) au sens large et noble du terme. Une des histoires dingues de la mise en commun de luttes excentrées, éloignées entre elles, mais qui se retrouvent en un point pour partager des moyens, des outils, et des réflexions. _([cliquer ici](https://www.arte.tv/player/v3/index.php?json_url=https%3A%2F%2Fapi.arte.tv%2Fapi%2Fplayer%2Fv1%2Fconfig%2Ffr%2F055150-000-A%3Fautostart%3D1%26lifeCycle%3D1&lang=fr_FR&config=arte_tvguide&embed=1&mute=0) pour accéder au docu)_

* * *

### LUNAR, de Christian Stangl

Motion Designer, Christian Stangl (accompagné de son frère Wolfgang qui signe la BO) offre un court-métrage qui nous emmène sur la Lune avec les astronautes américains… Un travail minutieux et super esthétique, dont je me lasse pas.

<figure class="kg-card kg-embed-card"><iframe src="https://player.vimeo.com/video/217051213?app_id=122963" width="640" height="360" frameborder="0" title="LUNAR" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></figure>
* * *

### MASTODON, enfin un réseau social normal

Facebook et Twitter, les deux géants… Personnellement, plus ça va et plus je les trouve obsolètes : les algorithmes ne me proposent jamais rien d’intéressant. je suis obligé de faire moi même toute une série de clics pour trouver du contenu qui retient mon attention… Sans compter le climat ambiant, les pubs, et le respect de la vie privée et de nos données…

Numerama a fait une excellent article présentant Mastodon, son usage, son principe. Ça m’évite de répéter ce qui est mieux dit ailleurs : [Début sur Mastodon… par Numerama](http://www.numerama.com/tech/246684-debuter-sur-mastodon-9-questions-pour-tout-comprendre-au-reseau-social-decentralise.html)

Je suis personnellement inscrit sur l’instance de la Quadrature du Net (dont j’épouse pleinement les valeurs et les positions relatives à internet et au libre). L’instance s’appelle [Mamot.fr](https://mamot.fr), et héberge une communauté vivante, plutôt drôle, majoritairement geek mais en grande recherche de diversification des membres. **Alors si vous êtes artiste, blogger, cuisinier, ou n’importe quoi d’autre, vous êtes bienvenu**!

D’un point de vue de l’utilisation, la page web ressemble à Tweetdeck, avec des colonnes permettant la navigation entre le fil principal (les comptes aux quels je suis abonné), les notifications, et le fil général (où on peut switcher en un clic d’un contenu global provenant de toutes les instances fédérées dans le monde, un peu genre Twitter en fait, ou le contenu local, qui ne regroupe que le contenu posté dans l’instance où on est inscrit).

Sur smartphone, j’utilise le client Fedilab (il y en a plein d’autres cela dit). Pratique, et bien foutu. Il est en constant développement et prends vraiment en compte les retours des utilisateurs, ce qui franchement, donne beaucoup d’espoir.

Mastodon est en fait le réseau social comme toutes ces plateformes devraient l’être : libre, respectueux, ouvert, et participatif. je n’ai même pas l’impression d’être inclus à quoi que ce soit de révolutionnaire. J’ai juste trouvé un endroit sur internet où les choses se passent normalement. Et je vous invite grandement à y venir !
