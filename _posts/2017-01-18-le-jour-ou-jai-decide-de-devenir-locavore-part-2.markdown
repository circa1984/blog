---
layout: post
title: Le jour où j’ai décidé de devenir locavore, part 2
date: '2017-01-18 18:00:00'
desc: Après avoir évoqué qu’il s’agissait d’avantage d’un processus continu dans le premier article, je voulais vous parler des solutions que j’ai trouvées de mon coté, à Paris. Mais avant cela, petit retour philosophique sur l’empreinte de l’Homme sur la planète…
---

Après avoir évoqué qu’il s’agissait d’avantage d’un processus continu [dans le premier article](http://circa1984.net/le-jour-ou-jai-decide-de-devenir-locavore-part-1/), en remarquant que le mouvement végan manquait peut-être (je ne demande qu’à en savoir d’avantage si je me trompe !) de lucidité sur la réalité de l’impact que ce type de consommation a sur nos écosystèmes (par la nécessité physiologique d’accéder à des denrées produites loin, l’impact sur la planète et donc les espèces n’est peut-être pas si faible que cela ?

Le mieux est l’ennemi de bien…), je voulais vous parler des solutions que j’ai trouvées de mon coté, à Paris. Mais avant cela, petit retour philosophique sur l’empreinte de l’Homme sur la planète…

* * *

### Nous ne sommes ni seuls, ni uniques, ni identiques

Deux questions reviennent souvent dans les débats ou les discussions sur ces sujets :  
– L’Homme doit-il être un assassin pour se nourrir (et par extension pour subvenir à ses besoins) ?  
– Les autres espèces sont-elles des assassins quand elles chassent / tuent pour se nourrir (et subvenir à leurs besoins) ?

SI je comprends le reflexe de ces deux questions, et les interrogations qui en découlent, j’ai aujourd’hui (ça n’a évidemment pas toujours été le cas…) du mal à y répondre frontalement tant elles me semblent très peu liées à une réalité pourtant très palpable : nous ne sommes ni uniques, ni identiques.  
Dans l’évolution de l’Homme sur la planète, les différents écosystèmes où il s’est installé ont façonné ses savoirs et ses savoirs-faire, et donc ses habitudes, ses habitats, son alimentation, ses mythes, ses références, mais aussi et surtout, ses besoins physiologiques et biologiques. Nous sommes, partout et différemment, construits par les différents écosystèmes qui nous entourent.

Alors comment pourrions nous imaginer appréhender l’alimentation par un seul biais moral ? Je n’aurais pas du tout l’idée de juger moralement une population qui pour ses stricts besoins physiologiques doit élever ou chasser des bêtes et en tirer un maximum de denrées…  
Par contre, il me parait tout aussi difficile de ne pas me sentir mal à l’aise quand on reproduit la même chose à l’échelle d’une société industrialisée où le « manque » ne justifie pas ou plus la quantité produite…

Et pour tenter de répondre à la première question, il n’y a donc à mon sens ni la dimension de devoir ni celle d’Humain (au sens « espèce ») dans l’alimentation, mais celle de besoin. Et c’est lorsque le besoin ne justifie plus rien qu’on bascule dans des choses nocives et dégradantes puisque uniquement justifiées par la rentabilité et le profit. L’assassin est donc d’avantage un système, dont l’ingénierie est aussi fine qu’efficace. Un système extrêmement dur à hacker et déconstruire.

Quand à la seconde question, j’y réponds généralement en me disant que se poser ces questions ne fait pas avancer grand chose dans ce débat, ni dans un sens ni dans l’autre… Effectivement, je me dis que la chaine alimentaire naturelle ne conçoit pas la morale. Mais dès lors que la société de consommation construit une nouvelle chaine alimentaire ex nihilo, avec des rapports de domination outrancière et non de cohabitation et de collaboration (parce que je crois que l’élevage, sur ses bases archaïques en tout cas, s’apparente à la relation du poisson pilote avec le requin : nous tirons profit des bêtes qui en retour trouvent une sécurité et une protection face aux prédateurs), sans penser à la préservation et à la diversité des écosystèmes et des ressources, je me sens la responsabilité de construire cette ligne morale, et de hacker ce système. Autant que faire se peut. En sortant de mes zones de confort, et en participant, quelque soit la manière, à des initiatives qui ont compris ces enjeux et qui proposent des solutions.

* * *

### Ce que ça signifie pour moi au quotidien :

Chacun son emploi du temps, sa localisation et ses capacités de déplacement. Pour ma part je vis en proche banlieue parisienne, je travaille dans Paris, et je suis commerçant (je bosse le week-end). Il m’est donc compliqué de faire les marchés, de passer chercher un panier AMAP chez un autre commerçant (puisqu’il ferme en même temps que moi bien souvent) etc etc. Je gagnerais sans doute un temps et une thune monstre en me faisant livrer via le site d’une grande surface non ?  
FAUX.

En arrivant en région parisienne, je me disais que j’allais essayer de retrouver l’alimentation que j’avais quand j’allais en AMAP (via [celle des Ponts-Jumeaux](https://amappontsjumeaux.wordpress.com/)) à Toulouse. J’avais aussi essayé des solutions quand j’habitais à Orléans, notamment [Les Paniers de Anne](http://www.lespaniersdeanne.com/). Sachant que je n’achète ni poisson ni viande, je me concentre surtout sur des légumes et légumineuses (en grande variété), des pâtes et du riz. J’ai la chance aujourd’hui d’être crémier-fromager, je vous fait pas un dessin sur cet aspect là…

Et puis ici, j’ai rapidement découvert l’existence d’[AU BOUT DU CHAMP](http://www.auboutduchamp.com/). Un réseau de petites échoppes en Ile de France, où le concept est très simple : en direct des producteurs dans un rayon de 100km, fruits et légumes ramassés le jour même (par la même équipe, qui file donc un coup de main aux producteurs), en bio et paysan. Rajoutez à cela que l’équipe est jeune, sympathique, et en constante croissance (ils recrutent)… Parfait, j’avais trouvé où me fournir.

* * *

### La route est encore longue !

Ce qui est certain, c’est que je refuse de m’inventer l’ambition et la passion de vouloir sauver la planète, mais que j’embrasse (et ce depuis longtemps) la volonté de vivre en adéquation avec elle, dans mon espace, mon temps, et à mon échelle. A quoi bon faire autrement ? D’ici 90ans, la diversité sur cette planète ne sera pas suffisamment abondante et florissante pour que la Terre puisse encore soutenir la vie. Les enfants des trentenaires d’aujourd’hui auront donc le loisir d’être témoins d’un scénario à la _Interstellar_. Le seul moyen de contredire ces prédictions et d’essayer de leur offrir un futur viable est de faire l’effort au quotidien de vivre d’avantage en adéquation avec l’écosystème qui nous entoure. Alors c’est sûr, ça demande de se résoudre à perdre des habitudes, du confort, de reprogrammer le temps qu’on alloue à une chose ou une autre, mais bon… C’est pas juste pour faire joli sur instagram ou sur un blog foodie…

Si j’ai la sensation d’être pas trop déconnant quand je vais chez [Au Bout Du Champ](http://www.auboutduchamp.com/), il y a évidemment dans mes placards des trucs que j’aimerais à terme remplacer, sachant très bien que je pourrais leur trouver des substituts tout aussi gourmands (tant ils relèvent du confort d’avantage que de l’apport nutritionnel… je suis pas un moine non plus !). Mes boissons de riz ou de soja (je ne bois pas de lait) pour mes chocolats chauds, mon riz et mes pâtes achetés en grande surface, sont les principaux produits que j’aimerais ne plus consommer de cette façon. Je cherche des solutions… locales…  
Et si vous en avez, je suis évidemment preneur !
