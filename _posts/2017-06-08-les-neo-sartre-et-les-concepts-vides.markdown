---
layout: post
title: Les neo-Sartre et les concepts vides
date: '2017-06-08 19:42:00'
desc: C’est l’histoire un peu folle de plusieurs générations de types qui éditos après éditos, bouquins après bouquins, interviews après interviews, ont dessiné et creusé les traits de la pensée usuelle française comme nous la connaissons en ce moment. Et je l'aime pas beaucoup.
---

C’est l’histoire un peu folle de plusieurs générations de types qui éditos après éditos, bouquins après bouquins, interviews après interviews, ont dessiné et creusé les traits de la pensée usuelle française comme nous la connaissons en ce moment.

Quand je dis pensée usuelle, je parle majoritairement de celle qui sévit dans les médias, en politique, et chez les intellectuels qui trouve tribune (il existe certes de rares exceptions, agitées comme bêtes de foire et attractions vendeuses… Michel Onfray, si vous lisez ce post, je t’aime bien la famille).

Celui qui me parait le plus symbolique de tout ça, et je ne citerais pas son nom, se déclare bien (trop) souvent comme héritier de Sartre, ou en tout cas appréciateur, étudiant, fervent lecteur. Et c’est en me rappelant cela pendant que j’écrivais quelques articles pour ce blog, que je me suis posé la question : comment on en est arrivé là ? si bas.

* * *

### L’essentialisme et l’existentialisme sur mon échiquier

Oh moi vous savez, je n’ai rien contre rien, mais j’ai quelques préférences. Elles s’affinent avec le temps que je prends à organiser ma pensée article après article. Ce qui m’est clair aujourd’hui, c’est que j’ai besoin des contextes pour comprendre les choses, et que je ne peux plus me contenter des concepts abstraits : j’ai besoin de concret extrapolé en concept. Inversion de la méthode de création et d’analyse.

Alors c’est sûr, face aux essentialistes et aux existentialistes, je pèse moins le souhait et l’action individuelle que l’ensemble des systèmes et des structures qui y mènent, y contribuent ou les influencent. Je suis peut-être trop marxiste ? bof… Désenchanté ? bof… Et puis, j’ai bien dit que je pesais « moins ». Pas que j’oubliais. Je relativise, en fait, dans mes analyses, les concepts abstraits auxquels mènent les conceptions essentialistes et existentialistes.

Mais quand je me relis (ouais, c’est un article un peu psychanalyse, merci pour le sofa), il y a souvent en fin d’article une velléité à l’action partant de l’individuel au collectif… Ce qui finalement nous ramène au « choix » si cher aux sus-nommés.

Sur mon échiquier, ces deux façons de penser sont donc les dernières que j’active et mets en place. C’est l’organisation préalable de mon jeu, de mes autres pions, qui leur permettra de trouver leur rôle. je ne conçois que très peu de procéder autrement… J’aurais l’impression de ne pas tenir compte de mon adversaire, de sa stratégie. De ne pas prendre le temps de lire le jeu, de prévoir, de manœuvrer, de me couvrir.

C’est là tout mon problème avec les neo-Sartre, chevelu à chemise ouverte en tête de gondole : ils manœuvrent à la dure, force le jeu, coûte que coûte au final. Sans prendre en compte les contingences et les contextes, en réduisant le plus possible le concept à l’abstraction, frisant presque la novlangue d’Orwell par moment.

* * *

### Les concepts vides, ou vidés

Terribles, et hyper nombreux. Et surtout, omniprésents. Ce n’est pas comme s’ils étaient insignifiants et de seconde ou troisième zone dans les débats publics. Non, ils sont on-ne-peut-plus là :

- liberté
- démocratie

C’est tout de même vachement marrant (lol) de voir que ces mots n’ont jamais autant été utilisés par les neo-Sartre et affiliés (politiques compris) alors qu’ils s’éloignent de plus en plus du réel commun. La dé-réalité de ces mots là est proportionnelle à leur omniprésence médiatique, éditoriale, ou, pour sonner un peu anarchiste : propagandiste.

C’est dommage, je ne suis pas linguiste, quoique j’adorerais (quelles compétences extraordinaires !), mais tout de même, rarement des champs lexicaux n’ont sonné aussi abstraits autant que séduisants. Ce que personnellement je trouve très louche. Et c’est là tout mon reproche aux neo-Sartre : vendre une dé-réalité cachant bien souvent des horreurs (les nombreuses guerres soutenues sinon appuyées par nombre d’entre eux) avec des concepts vidés dans des discours séduisant de force, de malice, de verve. Un vrai danger public.

Et quid de la traçabilité des analyses et des propositions ? Comment peut-on remonter le fil d’une réflexion générale (médias + politiques principalement) où la plupart des mots clefs désignent du vide ? Sans repère, il est difficile de s’y retrouver, et c’est d’ailleurs là une des principales explications, à mon sens, de la perdition absolue dans laquelle se trouve l’intellect lambda, dénué de repère, de vision, de perspectives, adossé à un mur incompréhensible, abstrait, sourd. Il est aussi ardu sans balise de retrouver l’origine du problème, des problèmes, de remonter les engrenages idéologiques qui mènent parfois (souvent, trop à mon goût…) à des situations et des décisions catastrophiques.

* * *

### Les concepts détournés

L’exemple le plus marquant ici, c’est clairement la tolérance. Avec le temps, je comprends de moins en moins ce que cela signifie dès lors qu’on présente cette valeur ou ce concept comme un curseur positif, porteur de progrès ou de bon sens… parce que clairement, tolérer, au sens strict du terme, c’est permettre la présence quand bien même on ne soit pas d’accord (jusqu’ici tout va bien), mais avec une notion de pouvoir exercé, de regard (con)descendant, de passe-droit donné à l’objet toléré. Moi, personnellement, je ne trouve pas ça DU TOUT positif, et encore moins de l’ordre du bon sens.

Par contre, je trouve qu’effectivement, ça s’inscrit clairement dans cette tradition intellectuelle de l’abstraction, où tout est invité mais rien n’est proprement utile (neo-Sartre) avec ce soupçon de suprématie morale (coucou Jules Ferry / François Hollande). Décrite et définie ainsi, la tolérance me parait plutôt être un curseur de domination, et rien d’autre. Le tolérant distribue les bons points et les validations, le tolérés s’estime heureux de son sort. Quelle atroce mensonge caché derrière cette devanture de « progrès ».

Objectivement, il m’est très difficile de fait, de militer pour la tolérance. Je préfère manifestement cultiver la curiosité, la compréhension, et si possible l’analyse. Trois petits réflexes qui s’ils ne sont pas conditionnés, peuvent le devenir par l’exercice et l’habitude. Et qui mèneront à l’acceptation, la bienveillance, et une certaine notion de fraternité ou à tout le moins de bienveillance (si les réflexes sont réciproques). mais bon, cela requiert déjà trop de mots, trop de complexité, et la novlangue, ou en tout cas intellect actuel, exige la simplification jusqu’à l’abstraction.

* * *

### Quelques liens à propos de cette novlangue politique :

[Libé taille le PS à ce propos.](http://www.liberation.fr/france/2014/05/16/novlangue-et-politique-les-socialistes-comme-des-idiomes_1019200)

E. Chouard interrogé sur le sujet :

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/xAwCMgEVYQw?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>
* * *

### Pas de repères, pas de responsables

Une analyse des systèmes, des structures, a au moins le mérite de décrypter les rouages menant au factuel, offrant la possibilité de corriger, changer, rénover, ou renverser des mécaniques qui seraient sources de problèmes. Avec cet esprit abstrait et cet intellect de novlanguiens, impossible : pas de responsable décryptable, donc pas de responsabilité concevable.

Pas de responsables, pas de coupables. Le verbiage vide ou détourné, rendu abstrait, par l’absence totale et absolue de considération pour les contingences et les contextes, donne lieu à un trou noir du concret, et donc à des situations où tout ceux qui provoquent ont accès au vie de procédure. Ceux qui invoquent la guerre et l’amènent sont intouchables, ceux qui la conçoivent même, ne sont carrément jamais cités.

Je cite principalement la guerre en exemple, parce que c’est criant. Et le « criant », ce sont les cris des victimes, directes ou collatérales. Mais il y a d’autres champs où cela peut s’appliquer… L’état des médias, de l’information et de la profession de journaliste, à notre époque, calamiteux. L’absence complète de projet de société, conditionnant le manque d’inclusion de la population dans la vie collective, quelque soit l’échelle, et donc la décrépitude du tissu social, et du contrat social…

Alors non, clairement, l’essentialisme et l’existentialisme, leurs auteurs et artisans de l’époque n’en sont pas coupables. Mais sur les branches de ces deux arbres ont poussé des parasites qui aujourd’hui font beaucoup de mal à l’intellect français. Et si je ne m’étonne pas que Camus (mon gars sûr) avait refusé d’être étiqueté comme tel à son époque, je regrette que l’analyse des contextes et contingences ait pris autant de recul dans la sphère médiatique, que le concret soit tantôt absent, tantôt dénigré ou calomnié.

Avoir des intellectuels proposant du concret, créant des concepts à partir de la réalité, est une grande nécessité dès lors qu’on fait face à des challenges et des urgences comme nous en connaissons aujourd’hui. Et nous, qu’avons nous à la place ?

Pour finir, revenons sur les deux mots-concepts, liberté et démocratie, que je donnais plus haut en exemple. Voilà des définitions, moins dilatoires et plus concrètes, données à des époques où on y mettait encore du préhensible, en l’occurrence celui auquel j’adhère toujours.

> « _Je ne suis vraiment libre que lorsque tous les êtres humains qui m’entourent, hommes et femmes, sont également libres. La liberté d’autrui, loin d’être une limite ou la négation de ma liberté, en est au contraire la condition nécessaire et la confirmation. Je ne deviens libre vraiment que par la liberté d’autres, de sorte que plus nombreux sont les hommes libres qui m’entourent et plus profonde et plus large est leur liberté, et plus étendue, plus profonde et plus large devient ma liberté._ »  
> **Mikhaïl Bakunine** “_La liberté est un bagne aussi longtemps qu’un seul homme est asservi sur la terre._”  
> **Albert Camus** « _La démocratie est, en profondeur, l’organisation de la diversité._ »  
> **Edgar Morin**

Et parce qu’il faut toujours citer les grands auteurs (lolilol) :

> Moi même : " _La démocratie c’est l’amalgame des actions individuelles regroupées en mouvements collectifs dans la perspective de l’intérêt général."_
