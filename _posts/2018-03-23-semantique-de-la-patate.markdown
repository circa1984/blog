---
layout: post
title: Sémantique de la patate
date: '2018-03-23 15:25:00'
desc: Un article qui va parler de décolonisation, de vocabulaire et de tournure de phrase, de sémantique, d’appropriation de l’histoire, de réappropriation du langage, et toutes ces petites choses sexy que j’adore.
---

Encore un beau titre putaclick… bien raté ! J’ai longtemps hésité avec une référence au film Les Trois Frères, ou bien essayer d’employer le mot « frite » pour immédiatement avoir votre attention (et celle de ma femme !). Et puis bon, je restais bloqué avec le terme _sémantique_, véritable cœur du sujet de ce post. Donc de toute manière, j’allais dans le mur des posts à titres chiants, irrémédiablement.

Une bien longue introduction, pour finalement, un article qui va parler de décolonisation, de vocabulaire et de tournure de phrase, de sémantique, d’appropriation de l’histoire, de réappropriation du langage, et toutes ces petites choses sexy que j’adore. Sauf que, aujourd’hui, on va servir de la patate, son origine, son histoire, son projet, comme ellipse.

* * *

### Brèves histoires de la pomme de terre

Ma femme lui voue un culte et remercie à chaque frite ingérée l’ensemble des sud-américains de ce monde et des anciens pour l’apport de ce tubercule à l’humanité (anecdote à peine exagérée). En un mot, la pomme de terre est centrale dans nos vies, ça ne fait aucun doute. Cela dit, qu’est-ce qui nous est raconté de son histoire ? De sa provenance ? De ses origines ? J’ai pensé à ça hier, en passant devant un panneau, à la station Parmentier sur la ligne 3 parisienne.

Selon ce panneau, la pomme de terre fait son apparition en 1540 en Espagne donc, avec de vagues origines incas… Elle y est ramenée après une expédition menée par Pizzaro à laquelle participe donc le savant Pedro De Leon. remercions donc le dit Pedro, d’avoir ainsi offert la patate au peuple européen, quoi qu’en pensaient celles et ceux qui voyaient en elle quelque chose de démoniaque, ou de malsain.

Personnellement, je suis pris d’un sentiment inconfortable en lisant ce panneau. On me présente le truc sous l’angle de l’illumination scientifique, avec le soupçon d’exotisme que le terme conquistador confère. C’est beaucoup trop beau. Moi, dans ma tête, je m’imagine la découverte de l’Amérique du Sud avec une bande de gars affamés descendant de leur bateau sur ces plages, avec pour ordre du royaume espagnol de s’approprier terres, ressources et autres, à la moindre occasion… Parce qu’ici, à Valence ou à Bilbao ou je ne sais où, on crève fort la dalle et on a besoin de prouver aux autres européens que nos bateaux sont plus gros et vont plus vite (c’est ma vision de la colonisation, je suis prêt à en débattre au besoin)…

Alors quand je suis rentré chez moi le soir, j’ai bêtement regardé d’autres sources me racontant la dite « découverte ». Un mot qui revient si souvent… Et je trouve sur wikipédia une version qui n’a absolument rien à voir avec le panneau du métro :

> « La pomme de terre est originaire de la cordillère des Andes (Pérou) dans le sud-ouest de l’Amérique du Sud où son utilisation remonte à environ 8 000 ans… »

Quand même pas le même genre d’entrée en matière. Je me sens moins berné, je trouve ça moins curieux… Les tournures employées plus bas dans le wiki en font même l’héroïne de son expansion. La patate est une conquérante sur wikipédia, elle est un remède aux famines européens du 17ème siècle. On remerciera les sud-américains plus tard, pour avoir maitrisé le tubercule et sa culture il y a 8000ans… Mais nous ne sommes même pas capable d’inculquer aux gens une histoire qui tient debout, même ethnocentrée : tout le monde s’imagine que sans Parmentier, pas de patate, or le monsieur est né en 1737, quand la pomme de terre avait été définitivement admise comme propre à la consommation en 1722, mais domestiquée depuis le 16ème en Europe, et depuis 8 à 10 000 de l’autre coté de l’Atlantique…

* * *

### La sémantique du colon

Ce que cet exemple commun et quotidien m’inspire, c’est combien l’esprit colonial européen a pénétré fort le monde. Appropriation des découvertes des autres peuples, destruction de leur histoire et de leur culture, de leur génie même, réécriture de la vérité, sans aucune once d’humilité. C’était il y a des siècles, mais c’est encore aujourd’hui palpable jusque dans des couloirs de métro, sur des panneaux offerts à la lecture de toutes et tous. Et si c’est là, c’est que ce n’est pas un hasard : il y a une chaine éditoriale derrière la mise en place de tels panneaux. Des gens qui ont bossé sur le projet, rédigé et validé les textes. Des gens qui ne sont peut-être même pas conscient de combien l’esprit colonial est encore présent en nous tous.

Ici, il est surtout d’ordre sémantique. Des tournures de phrases, des choix de mots, de vocabulaire. Des orientations dans la rédaction, privilégiant un certain ordre d’information, des mises en avant, des percussions, qui donnent quand même bien l’impression que « nous », les occidentaux, avons littéralement découvert la patate ex nihilo. Le contexte préalable à cette découverte est souvent ultra sommaire, si pas complètement vacant ou absent. Rares sont aussi les infos qui permettent de mettre la dite découverte en perspective : y’a des gens qui ont masterisé la patate y’a 8 000ans, mais ce qui importe le plus, c’est qu’un Pedro l’ai découverte à une date très précise du 16ème siècle.

Quel est l’intérêt d’une telle sémantique ? Quelle est l’intérêt d’une telle réécriture de l’histoire ? Et quel est l’intérêt à se centrer autant lorsqu’on discute ou raconte une histoire qui de toute façon ne nous appartiendra jamais ?

Mon choix sémantique serait le suivant :  
La pomme de terre, tubercule sud-américain, fut rapportée par l’expédition d’invasion coloniale de Pizzaro au 16ème siècle\*. Cultivée depuis 8 000 ans en Amérique du Sud, il nous fallut un siècle pour apprendre à la domestiquer convenablement et ne plus douter de ses apports nutritionnels. Sa culture nous permettra de pallier autant que possible aux famines européennes du 17ème siècle. Et c’est Parmentier, en insistant sur ses apports nutritionnels, qui banalisera son nom.  
_\* les circonstances dans lesquelles la pomme de terre est à portée à la connaissance des colons restent obscures mais doivent être relatives aux principes coloniaux du royaume espagnol de l’époque._

* * *

### Rien n’est anodin pour celles et ceux qui sont oubliés

Et quand je dis oubliés, je parle de celles et ceux dont on efface l’existence, la réalité, la présence, le génie aussi, des tablettes. Juste avec un panneau et trois phrases d’intro. Et encore, il ne s’agit là que de pomme de terre. Et de civilisations disparues (anonymement, ou par la colonisation).

Mais combien sont les exemples qui touchent bien d’autres aspects de notre histoire, et des vérités toutes cheloues qu’on se raconte, dans les livres d’histoire à l’école, dans les magasines, les journaux, à la TV, la radio, les conférences…

A base d’ancêtres gaulois pour tous, idolâtrie de nos « entrepreneurs coloniaux » (Christophe Colomb par exemple), tout ça est tellement présenté comme banal et anodin. Comme si on avait planqué la partie immergée de ces histoires sous un tapis. Personnellement, je ne m’estime pas responsable de ce qu’il y a sous le tapis. Je ne porte pas en moi la culpabilité de ce que les colons européens ont fait au monde pendant des siècles. Et à ce titre, je ne comprends pas pourquoi ce tapis cache autant de choses. Nous aurions intérêt à tout mettre à jour, à tout mettre en lumière, à chercher un peu plus à décentrer les histoires que nous racontons, à soigner la présentation que nous faisons des faits, en recherchant une sorte de neutralité (la moins relative possible). La seule honte qui nous guette, c’est celle de laisser les choses en l’état.
