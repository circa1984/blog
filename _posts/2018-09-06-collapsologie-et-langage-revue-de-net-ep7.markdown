---
layout: post
title: 'Collapsologie et Langage : revue de net ep7'
date: '2018-09-06 05:50:00'
desc: Au programme, une série documentaire sur la collapsologie, un article (et un blog) passionnant sur des choses qu'on apprend à "éviter de dire", et un article fabuleux sur les communs comme modèle post-capitaliste. Go !
---

Un bel été, un article sur l'anthropocène, des choses plein la tête pour ce blog... Voilà comment se passe la rentrée ici. j'ai lu et croisé des tas de choses ces temps ci sur internet. Quelques unes via un excellent contact sur Facebook (coucou Alexandre !). mais principalement sur [Mastodon](https://joinmastodon.org/), où, je dois le dire, les quelques dernières semaines sont d'une intensité folle en matière de posts qualitatifs, d'effervescence dans les échanges. Le ton y est très agréable, les analyses et les avis sont toujours ouverts à discussion, la communauté que je côtoie est extraordinaire (merci à vous toutes et tous) !

Voici donc quelques liens, repérés ça et là sur les autoroutes numériques de l'information. Au programme, une série documentaire sur la collapsologie, un article (et un blog) passionnant sur des choses qu'on apprend à "éviter de dire", et un article fabuleux sur les communs comme modèle post-capitaliste. Go !

* * *

### J'évite de dire...

Ma vie perso est un exemple typique de ce dont l'article parle. Parce que j'ai pris conscience des **préjudices vécus par d'autres** et des **privilèges que j'avais** (liés à ma couleur de peau, mon genre, mon âge et mon statut social / pro), j'ai dû apprendre, non sans grandes remises en question, à changer de lexique, formuler autrement, travailler mon vocabulaire (entre autres). Il y a des choses que l'on dit, et qu'on devrait éviter de dire. [Ce blog et cet article](https://www.noschangements.fr/posts/2-liste-blanche-liste-noire-maitre-esclave/) parlent de cela, non pas sur le plan de la morale ou de la démonstration du pourquoi, mais **par le comment** , avec les exemples concrets mis en place.

* * *

### [NEXT] Série documentaire de Clément Montfort

C'est après la lecture d'un bouquin sur le sujet que Clément Montfort (ancien journaliste de France 2) a décidé de partir découvrir les collapsologues actuels. Ses rencontres, il en a fait [une série](https://www.next-laserie.fr/), toujours en cours, disponible intégralement là :

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/videoseries?list=PL6g6uC6ZfFJkfO1NACqSUUMRg_0AEB3rW" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

C'est évidemment un thème qui me branche. Non pas que je sois fasciné par l'effondrement, mais plutôt par **"demain", "après"**. Mon blog parle des alternatives et des solutions que nous pourrions mettre en place, nous aujourd'hui et nos prochains ensuite, pour **construire des sociétés humaines capables d'entretenir entre elles et avec leurs territoires des relations de collaborations saines et dynamiques**. Alors évidemment... la collapsologie me titille !

* * *

### Les Communs - le modèle de "l'après" capitalisme libéral ? - Myriam Bouré

Il y a des pépites sur [la page Medium de Myriam Bouré](https://medium.com/@myriamboure). De nombreux articles qui traitent d'un sujet qui me tient particulièrement à cœur : l'alimentation et l'agriculture / distribution de demain, mais aussi dela démocratie, des communs, de son travail pour [Open Food Network](https://openfoodnetwork.org/)...

Dans celui-ci, Myriam décrit comment la société des communs (je n'en ai jamais parlé directement ici, mais Maïa Dereva s'en occupe très bien [ICI](http://maiadereva.net/cest-quoi-ta-conception-des-communs/)) est l'option la plus adéquate pour répondre aux besoins humains et écologiques post-capitalistes. Cette anticipation sociologique, écologique et sensible de l'après société de consommation est EXACTEMENT ce que j'aime dans la collapsologie. Merci Myriam _(et au fait, tu devrais passer sur [WRITE.as](https://write.as/) ;) )_ !

Bonne lecture, et bon visionnage à toutes et tous !
