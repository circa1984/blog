---
layout: post
title: Sky, Thinkerview, complotisme et esprit critique
date: '2019-11-27 14:34:44'
desc: Depuis quelques mois maintenant, les informations sur qui est Sky de Thinkerview, pour qui il roule et pourquoi, sont le sujet de nombreux articles. Des billets plus ou moins fondés, plus ou moins à charge, qui cherchent à comprendre / faire la lumière sur l'interviewer de la chaîne youtube.
---

Depuis quelques mois maintenant, les informations sur qui est Sky de Thinkerview, pour qui il roule et pourquoi, sont le sujet de nombreux articles. Des billets plus ou moins fondés, plus ou moins à charge, qui cherchent à comprendre / faire la lumière sur l'interviewer de la chaîne youtube.

Comme je l'ai dit il y a un moment déjà sur ce blog, Thinkerview est une chaîne qui propose un contenu dont la qualité dépasse grandement la moyenne, et sur un format où les sujets peuvent être réellement abordés. Un projet qui souffre des mêmes problèmes que les autres médias : l'esprit critique de celles et ceux qui sont derrière leurs écrans.

# Sky en personne

Il y a quelques années (avant que Les Parasites rejoignent l'équipe technique de la chaine), j'ai rencontré Sky pour la première fois. Nous nous sommes vus un long moment, dans un café. Il n'avait pas ses lunettes et m'a filé son prénom, tout en conservant tout de même une grande partie de son anonymat (et moi de même). Nous avons discuté de tout et de rien, et particulièrement d'esperit critique sur les sujets épineux qui jalonnent tant la politique extérieure de la France, que les nombreuses réformes en cours à l'époque.

Je me souviens de cette rencontre, où j'avais en face de moi un type qui réflechissait au meilleur moyen de proposer du contenu mettant en lumière des angles experts réels : étaient alors invité-e-s sur la chaine des gens qui faisaient vraiment partie des sujets discutés, et pas juste des gens qui ont un avis sur la question (comme on en voit trop souvent sur les plateaux, Finkielkraut en tête par exemple). J'y voyais une prise de position forte face au ventre mou journalistique qui n'a eu de cesse de se creuser depuis (à quelques exceptions près).

Ensuite, j'ai revu Sky lors de quelques soirées Thinkerview organisées dans des cafés et ouvertes à la communauté. Des soirées conviviales, où les discussions allaient bon train, sans jamais verser dans le complotisme obscur. Des soirées où on questionnait la vie privée et le droit à l'intimité numérique en discutant par exemple du téléphone ou de la ROM/OS la plus à-même de nous défendre sur ces points là. Des repas où on échangeait sur l'attitude de la France à Mayotte par exemple, ou bien de la politique étrangère catastrophique autour de la guerre en Lybie, etc.

Je n'ai jamais eu la sensation d'être au milieu de complotistes. Encore moins d'assister à des congrès d'un Sky gourou. Juste de participer à des rencontres plutôt conviviales de gens qui se posent des questions parce que les instances qui devraient y répondre ne font pas leur taf...

# La notion de complotisme

Et c'est bien ça le truc... Il ya tout de même un sacré monde entre croire à la théorie des reptiliens, et se poser des questions sur la rigueur intellectuelle, opérative, administrative et de corporation, d'une enquête policière, judiciaire ou journalistiques. Que le sujet soit les bavures et les multiples relaxes policères, ou bien le lobbying dans l'agro-alimentaire, la politique étrangère liée au commerce international des armes, ou bien, puisque c'est de ce sujet dont il s'agit pour Sky, le 11 septembre.

Il y a comme ça une guirlande de sujets importants, voire graves, qui sont mal traités par qui de droit. Pourquoi le sont-ils ? Peu importe : ils le sont. Que des gens se posent des questions parce que leur sensibilité sur un sujet ou u autre les y incitent, ca ne tuera pas grand monde... La notion de complotisme est trop rapidement employée, et l'étiquettage est systématique aujourd'hui. C'est dommage, d'abord parce que sémantiquement c'est une idiotie, et ensuite parce que c'est tout sauf constructif.

Plus difficile encore de faire preuve de mesure, lorsque pour une raison ou une autre, on discute / infiltre un milieu hétérogène (où tout le monde ne partage pas forcément le même ensemble pur d'idées) pour en percer la direction et la dynamique. Il faut être sacrément accroché-e pour aller voir des gens avec qui ont ne partage rien, où si peu, mais avec chez qui on souhaite tout de même prendre la température de ce qui se trame, se discute, se concoit. Je l'ai fait avec quelques identitaires blancs supremacistes, avec qui je ne partage rien du tout si ce n'est de poser frontalement la question des rapports de races sociales (et évidemment, nous n'y repondons pas du tout de la même façon). Je l'ai également fait avec des écologistes globalistes, avec qui je partage sans doute l'ambition d'une attention retrouvée pour la nature, mais qui pensent qu'un pouvoir centralisé est la meilleure réponse à tous les enjeux actuels, quand pour ma part je ferai tout pour un monde décentralisé, localiste et interagissant. Je l'ai aussi fait, pour finir, avec des vegans, alors que je suis fromager, parce que nous bataillons chacun avec nos armes pour que les animaux soient mieux traités, quand bien même nous ne soyons pas d'accord sur la relation à la nourriture et à la priorisation des combats...

# Les procès pour esprit critique

Alors on en vient rapidement à cette phase là. Où pour avoir discuté avec quelqu'un-e, partagé une seule idée en s'accordant, quand on s'est déchirés sur 50 autres, on a droit au procès d'intention, ou même de discussion.

Mais pour ma part, j'ai du mal à changer d'idée sur ce sujet : c'est un problème d'esprit critique. Un problème majeur... Parce que si on ne peut pas comprendre que tout ce qui compose une masse étiquettée par un tiers n'est pas necessairement de la même nature, alors on aura beaucoup de mal à faire société, et à produire une politique saine et humaine.

Dernier exemple : [je suis abstentionniste](https://circa1984.net/non-merci-je-vais-mabstenir/). Est-ce que vraiment, on peut considérer que je suis du même abstentionnisme que celui ou celle qui ne vote pas par flemme ? Est-ce que je suis du même absentionnisme que celui ou celle qui a oublié de s'inscrire sur une liste électorale ?

Et en allant plus loin, est-ce que je suis moins politisé et dans la réflexion que quelqu'un-e qui vote aveuglément pour faire barrage ? Est-ce que je me dédouane davantage des problèmes de notre société (racisme, écologie, système capitaliste) qu'un adhérent associatif chez SOS Racisme, EELV ou quelconque syndicat ? Est-ce qu'adhérer à un de ces organismes est le seul moyen de valider un engagement et de le rendre "parfait" et intouchable ?

Si oui, c'est quand même vachement triste. L'indépendance absolue aux étiquettes est une des rares beautés que proposent l'individualisation. Nous somems en effet libres de pouvoir rejoindre d'autres personnes sur des points ou d'autres d'un sujet, et pas du tout sur d'autres. C'est une chance du quant-à-soi, de l'esprit critique individuel... Et il n'y en a vraiment pas des masses, des chances comme ça, dans une société individualiste, alors chérissons celle ci.

Sky me parait être autant complotiste que la Reine d'Angleterre est reptilienne. [Ses récentes réponses pour Libé](https://www.liberation.fr/amphtml/checknews/2019/11/27/le-fondateur-de-la-chaine-thinkerview-etait-il-un-membre-actif-du-forum-reopen911_1764258) me paraissent tout à fait plausibles et j'y reconnais le Sky que j'ai croisé quelque fois. Qu'il ne soit pas satisfait des méthodes d'une enquête, et donc des résultats, ou des justifications proposées par voie officielle, ne me parait pas quelque chose de hautement condamnable. Et je ne saisis même pas pourquoi on pousse autant le tribunal populaire... C'est vraiment qu'on a pas autre chose à faire et à penser... Pourtant, c'est pas sujets qui manquent hein !
