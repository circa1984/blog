---
layout: post
title: Bye Google, part 3
date: '2017-02-27 17:46:00'
desc: Moteur de recherche trouvé, service mail aussi, à l'attaque d'Android ! La découverte des des ROMs sans Google, cette révélation !
---

Après avoir discuté moteur de recherche, service d’email, et autres dans [cet article](http://circa1984.net/bye-google-part-1/) et [celui-ci](http://circa1984.net/bye-google-part-2/), je voulais parler smartphone.

Parce que finalement, Google est un peu le maître du jeu avec Android, et ce sur quasi l’ensemble des marques de smartphones.

Cela dit, et contrairement à ce qu’il propose et offre comme services internet, la donne est un peu différente ici, pour diverses raisons.

* * *

### Android = Linux = Open Source ?

Déjà, il faut noter qu’Android est basé sur Linux. De ce fait, il est open source, donc son code est ouvert… Mais selon votre constructeur ou opérateur, vous pouvez avoir des versions moins ouvertes, spécifiques, qui orientent l’utilisation d’une façon ou d’une autre.

Dans ce cas là, on ne peut pas dire qu’on fait ce qu’on veut de sa vie sur son téléphone, et on dépend d’applications ou d’une ergonomie que l’on a finalement ni souhaitée, ni choisie.

Ce n’est évidemment pas ma façon de fonctionner et j’ai besoin d’outils que je choisis de A à Z. Quitte à geeker des heures dessus, je préfère décider qu’être guidé d’un coté ou l’autre. A ce titre, j’avais opté pour un Oneplus One, un téléphone sorti il y a quelques années et doté à l’époque d’une ROM (c’est à dire une version d’Android) nommée CyanogenMod, qui était développée par une communauté et non par la marque en direct… Une ROM dont la facilité de configuration permettait vraiment d’installer ce qu’on voulait, et aussi, en creusant à peine d’avantage, d’enlever le superflu ou l’inutile.

* * *

### De CyanogenMod à Lineage Os, s’en tenir aux besoins stricts

Une baignade dans l’océan basque plus tard, mon Oneplus n’était plus et le temps de m’en procurer un autre (6 mois passés avec un Logicom… De quoi s’adonner au minimalisme digital sur smartphone…), CyanogenMod avait été arrêtée par ses directeurs…

Une fork (un derivé) avait cependant été lancé, sous le nom de [Lineage OS](http://lineageos.org/). Après installé, je me retrouvais donc sans Gapps (le pack d’applications Google), avec un OS ultra rapide, fluide, lisible, et extrêmement efficace sur la gestion de la batterie.  
Evidemment, Lineage OS est disponible pour tout un tas de smartphones, et pas juste le Oneplus One. Vous trouverez la liste continuellement à jour ici : [Lineage Os Downloads](https://download.lineageos.org/).

Les applications que j’utilise sur smartphone sont les suivantes : WordPress, Ello, Snapchat, Facebook, Twitter, VSCO, Instagram, Maps, de quoi envoyer des SMS (j’utilise Silence), des mails (Protonmail évidemment…), et calculer des itinéraires (dur de trouver un remplaçant à Maps de Google… Quand bien même ce soit dans doute une des apps les plus horribles du point de vue collecte de datas…).

Pour les installer, la plupart requièrent le Google Play Store… Il existe donc une solution pour n’installer que lui, sans avoir à se fader l’ensemble des apps Google… [OpenGapps](http://opengapps.org/), en version pico. (En écrivant, je réalise toute la geekerie qui est en moi…).

Je n’ai donc ni gmail, ni youtube, ni drive, ni tout le reste… Et mon agenda n’est connecté à aucun compte ([grâce cette app](https://play.google.com/store/apps/details?id=org.sufficientlysecure.localcalendar)).

* * *

### Au-revoir, au-revoir président !

Utiliser ce genre d’OS sur ce genre de téléphone me permet de choisir comment JE fonctionne, sans être dépendant ou contraint de suivre un fabricant ou un autre, une marque ou une autre, … La seule dépendance technologique qu’il y a est basé non pas sur une entreprise mais sur une communauté de développeurs (ambiance Linux).

Je suis aussi moins tracé, un petit peu plus invisible, et la visibilité que je cède est choisie. Du moins j’en ai l’impression (coucou les lois françaises…).

Il y a encore un tas de choses que j’aimerais changer : switcher de Google Maps vers quelque chose de plus respectueux de la vie privée (qui n’enregistre mes positions et mes parcours… notamment) est sans doute ce que j’attends le plus. Un autre hébergeur vidéo, qui nous sorte un peu la tête de YouTube (et Dailymotion), …

En attendant, on se débrouille !  

**MAJ :** _depuis, il y a PEERTUBE, un fabuleux endroit initié par Framasoft. Ma chaine est du reste en lien dans la barre de navigation sociale en haut à droite ;) ! Je vous laisse découvrir Peertube par vous même !_
