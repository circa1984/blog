---
layout: post
title: l'écologie est-elle l'unique levier du futur de l'Anthropocène ?
date: '2021-03-01 07:00:00'
desc: Lorqu'on parle d'Anthropocène, on imagine principalement (sinon uniquement) les conséquences de l'activité humaine sur la planète. Ne s'agit-il vraiment que de cela ?
---

La semaine dernière, j'avais le plaisir de donner une journée de cours dans le cadre du MSc Strategy & Design for the Anthropocene, à l'école de design Strate, à Lyon. Durant cette journée, avec les étudiants, nous avons essayé de saisir combien un produit quotidien comme le fromage était une si bonne synthèse de l'évolution de l'Anthropocène.
Inutile pour le bien de ce post de revenir en détail sur le fond de ce cours. En revanche, cette journée a scellé quelque chose qui trotte dans ma tête depuis un moment.

Lorsqu'on parle d'Anthropocène, on imagine principalement (sinon uniquement) les conséquences de l'activité humaine sur la planète, en terme environnemental. Ne s'agit-il vraiment que de cela ? Le futur de l'Anthropocène est-il uniquement lié à notre capacité à comprendre et agir en fonction des situations environnementales et des considérations écologiques ?

Je crois fermement que non. Et voici pourquoi.

#### Préambule : une définition pratique de l'Anthropocène
Commencons par définir avec un angle pratique ce qu'est l'Anthropocène. J'aime bien le définir comme l'ère de l'influence de l'humain sur l'habitabilité de la planète pour l'ensemble des espèces.
Pourquoi une définition de ce type :
1/ *l'ère* : parce qu'il s'agit d'une période, d'une paranthèse temporelle et temporaire.
2/ *l'influence de l'humain* : parce que notre activité en tant qu'espèce a des conséquences qui dominent l'activité des autres espèces.
3/ *l'habitabilité de la planète* : la capacité à vivre sur cette planète dans des conditions qui ne relèvent pas de la survie en milieu hostile.
4/ *l'ensemble des espèces* : nous humains, et tout le reste de la biodiversité, de la plus petite échelle à la plus grande.

### LA QUESTION ENVIRONNEMENTALE
Lorsqu'on parle d'Anthropocène, la discussion s'inscrit dans un cliché collapsologique determiné aujourd'hui par l'urgence climatique et écologique. C'est du reste la collapsologie qui parvient à mettre, à de trop rares occasions, la notion d'Anthropocène sur le tapis, elle qui est aux abonnés absent du débat public.

Cette dimension écologique et environnementale de l'Anthropocène est indéniable, et l'objectif de cet article est loin de contredire cette réalité. Le climat évolue, se transforme, change, parce que l'activité humaine, industrielle et post-industrielle, profondément capitaliste et globalisée, accélère, active, provoque un certain nombre de bouleversements à très grande échelle. On peut aisément parler du Capitalocène (sous-paranthèse qu'est l'ère capitaliste à l'intérieur de l'Anthropocène) comme un levier de désastres écologiques.

Au regard de la définition que j'ai donné plus haut de l'Anthropocène, l'activité humaine aujourd'hui ne permet pas du point de vue environnemental l'habitabilité de la planète pour toutes les espèces, bien au contraire. C'est sur ce constat terrible que repose aujourd'hui la majorité des débats sur l'Anthropocène. Or, n'y a t'il pas d'autres questions qui devraient avoir une place naturelle dans ces débats ? Des questions dont la résolution des problématiques soulevées devraient avoir toute leur place dans le design du futur de l'Anthropocène ?

### LES QUESTIONS OUBLIEES
On peut distinguer selon moi deux autres grandes familles de problématiques contemporaines qui sont bien trop oubliées. D'une part les questions sociales, et de l'autre les questions de gouvernance.

#### LES QUESTIONS SOCIALES
Revenons sur une définition, celle de l'habitabilité. Parallèlement au changement climatique qui met en péril la capacité des humains à rester sur les territoires qu'ils occupent aujourd'hui, la simple notion d'habitabilité dépasse la montée ou la descente du mercure dans un thermomètre. Est habitable un endroit qui permet à un groupe ou à une personne de vivre avec le minimum de confort.
Pour ce qu'elle vaut, la pyramide de Maslow nous apprend que les besoins premiers d'une personne sont les sentiments d'utilité, d'estime, d'appartenance de sécurité et évidemment les besoins physiologiques.
J'en viens à mon point : notre société présente-t'elle des conditions d'habitabilité *normales* pour tous les groupes et toutes les personnes qui la composent ?
Posons les questions plus directement :
- le racisme systémique permet-il l'habitabilité du territoire sur lequel la société qui le produit abrite des personnes racisées ?
- l'homophobie et la transphobie structurelles permettent-elles l'habitabilité du territoire sur lequel la société qui les produit abrite des membres de ces groupes ?

Les mêmes questions se posent avec la culture du viol et le sexisme, la grossophobie, les rejets structurels vécus par les personnes en situation de handicap, la fracture rurale, les classes sociales...

Ces mécaniques fabriquées, structurelles, mettent les groupes qu'elles touchent dans des zones de liminarité, quelque part entre la violence de l'exclusion et le mensonge de leur appartenance. il faut aussi noter que cette liminarité n'étant pas issue d'un rituel dont l'aboutissement est l'inclusion totale et non-questionnable, elle est en fait aussi infinie que non-admise. Et lorsque ces groupes décident de la mettre en lumière, la lenteur de la réalisation collective ressemble à un déni confortable pour les uns, insoutenable pour les autres.

Je ne peux donc pas m'empêcher de penser aujourd'hui que dans ces conditions où les sentiments d'appartenance, d'estime, d'utilité et de sécurité ne sont pas garantis pour les groupes cités ci-dessus, alors l'activité structurelle et politique de notre société remet grandement en question l'habitabilité de la planète pour ces personnes.
Il ne me parait donc pas si curieux de voir que si le *collapse* du climat pousse certains vers la perspective survivaliste, les oppressions systémiques poussent quant à elles leurs cibles à créer des *safe-places* communautaires. Après tout, ne s'agit-il pas du même reflex de survie ?

Réfléchir à l'Anthropocène de demain, que celui-ci soit designé autour de cette habitabilité totale, ne peut donc passer que par l'inclusion des problématiques sociales sur le même plan que les problématiques écologiques (et à ce titre, la notion d'écologie décoloniale me parait être un très bon exemple).

#### LES QUESTIONS DE GOUVERNANCE
L'accéleration phénoménale de l'Anthropocène a t'elle un lien avec l'évolution des gouvernances ? Ca je sais pas. Ce que je me dis, c'est qu'on pourra difficilement entrevoir l'Anthropocène harmonieusement si nous ne repensons pas les principes de gouvernance autrement. Et "autrement" ca commence selon moi par sortir de la verticalité.
De la royauté à la démocratie éléctoraliste, la verticalité est omniprésente dans l'histoire de la gouvernance de notre société. Or, et aujourd'hui plus que jamais, les problèmes de démocratie sont eux aussi partout. Sans faire de sophisme, j'y vois une relation de cause à effet évidente. Il est donc grand temps de repositionner la gouvernance, et de voir quelles solutions pratiques l'horizontalité a le mérite d'offrir.

Clairement je parle ici de société des communs, d'anarchie, et de construction libertaire du vivre ensemble. En clair, une rupture (révolutionnaire) très profonde, donc radicale, avec le paradigme actuel. Une nouvelle donne dans laquelle la démocratie est effective, c'est à dire où chacun et chacune possède sa capacité d'expression et de participation à l'exercice social et politique au quotidien.

En terme pratique, la délégation que nous connaissons aujourd'hui sous la forme électorale disparaitrait. Il ne s'agirait donc plus de voter pour que quelqu'un incarne un mandat d'autorité individuel, mais de voter collectivement pour chaque décision normalement prise par cette personne élue.
La délégation de nos voix devra exister. L'exemple de la démocratie liquide est une piste, dans la mesure où celle-ci comporterait une modération mécanique de l'accumulation de voix.

Avec pour objectif de dissoudre les rapports de domination induit par la verticalité, l'horizontalité permet d'effacer une partie exécutive et pratique des rapports privilèges/préjudices en limitant leur représentation.

Mais comment arrive-t'on à cela dans les faits ? C'est vrai que les quelques paragraphes si dessus vont aussi vite qu'une recette de cuisine au micro-onde. C'est sans doute irréalisable en un temps record, particulièrement dans une dynamique électorale. Aucun parti politique ne saurait à mon avis porter un tel projet, ce serait presque de l'ordre de l'oxymore.

Cet avenir me parait ne se dessiner que dans les initiatives qui ne visent pas la prise de pouvoir (ni révolutionnaires au sens habituel du terme, ni électoralistes donc). Des initiatives qui visent davantage l'autonomie que le renversement.
Quel meilleur moyen pour limiter le poids structurel d'un État et d'une société sur soi et son groupe que de devenir si autonome qu'on s'en extrait au maximum ?

Ce sera sans doute l'objet d'un prochain billet, qui devra creuser davantage l'idée.
