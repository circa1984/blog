---
layout: post
title: La société de la contribution
date: '2017-09-04 10:49:00'
desc: La révolution telle que vue parfois dans les (anciens) fantasmes anarchistes, je n’y crois plus. (Quoique...)
---

La révolution telle que vue parfois dans les (anciens) fantasmes anarchistes, je n’y crois plus.

Ce renversement du pouvoir par la rue n’a selon moi aucune chance de se réaliser. Encore moins sans violence. Encore (encore) moins par les urnes. Encore (encore) encore moins au sein de l’UE.

Il ne reste donc qu’une seule piste. La contribution locale à des actions d’une radicalité parfois insoupçonnée mais très concrète. Et avec cela, le retour à la souveraineté de chacun et de tous sur l’espace et le temps investi.

* * *

### Nous ne sommes plus les artisans de notre propre société

Parce que celle-ci est aujourd’hui conçue et régie par des principes proches du taylorisme. Parce que la division parcellaire de ses éléments de composition nous éloigne tous et chacun dans notre coin, loin de la réalité globale, du pouvoir de conception et du pouvoir de décision.

En effet, la situation est telle que nous ne savons même plus comment fonctionne notre société : ses organes, institutions, mécanismes. Tout cela nous est très abstrait. Aussi abstrait du reste que la réalité concrète ne l’est chez ceux qui les conçoivent et conduisent : nos politiques. Comme dans une entreprise où la division des tâches et des attributions séparent et aliènent les ouvriers de la chaîne loin de la conception du produit, et éloignent les concepteurs du concret et de la réalisation.

(Petite parenthèse : rien de surprenant ou de choquant donc, de voir une génération entière de jeunes travailles / cadres dynamiques finir en bore-out / burn out à cause de métiers qui n’ont « aucun sens ». Chers camarades, vos métiers ont un sens – souvent peu ragoutant en réalité, mais la lecture est troublée, rien de plus. Le concret des conséquences de votre utilité ne vous ait pas préhensible. Et accepter de comprendre et « dé-flouter » la situation ne sera pas sans douleur…)

L’artisanat de notre société, c’est à dire la faculté pour le groupe qui la compose de concevoir ET réaliser ses principes et ses mécanismes, n’existe pas ou plus. Évanouie malheureusement, avec pour seul placebo de fortune un droit de vote inutile et asservissant.

* * *

### Contribuer à l’échelle locale pour apprendre et transmettre

Si chacun d’entre nous, ou quelques uns par groupes, faisons le choix d’une contribution à échelle locale, je pense que nous pourrions observer une série d’impacts directs puis indirects, globalement positifs pour l’intérêt général.

D’abord cela permettrait aux concernés de se réarmer pour l’action politique. Et par politique, il faut entendre ici : chose publique. Retrouver des méthodes et des outils de conceptions et d’actions coordonnées, convergentes ou intersectionnelles. Comme par exemple la constitution. Savoir à nouveau concevoir, aplanir, rédiger les constitutions des institutions de l’échelle concernée : par des assemblées, des think tanks, … Il n’y a aucun mot complexe ici. Il ne s’agit finalement que de rédiger le mode d’emploi du vivre ensemble tel que nous voulons le transmettre aux générations d’après.

Si constituer est évidemment le premier exemple qui vient en tête, d’autres suivent de très près : la conception et création de monnaie d’abord. Et cela fait [suite directe à cet article](https://circa1984.net/echelle-locale-et-monnaie-locale/). Parce que c’est là le pivot-garant des rapports économiques intra-local et pan-local. Que nous puissions nous munir à nouveau d’un outil transactionnel fluide et parfaitement adapté et adaptable à nos besoins. Le reste des outils découlent de ces deux axes majeurs. Et ainsi arrivent donc très vite la couverture sociale et la santé, l’éducation évidemment, le logement, la justice, la culture, la gestion de l’agriculture, l’harmonie de l’écosystème et l’écologie, etc.

Au delà de la reprise en main de ces outils, d’autres impacts suivent. Réapprendre à cultiver par exemple, une réflexion, un savoir-faire, des graines, peu importe. Savoir partir de rien et faire pousser des choses, les amener à grandir et transmettre le fruit de ce travail, la méthode et les astuces, à celles et ceux qui nous entourent et aux suivants. C’est en quelque sorte quelque chose qui se rapproche mon sens de l’étude des _communs_, et de tout ce qui en découle comme pensée sociale.

Sans oublier bien sûr de réapprendre à créer organiquement un désir collectif et inclusif de société, avec notamment des prises de décisions, après concertation et discussion, sur des sujets du quotidien. Ce qui reviens finalement à « légiférer » par la contribution et l’inclusif.

### Retrouver l’état normal d'harmonie

L’état normal de la société comme des choses et des gens. J’imagine la contribution comme une révolution nette faite de résilience. Qui s’affranchit de la violence sans oublier d’être radicale. Elle créé le changement par l’action, la création de ce qui va supplanter l’actuel. De fait, elle nourrit les élans communs ou intersectionnels de tous ceux qui ont à redire sur l’état actuel des choses, en leur donnant les éléments et les outils pour mettre en place ce qui leur semble être l’état normal.

La contribution se déclenche selon moi dès l’échelle humaine/locale mais obligatoirement dans une sphère collective et inclusive. Et faire des choix de vie individuels n’est qu’indirect. Je crois qu’à l’heure actuelle (je parle souvent des urgences que nous avons devant nous…), nous ne pouvons et devons pas nous arrêter là. Et si nous voulons retrouver l’état normal, alors nous devons y aller plus vigoureusement.

Pour ma part, j’ai quelques pistes, liées complètement ou partiellement à ce blog. Je pense que parler de ses engagements publiquement et en donner les tenants et aboutissants est déjà un engagement collectif. Témoigner et encrer ce témoignage quelque part à la vue de tous, c’est déjà participer et contribuer activement. Photographier, filmer, designer et dessiner, écrire… Tant que l’on rapporte et qu’on conçoit, en plaçant le fruit du travail accomplit à la vu de toutes et tous, alors nous commençons déjà contribuer.

Mais au delà de cela, je veux absolument pouvoir transmettre d’avantage le peu que je sais, et rejoindre des réseaux plus actifs : assemblées constituantes, ateliers, think tanks… Nous verrons bien, j’en parlerai ici bien évidemment !
