---
layout: post
title: Mais… pourquoi locavore alors ?
date: '2017-07-15 20:24:00'
desc: Pour aller plus loin, il faut que je passe l'étape qui consiste à organiser ma pensée disons « philosophique » derrière ce choix. Essayer de décortiquer pour expliquer cette démarche simplement.
---

Pour aller plus loin, il faut que je passe l'étape qui consiste à organiser ma pensée disons « philosophique » derrière ce choix. Essayer de décortiquer pour expliquer cette démarche simplement.

Avec aussi pour but de nuancer proprement et sainement les différences qui me font aujourd’hui privilégier le locavorisme et critiquer (avec beaucoup de bienveillance, soyez en assurés) le véganisme.

* * *

### Consommation dégénérée, espèce humaine, futur

En réalité, ma démarche émane d’une analyse croisée des champs socio-anthropologique et philosophique. De nombreux constats se sont percutés les uns aux autres. Provoquant des émotions, des réflexions, et des conclusions qui pointaient toutes dans la même direction.

Évidemment il s’agissait de critiquer, plutôt violemment, la société de consommation telle que l’après WW2 l’a vue naître et telle que nous la connaissons aujourd’hui. Une société violente, grave, sale. Des animaux torturés, des masses de gens gavés, des santés suicidées, des psychologies dégradées. Un rapport à l’assiette, aux produits et aux circuits complètement dégénéré. Ras-le-bol.

Il s’agissait aussi d’essayer de comprendre notre espèce : très adaptable, capable de vivre sous toutes les latitudes ou presque, de s’accommoder de tous les écosystèmes ou presque, par son ingéniosité, et sa capacité d’innovation et de complétion. Créant ainsi une diversité de peuples, de cultures, y compris alimentaires, aussi fabuleuse que nécessaire. Et dont la contemplation a chez moi des conséquences spirituelles assez profondes…

Et avec cela, il s’agissait enfin de réfléchir au futur. Parce que je conçois de moins en moins que cette planète ait un quelconque propriétaire. Et que, peut-être avec l’âge de la paternité arrivant, naît en moi le désir / devoir absolu de laisser aux suivants une planète en meilleur état qu’aujourd’hui. Clairement pas un chantier facile…

* * *

### Analyse politique, changement de paradigme

Alors, j’ai essayé de regarder ce qui nous avait mené au point où nous en sommes. Les décisions majeures, les orientations principales qui aujourd’hui nous mettent face à des urgences et des catastrophes terribles. J’ai trouvé plusieurs réponses, en remontant le fil. Et curieusement, c’est pendant ma formation de fromager, et des connaissances acquises sur cette filière, son patrimoine et son histoire, que tout s’est relié avec cohérence.

(Je ne compte pas la révolution industrielle menant à l’urbanisation exponentielle donc…) Il y a d’abord cette Seconde Guerre Mondiale, qui fout l’Europe en vrac. Y compris son agriculture d’ailleurs. La France à genou, plan Marshall, politique d’immigration et de grand élan économique pour reconstruire un pays en ruines… On produit à gogo, de tout, à fond. Les révolutions technologiques y aident bien. L’électronique, l’ordinateur, tout accompagne la grande industrialisation de l’alimentaire (entrer autres – cf les films de Jacques Tati). Et l’urbanisation des modes de consommation. Sans oublier l’américanisme glouton (cf [les ouvrages de Clouscard](https://www.scribd.com/doc/302011258/Clouscard-Michel-Le-Capitalisme-de-La-Seduction)).

<figure class="kg-card kg-embed-card"><iframe width="459" height="344" src="https://www.youtube.com/embed/LE9t98Gox60?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

Il y a ensuite cette Europe administrative et financière. Construite à la hâte, pour éviter de nouveaux conflits (?), pour partager les miettes entre l’URSS et les USA. Une échelle globale qui s’installe partout et qui devient la nouvelle nomenclature de pensée et d’organisation : on internationalise, on globalise, on mondialise. On connecte des masses, des produits et des marchés, à d’autres, qui n’ont rien à voir, qui n’ont parfois / souvent rien demandé. Vite, fort, fermement. C’est violent mais la technologie, les médias et l’effervescence produisant tellement d’adrénaline collective et de fantasmes qu’on oublie. On oublie les conséquences possibles, ou on les calcule. Tout dépend sa position sur l’échelle de classe. Des conséquences directes et indirectes : une déconnexion du réel, du pratique, de l’échelle humaine, un vide de plus en plus sidéral du sens de nos emplois, de nos usages, de nos pratiques…

Pendant 60 à 70 ans nous vivons à cette vitesse prodigieuse, gloutonne et vorace. Grandissante, envahissante. Globalisé, le monde est plus que jamais uniforme, déséquilibré, colonisé aussi. L’expansion de Mc Donald’s est le symbole de l’américanisation des modes de consommation. Plus généralement, une uniformisation de la nutrition, et une industrialisation prédatrice de la production et de la distribution agro-alimentaire (coucou Lactalis).

Il y avait peut être du bon à cela. Quand effectivement les pays avaient besoin de produire beaucoup, nourrir beaucoup, à bas prix. Est-ce toujours le cas aujourd’hui ? Je n’y crois plus une seconde. Les besoins ne sont plus ceux de pays en état de guerre ou en reconstruction. Les besoins changent avec le paradigme. Ma réflexion essaie de suivre ce mouvement.

* * *

### Locavore parce que « localiste »

Si le global a pu accompagner des besoins pré-2000 (je laisse le bénéfice du doute…), je suis aujourd’hui convaincu que cette échelle n’est plus adaptée au monde moderne… Je le répète à longueur de posts ici… Il faut savoir vivre son échec, en tant que société. Tirer les leçons aussi. Mais ces deux choses là nous sont volés : ceux qui gouvernent et ceux qui décident sont ceux qui jouissent du système actuel : comment pourraient-ils lâcher prise et nous permettre de changer de modèle pour nous adapter à NOS besoins ?

Changer de modèle pour moi, c’est comprendre les besoins actuels, inhérents aux urgences actuelles. Et à ce propos, la diversité est au centre de tout. Tant dans le champ alimentaire et biologique, que dans le champ humain et anthropologique. Sans oublier bien sûr l’aspect culturel et sociologique.

Ces besoins me paraissent (pourtant) simples :

- se réadapter aux écosystèmes où nous vivons : réappropriation des territoires et de notre adaptabilité à eux
- reconstruire les piliers sociétaux de façon ouverte et libre : science open-source, laïcité réelle, identité libre, économie vertueuse, instruction « désidéologisée », démocratie participative effective
- modéliser un avenir « sustainable » / durable pour les générations futures de toutes les espèces

Et avec mon cursus d’anthropologue, je ne pouvais pas passer à coté de cette conclusion : le local est l’échelle humaine naturelle, garant de la préservation et de la dynamique de toutes les diversités. Je suis donc naturellement devenu « localiste ».

Cela signifie que, des années après avoir eu l’instinct de consommer local en m’engageant en AMAP, et sans doute avec le souvenir du jardin de mon grand-père nourrissant 10 personnes toute l’année, j’entamais la mise en logique de cette réflexion. Et depuis, je déconstruis et reconstruis ma consommation au même rythme que ma vision d’ensemble.

* * *

### Locavore, parce qu’il n’y a en fait qu’un seul front

La cause animale résume toute l’aberration de notre système de consommation. Elle synthétise tout ce qu’il y a à appréhender au rayon des horreurs et de la violence d’un système. Les écosystèmes sont dans une telle souffrance, que si on sort du déni à ce sujet, on ne peut qu’être sensible au végétarisme (_a minima_) et tenté par le véganisme (qui ne se contente pas de l’alimentaire). Ce sont de justes et nobles réponses à l’émotion, et qui peuvent être adéquates en fonction de leur application (je développerai plus bas). Je remets le lien du documentaire _Racing Extinction_… déjà présent dans un de mes articles précédents…

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/jgjQJdsySrM?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

Il n’y a pas que la cause animale ou écologique. Il y a aussi un ensemble de problématiques humaines, plus ou moins sournoisement cachées. Des nœuds à démêler et qui concernent en autres le droit du travail, les délocalisations, l’urbanisation et les déserts ruraux, la gentrification, la taxation et la fiscalité, la qualité de vie des producteurs, la prédation économique. Tout cela est à l’œuvre, dans une horlogerie qui demande du sang froid quand on veut en comprendre la routine…

Tout cela se rejoint aussi. Dans l’oppression subie par les animaux, les écosystèmes, les populations. Et aujourd’hui en 2017, le niveau de violence est à mon avis cathartique, tout au long de la chaîne…

* * *

### Végan, la réponse la plus radicale

De ce que j’en ai compris, et j’invite tous les végans qui liront ces lignes à me corriger ou me reprendre au besoin, le véganisme est donc un mode de consommation général basé sur des produits où l’animal n’entre jamais dans quelconque phase du processus de fabrication et de production, et où la production doit se faire en respectant tous les acteurs de la chaîne.

C’est évidemment un reflex de consommation sain et logique, comme je le montrais plus haut. En revanche, c’est une radicalité qui ne souffre aucun répit. Et j’imagine donc très mal le militantisme végan autrement qu’en communauté autonome rurale. Comment pourrais-t’on être végan en continuant de consommer sur le même modèle globalisé qui dérègle et dégénère tout ?

Et je ne l’imagine pas non plus pouvoir être transposable à une échelle globale, sans faire subir aux hommes une violence sociale et culturelle terrible (et donc antinomique du véganisme). C’est vrai : comment un eskimo, par exemple, pourrait-il « véganiser » son mode de vie sans s’imposer la violence inouïe d’un exode ?

Je trouve cette radicalité d’engagement sublime. Mais j’ai encore du mal à saisir qu’elle puisse être graduelle. Qu’il puisse y avoir un véganisme urbain, ou encore un mondial… Ma critique de ce mouvement est principalement liée donc à ses velléités d’universalisation.

* * *

### Locavore, moins radical, mais à mon sens plus révolutionnaire

Le locavorisme est clairement moins radical. Parce qu’il n’est pas dans le combat moral, il est uniquement un engagement de société. Il n’incite qu’à comprendre que le circuit court est la réponse qui tord le plus fort le cou à la société de consommation que nous connaissons. En détruisant aisément et en douceur, l’ensemble des rouages complexes de l’horlogerie exposée précédemment ici.

Il est aussi naturellement protecteur des identités culturelles locales, et incite celles ci à reconstruire l’harmonie qui lie l’humain à son territoire, son écosystème et sa préservation. Il engage celles et ceux qui le veulent à reprendre en main l’ensemble de leur modèle de consommation, et à déserter le dégénéré actuel. Quelque soit le milieu social, territorial, culturel, religieux. Sans contre-indication.

Il n’a pour seule conséquence universelle que la préservation des diversités, et peut même, à l’heure actuelle, être un vecteur de leur recensement. Parce qu’il est un outil de définition culturelle et sociale par les hommes vis à vis du territoire qu’ils habitent, de construction communautaire saine et inclusive.

Il est en fait l’inverse absolu du système dans lequel nous vivons aujourd’hui. Pour autant, il n’est ni un _downgrade_, ni une perte de confort, ni une soumission, et encore moins une compromission. Il est au contraire l’endroit où tout est à la portée de tous et de chacun.

* * *

### Locavore et végan ?

Voilà pourquoi j’apprends plus à être locavore que végan. Deux choix qui ne sont du reste, sous certaines latitudes, pas forcément inconciliables. Cela signifierait alors que nous serions capables de produire sur place l’ensemble des aliments ou sources nutritionnelles répondant à nos besoins, sans ressource animale.

Je prends l’exemple de la Vitamine B12, complément alimentaire essentiel à la santé des végans. En étant locavore et végan, il faudrait pouvoir la synthétiser sur place. Pas évident ? Eh bien c’est peut-être la gambiologie et le biohacking (je parle plus haut de science open source aussi…) qui sauraient/sauront mettre cela à la disposition des communautés le désirant. Un espoir pour celles et ceux, végans convaincus, qui ne veulent plus voir aucune source animale dans leur assiette.

Et même si je respecte hautement ce choix là, je ne le fais pas moi même, et ne le ferai sans doute jamais. Ma lecture de l’Histoire de l’Homme étant celle d’une cohabitation gagnant-gagnant avec les espèces nous entourant, où nous offrons la protection et le confort à des espèces qui nous le rendent en produits alimentaires (coucou l’histoire des troupeaux, puis des fromages). Et même si la linéarité et l’intégrité de cette Histoire est catastrophique, particulièrement aujourd’hui, le monde rural, l’anthropologie et quelques initiatives ici ou là dans le monde, m’aident aujourd’hui à plancher et agir pour cette cause là.

Advienne que pourra !
