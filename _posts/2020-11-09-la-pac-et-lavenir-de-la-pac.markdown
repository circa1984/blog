---
layout: post
title: La PAC et l'avenir de la PAC
date: '2020-11-09 14:30:59'
desc: Petit point route du fonctionnement de la PAC suivi de grandes idées pour sa prochaine réforme, voilà ce dont va discuter cet article.
---

Je me suis penché sur la PAC ces derniers temps.  
C’est un très gros sujet qui implique beaucoup de choses. Un gros sujet parce que la Politique Agricole Commune est le plus gros outil de l'Union Européenne. C'est ce qui mobilise le plus de ressources, humaines et financières, et c'est aussi ce qui touche à des choses très concrètes et particulièrement en lumière dans l'actualité des dernières années, à savoir l'environnement et l'écologie, l'alimentation, l'agriculture et les agriculteurs et agricultrices.

J'ai commencé ce travail pour un groupe, mais il a sa place sur ce blog. Alors petit point route du fonctionnement de la PAC suivi de grandes idées pour sa prochaine réforme, voilà ce dont va discuter cet article.

### SYNTHÈSE RAPIDE DU FONCTIONNEMENT DE LA PAC

Les premières choses que j'ai comprises sont les suivantes :

- la PAC est en cours de réforme
- la PAC actuelle souffre de nombreux défauts d’orientations et doit rectifier le tir
- la PAC doit s’engager sur un plan de 10ans, jusqu’à 2030

La PAC est en place depuis 1962. Son objectif à l’époque était de développer la production agricole afin de nourrir les européens (sortie de la WWII, etc). Elle est à ce jour le projet le plus abouti de l’UE en terme de fonctionnement, et de “résultat” : c’est en tout cas celle qui est le plus décidée de manière commune par les membres de l’UE.  
Elle représente d’ailleurs 40% du budget total de l’UE, ce qui est clairement colossal.  
Depuis sa création, elle a évidemment subit de très nombreuses réformes, notamment pour des mises en conformité avec les règles de l’Organisation Mondiale du Commerce. La prochaine réforme doit entrer en vigueur courant 2021.

### FONCTIONNEMENT :

La PAC repose sur 3 piliers fonctionnels :

**LES AIDES DIRECTES :** elles concentrent 70% du budget total de la PAC et sont destinées aux agriculteurs et agricultrices, comme une sorte de revenu de base sans lien avec ce qui est produit, mais proportionnel à la surface exploitée et au potentiel de productivité.  
**LE DÉVELOPPEMENT RURAL :** c’est un cofinancement entre l’UE et des instances nationales / locales, pour apporter un complément aux agriculteurs et agricultrices qui sont dans des zones géographiques sources de désavantage compétitif, ou bien qui seraient en démarrage d’activité. On a aussi une partie de ce budget alloué à l’accompagnement des fermes vers plus de compétitivité et de prise en compte de l’environnement.  
**L’ORGANISATION COMMUNE DES MARCHES :** en cas de crise, c’est un organe qui permet à l’UE d’intervenir sur les marchés de produits agricoles. C’est également un organe qui accompagne la structuration des filières, et qui régule l’import/export. Aucune aide touchée par les agriculteurs et agricultrices en provenance de ce troisième et dernier pilier.

En France, le relai de la PAC est bien entendu ministériel, bien qu’une partie des décisions puissent également émaner directement des régions (sur le second pilier, le Dev Rural).  
Nous sommes l’état membre de l’UE qui reçoit le plus d’aides, avec 9 Milliards d’€ /an. Et il convient de clarifier que pour le coup, ce n’est pas l’Etat qui reçoit les aides, mais bien les agriculteurs et agricultrices (et quelques autres organismes, mais dans une bien moindre mesure).

### UNE VISION DE LA PAC :

Rentrons donc dans le vif du sujet. Il y a un grand besoin collectif de discuter de la PAC de demain.  
Si on revient sur ses objectifs initiaux, la Politique Agricole Commune c'est avant tout la mise en commun par les états membres de l’Union Européenne de leurs savoir-faire et de le force de production agricoles avec pour objectif premiers de nourrir les citoyens européens. A ce titre, elle se veut être un accord commun, évolutif et sensible à son époque et aux autres éléments de société, orienté naturellement vers la souveraineté alimentaire.  
Il y a donc pour moi du Commun, de l’universalité, du droit de l’humain, de la coopération, du partage et de la résilience dans l’ADN de la PAC, et c'est précisemment cela qui doit continuer de définir sa direction.

Mais la PAC a aussi quelques défauts. Ces défauts, majeurs si on en croit les acteurs des filières concernées, semblent être son orientation industrielle actuelle, qui nuit aux agriculteurs et agricultrices, aux territoires et à l’environnement, ainsi qu’aux produits : accords de libre-échange en tout genre, indemnités versées à la surface plutôt qu’à l’actif, on est sur un marché extrêmement violent, avec énormément de prédation.

Or, l’agriculture, c’est le début de l’alimentation, et que se nourrir, c’est comme se soigner, on ne devrait pas voir un marché violent s’installer dans ces endroits là de la société.

Et si nous devions penser une réforme de la PAC, alors pourquoi ne pas la réorienter vers ses objectifs initiaux, en insufflant à nouveau un peu de commun dans sa structuration ?

### LES ENJEUX DE LA PAC DE DEMAIN

Parce que je ne l'ai jamais assez fait sur ce blog, et qu'il n'est jamais trop tard pour bien faire, voici quelques propositions politiques, dans les grandes lignes, pour une réforme de la PAC.

Ces propositions suivent trois axes que je trouve, après avoir mené ma petite enquête sur ce sujet, fondamentaux : la démocratie, la souveraineté alimentaire mondiale, et évidemment, l'environnement.

#### LA PAC ET LA DÉMOCRATIE

La PAC porte bien son nom : c’est avant tout une Politique. De fait, elle ne devrait pas déroger à la règle de se vouloir le plus démocratique possible, en étant l’outil de celles et ceux qui produisent et qui consomment.  
A ce titre, nous pourrions imaginer des réformes pour :

- structurer sa gouvernance autour des citoyens, ceux qui produisent et ceux qui consomment pour créer une démocratie alimentaire à horizon 2030 (en s’appuyant sur les Régions par exemple)
- rendre la transparence totale sur les décisions prises à propos de la définition ou de la mise en œuvre de la PAC
- orienter les aides directes non pas à la surface mais à la force de travail active (avec un maximum de salariés par chef d’exploitation, pour encourager à la fois l’emploi et les petites exploitations)
- n’octroyer les aides directes qu’aux travailleurs agricoles (limitant la spéculation des grands propriétaires terriens et des sociétés d’investissements)
- fondre la PAC dans le reste des décisions de société (en la liant plus profondément à la santé et l’éducation par exemple, pour rendre son statut d’intérêt général plus préhensible)
- créer un Fond de Mutualisation Européen Agricole, géré par l’UE et cofinancé par tous les acteurs des filières agricoles, favorisant l’autonomie de l’Agriculture Européenne en terme d’assurance

#### LA PAC ET LA SOUVERAINETÉ ALIMENTAIRE MONDIALE

Recentrer la PAC sur son objectif initial est important pour deux raisons. D’abord, protéger les productions locales et les producteurs européens, tout en garantissant des aliments de bonne qualité aux consommateurs européens. Ensuite, limiter la prédation de l’UE sur d’autres continents, par exemple en limitant les aides à la production de produits exportés qui vont aller se mettre en concurrence déloyale partout dans le monde, avec des effets très négatifs sur les paysanneries des pays du Sud.

Nous pourrions donc réfléchir sur les points suivants :

- dans les aides directes, orientation d’un budget incitatif vers la production des denrées saines aujourd’hui déficitaires dans l'UE (par exemple : légumineuses, fruits frais, légumes frais)
- création de barême(s) d’accès aux aides avec des objectifs intégrant l’équilibre nutritionnel et le goût des aliments produits (cahiers des charges)
- éligibilité des PAT (Projets Alimentaires Territoriaux, qui s’appuient sur un diagnostic territorial des besoins alimentaires) aux aides de la PAC, pour accélérer leur développement et répondre aux besoins locaux avec fluidité et efficacité
- structuration des filières territorialisées, avec des aides à l’autonomie alimentaire des collectivités (par exemple sur la restauration collective municipale)
- renoncement à la ratification de tout accord de libre-échange
- remboursement des aides de la PAC percues dans l’UE pour des matières agricoles exportées hors UE (lorsqu’on produit pour exporter hors UE, on rembourse les aides touchées)

#### LA PAC ET L’ENVIRONNEMENT :

Pour fermement inscrire la PAC dans l’idée d’une société qui comprends pourquoi et comment elle produit les choses, il est nécessaire de la rendre cohérente vis à vis des enjeux environnementaux nationaux, européens et mondiaux. Les aides versées par la PAC doivent donc avoir en elles des objectifs équilibrés autour de l’environnement, de l’humain, et des animaux agricoles. Un vrai triangle vertueux à assainir, dynamiser et pérenniser, avec quelques idées :

- valoriser les mesures des agriculteurs et agricultrices à effets positifs sur la biodiversité, les sols, l’eau, l’air (par exemple : des primes pour “services environnementaux” à définir)
- nouvelle conditionnalité des primes, comprenant le bien-être animal, la cohérence agro-environnementale des fermes ainsi que les conditions de travail des salariés agricoles.
- inciter à la diversification et l’autonomie (notamment par l’échange / coopération p2p de semences, savoir-faire, employés) des exploitations agricoles pour augmenter leur capacité de résilience
- inciter à la gestion en amont des risques sanitaires

Voilà dans les grandes lignes ce vers quoi la PAC pourrait tendre si elle voulait épouser les urgences que nous avons devant nous, et participer au rééquilibrage sociétal en matière d'alimentation et d'agriculture.

Autrement dit : la route est très longue !

* * *

_sources :_  
_[Guide de la Pac - pouruneautrepac.eu](https://pouruneautrepac.eu/wp-content/uploads/2019/06/Guide-de-la-PAC.pdf)_  
_[La PAAC post 2020 - pouruneautrepac.eu](https://pouruneautrepac.eu/notre-vision/notre-paac-post-2020/)_  
_[Vers la PAC de l’après 2020 - europarl.europa.eu](https://www.europarl.europa.eu/factsheets/fr/sheet/113/vers-la-politique-agricole-commune-de-l-apres-2020)_  
_[Pour une PAC post-2020 - confédération paysanne](http://confederationpaysanne.fr/sites/1/mots_cles/documents/PAAC_post_2020.pdf)_  
_[Propositions et Réactions pour la future PAC - supagro.fr](https://www.supagro.fr/capeye/2018/06/11/propositions-et-reactions-pour-la-future-pac/)_  
_[Pour une réforme en profondeur… - agriculture-strategies.eu](https://www.agriculture-strategies.eu/2018/05/pour-une-reforme-en-profondeur-de-la-pac-dans-un-cadre-multilateral-a-renouveler/)_  
_[PAC 2020, une réponse au défi européen - agridees.com](https://www.agridees.com/download/PAC2020-une-reponse-au-defi-europeen1.pdf)_  
_et le site [imPACtons - debatpublic.fr](https://impactons.debatpublic.fr)_
