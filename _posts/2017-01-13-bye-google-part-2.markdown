---
layout: post
title: Bye Google, part 2
date: '2017-01-13 17:40:00'
desc: Après avoir trouvé un nouveau moteur de recherche, me voici en quête d'un service mail. Bye Gmail, bye Google !
---

### Contraint de quitter Gmail ?

Pour deux raisons : j’ai évoqué la première dans le premier article (pour aller vite : mon décalage complet avec la politique de la firme, et mon attachement grandissant pour la neutralité du net et les garanties de vie privée sur le web, et je vous laisse parcourir les pages du site de la [Quadrature du Net](https://www.laquadrature.net/fr/) pour en savoir d’avantage sur le sujet).

L’autre raison, qui va peut-être me faire passer pour un parano (alors que pas du tout finalement), ce sont les lois françaises votées au quart de tour après les attentats… Des lois extrêmement liberticides, et encore plus indignes sur l’intrusion qu’elles permettent dans la vie privée des gens. Pour résumer, l’état d’urgence et la liberté sur internet ne font pas DU TOUT bon ménage, et, je me répète, si je n’ai rien à cacher, je n’ai pas non plus envie d’être « espionnable » et encore moins espionné. Inutile de dire que je vous recommande [_Citizenfour_](https://www.youtube.com/watch?v=1g2UaZEFdIw), un documentaire incroyable sur la rencontre de Snowden et Glenn Greenwald pour les révélations qu’on connait :

<!--kg-card-begin: html--><iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/841530af-7c72-4207-a318-6f17165683ed" frameborder="0" allowfullscreen></iframe><!--kg-card-end: html-->
* * *

### Changer complètement mes habitudes

Depuis quelques temps maintenant, une phrase tourne dans ma tête sur bien des sujets, et une fois n’est pas coutume, ce fut valable sur celui-ci : le confort est une drogue dure.  
Et par confort, j’entends ici les habitudes que j’avais et qui (je le pensais fort) me facilitaient la vie sur internet : je les pensais plus flexibles, plus efficientes que n’importe quelle autre type de configuration… Je me trompais évidemment très fort.

Et j’ai compris cela en cherchant à quitter Gmail, sans pour autant souscrire à un service dont les serveurs étaient installés dans un pays dont les lois étaient similaires aux nôtres sur ce sujet : exit donc la France, la Grande Bretagne, les États Unis évidemment… et j’en passe. Exit aussi bien des services qui ont l’air top, mais qui auraient pu du jour au lendemain [chuter comme Lavabit l’a fait](https://fr.wikipedia.org/wiki/Lavabit) (ancien serveur mail au top de la sécurité et de la vie privée, hébergé aux US, et dont les pressions du FBI pour avoir accès aux données ont contraint le fondateur à arrêter le service… Mais qui reprend [bientôt](https://lavabit.com/) su service ?). Je cherchais également un outil / une plateforme avec chiffrage des données, au cas où…

Il me fallait donc changer complètement mes habitudes. Et j’avais peur du compromis… Sauf que nous ne vivons pas en 1995, ni même en 2005, et nous trouvons parfois des solutions conformes à nos attentes, en l’occurrence pour moi : un service d’email en ligne uniquement (je ne veux pas stocker ma messagerie sur mon ordinateur ou passer par Thunderbird) avec une app android « propriétaire ».  
J’avais la solution [Unseen.is](https://unseen.is/), hébergé et développé en Islande. Un rêve éveillé, à ceci près que leur interface de gestion email est bien rustre pour moi. Et puis [Protonmail](https://protonmail.com/) s’est avéré champion toutes catégories dans ma recherche : créé par des gens du CERN en Suisse, hébergé là bas, customisable et très agréable à utiliser, et une appli android très très honnête.

Évidemment, là où Google est entièrement « gratuit » (la gratuité n’existe pas hein… ne soyons pas trop hippie non plus), ces plateformes ne le sont pas. Enfin si, mais l’espace de stockage est minime… J’ai opté pour une solution payante, avec un peu de stockage, et la sensation de m’engager avec des gens qui développent de vrais outils au service des utilisateurs. Cette solution me permet de créer et gérer des alias encore plus aisément qu’avec Gmail, j’ai failli en sauter de joie, moi qui, par ma double activité de musicien / fromager (et blogueur ahah), aie parfois besoin de multiplier les identités.

Je découvre donc une nouvelle façon de procéder : la consultation de mes mails est une vraie correspondance, et pas un onglet omniprésent à partir duquel je vais partout via les outils de la suite Google : je ne pars pas de mon Gmail pour aller dans un Doc, puis noter dans mon agenda, et faire une recherche, etc. Ma relation à internet est beaucoup plus spontanée, beaucoup plus variée, improvisée. J’ai à nouveau l’impression de mon adolescence où je me SERVAIS d’internet, alors que jusqu’à il y a quelques mois, je me contentais de passer du temps dessus (bien trop d’ailleurs !).

* * *

### **Le gros du boulot est fait !**

Nouveau moteur de recherche, nouvel email, je me suis rendu compte une fois ces deux étapes passées, que je n’avais finalement pas besoin de grand chose d’autre… Chose qui m’est confirmée chaque jour depuis que j’utilise internet différemment sur ces deux aspects centraux.

Parce que j’avais bien cherché des solutions en ligne pour remplacer Google Agenda, Google Doc, etc. J’étais notamment tombé sur l’infatigable mais néanmoins superbement moche suite Framasoft. Open Source, libre, en constante évolution, mais en ai-je vraiment besoin ? Non. (D’autant que je ne suis pas certain que l’hébergement soit safe…)  
J’ai besoin de rechercher sur internet de manière ouverte, et non en boucle fermée, et de consulter / envoyer mes mails tranquillement.

Une sorte de quête du minimalisme et un repositionnement plus centré et équilibré. Mon cerveau s’en porte beaucoup mieux, mon horizon culturel aussi !  
Il ne s’agit pas d’une démarche de boycott d’ailleurs : je continue de regarder des vidéos sur Youtube, par exemple, et si j’avais à en faire, ce serait sans doute là bas que je les hébergerais. Après tout, quand on publie une vidéo sur internet, c’est qu’on désire montrer quelque chose, sciemment. Or, toutes les données que nous avons sur la toile ne font pas forcément toujours parti de celles que nous désirons montrer sciemment… Et c’est exactement cette réalité qui m’a menée à cette démarche.

Part 3 à venir : Android ROMs, Gapps… les trucs de smartphone quoi.

* * *

### PS : Quelques vidéos utiles pour mieux comprendre…

J’aborde le sujet de ma propre perspective, ce n’est évidemment pas forcément entièrement expliqué ou même explicite. C’est sans doute peu documenté, peu sourcé et peu à même de vous renseigner réellement sur comment faire si vous vous posez les mêmes questions que moi. Mais j’ai fait ce blog pour partager mes modestes démarches, à mon échelle, pas pour donner des tips et astuces, et encore moins des leçons techniques ou morales…  
L’aspect technique et d’information, justement, appartient à d’autres, qui se décarcassent d’avantage. Et si j’avais cité Korben dans l’article précédent, je vous poste ici la collection « Internet » des vidéos réalisées par [Thinkerview](https://www.youtube.com/user/thinkerview), toujours prête à poser des questions pleines de sens, sans vergogne, à des invités triés sur le volet :

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/videoseries?list=PLnRz6CkWwLlJVro8jfdISzsyPoZckajaW" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>
