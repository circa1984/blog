---
layout: post
title: De la roue à la 5G
date: '2020-12-20 21:00:00'
desc: la 5G a été un grand débat débat qui a soulevé, à la marge de son déroulement chaotique, de très bonnes questions quant au sens qu'une technologie donne à la société.
---

2020 aura été une année de polarisation. Sur de nombreux sujets, on aura écouté, entendu, et plus ou moins compris la segmentation de plus en plus vive des avis et des opinions de ce qui devenaient des camps adverses et belliqueux.
En toute honnêteté, sur le sujet précis de la 5G, l'aspect techno pur me parait, pour ma part, complètement insaisissable. Je ne suis pas capable, je n'ai pas la compétence, de comprendre ce que cette technologie / technique implique d'ingénierie.
Ceci étant, j'ai été lire et croisé des études, parcouru des threads de vulgarisation et de debunk, et j'ai aussi fait appel à quelques outils logiques par la perspective qui est la mienne, celle de la sociologie et de l'anthropologie.
Bref, voici en quelque sorte mon avis sur la 5G...

### PROUESSE TECH ET QUESTIONS
Elles ont été si nombreuses, ces étapes de société(s) où une tech (technique ou technologie) vient participer à une transformation du quotidien, du présent et du futur. Des stades clefs où quelque chose semble cliquer et impliquer, comme par vagues, tout un tas de transformations à l'échelle de la société.
La téléphonie fait partie de ces séismes qui ont bouleversé notre société, et qui continue, d'une tech à l'autre, de la 4G à la 5G par exemple, de construire un présent et un futur aux implications diverses, importantes et profondes, sur un grand nombre d'aspects de la société.
Je parle volontiers de prouesse parce qu'il y a, selon moi, une dimension inédite, nouvelle, vertigineuse et clairement pas accessible à tout le monde. Et pour ma part, je trouve que le developpement des techs réseau (internet / téléphonie / data, peu importe) relève de la prouesse en terme d'impact sur la société.

Mais de quels impacts on parle exactement ?
Le gros du sujet concerne l'écologie. En tout cas sur twitter, ou les camps ennemis se battent à coup de rant pas possible où on parle d'effet rebond, de consommation de données, d'impact écologique, etc etc.
[Bon, après quelques fouilles](https://twitter.com/GoldbergNic/status/1340229460310822912), perso ce qui m'interesse le plus c'est de savoir si la 5G va permettre d'utiliser les terminaux (comprenez, les smartphones, grosso modo) plus longtemps... Parce que le coeur du sujet chez moi, c'est la dimension politique et sociale de la 5G, et notamment ces questions là :
- quid des rapports de prédation coloniale Nord-Sud dans l'exploitation des minerais dont la tech a besoin ?
- quid de la législation permettant la protection des individus devant l'accélération de la société de surveillance ?
- quid de l'amélioration des circuits de recyclage / réutilisation des ressources nécessaires à l'exploitation de ces technos ?

Clairement, je ne crois pas que la 5G soit particulièrement responsable de mes angoisses devant les réponses à ces trois questions qui se dessinent ces derniers temps à l'horizon... Je ne crois pas que la 5G soit intrinsèquement dangereuse quand on parle de technopolice, de prédation coloniale, ou d'écologie... Du coup, tout le foin sur la 5G sur ces questions là concluant au fait que la 5G c'est du caca, j'ai un peu de mal à m'en satisfaire.
Ces questions me sont très importantes. J'aimerais beaucoup qu'on leur dessine démocratiquement des réponses satisfaisantes. Mais la prouesse tech est toujours plus rapide, particulièrement dans le cadre du paradigme capitaliste, que la Loi... La société, elle, se retrouve à gérer l'écho, plus ou moins bien. Aujourd'hui avec la 5G, hier avec le béton, le feu, le nucléaire, ou la roue...

### LA ROUE

La roue a une histoire de dingue. On ne se doute pas que ce truc aussi banal et quotidien pour nous, a pu avoir autant d'impact politique et social dans des sociétés, quelque soit l'époque.
[Dans cet article](https://next.liberation.fr/livres/2020/03/11/la-roue-fortunes-et-infortunes-d-une-invention-millenaire_1781330), j'apprenais par exemple que certaines sociétés avaient refusé la roue. Refusé, la roue. Donc, on a montré le projet aux dirigeants politiques, et après concertation ou non, ils ont dit "écoutez, on vous remercie pour la prez, c'était très interessant, mais on va attendre d'avoir un peu de recul sur ce que ca donne chez vos autres clients."
Je laisse les meilleurs futurologues inventer une société moderne sans roue, parce que c'est clairement fou ce que ca implique à tout point de vue. Et la profondeur et la densité de ces implications est d'ailleurs exactement la raison (sérieuse cette fois-ci) du pourquoi les dirigeants politiques des sociétés refusant la roue ont dit non.
La roue implique effectivement du mouvement. Un mouvement qui dépasse l'échelle purement corporelle, musculaire, humaine. Qui la dépasse en rapidité, en facilité, en énergie développé. la roue, c'est une accélération considérable. On peut donc supposer que les questions politiques de la roue est la suivante :
- Quelles sont les champs dans lesquels la roue va bouleverser les choses ?
- La société est-elle capable de soutenir de tels bouleversements ?
- De tels bouleversements sont-ils acceptables relativement au paradigme idéologique (sinon théologique) de la société ?

C'est là que ca devient interessant de parler de technique et de technologie. Est-ce que la société est capable de juger si une technologie est souhaitable ou non pour son épanouissement ?
Certaines sociétés ont statué que non. D'autres que oui. Et la question s'est posé et se pose à chaque nouvelle prouesse tech, qui s'imbrique à toutes les précédentes, et impliquent les suivantes.

### 5G PARTOUT, JUSTICE NULLE PART

Je m'en fiche de la 5G perso. Enfin, je m'en fiche. En fait ce n'est pas ce qui m'intéresse quoi. Je suis persuadé que ce n'est pas la 5G *en particulier* qui amènera davantage de sécurité alimentaire, d'égalité dans la diversité, d'accès au logement, de sécurité. Et c'est pourtant PILE POIL ces sujets qui m'intéressent. Si la société, quelque soit l'échelle, décide de s'emparer d'une techno pour améliorer la vie de toutes et tous sur un ou plusieurs de ces sujets, alors top, merci, bravo, prenez ma thune !
Mais je vais pas remercier la 5G. Ce seront avant tout des gens qui décideront de développer une idée grâce à elle, pour le meilleur et le bien commun.

Alors, l'argument opposé est évidemment qu'on peut aussi développer le pire. Exact, bravo, super démo de logique les frères ! Mais ca ne prouve toujours pas que c'est la faute de la 5G.
Si la 5G accélère la technopolice, elle ne sera jamais qu'un accélérateur. Si elle accélère la consommation et l'exploitation coloniale de minerais rares, elle ne sera jamais qu'un accélérateur. Le responsable c'est le marché et le capitalisme de surveillance.

Donc finalement, on retombe au même endroit que nos camarades des sociétés disparues qui ont refusé ou accepté la roue pour des raisons politiques : celui où l'on doit faire un choix, sinon des choix politiques.

2022 c'est dans deux ans. Présidentielle et législatives vont se succéder et celles et ceux qui voteront auront tout le loisir de chercher autant que possible à faire ce choix. C'est vrai, il *suffira* de trouver un ou une candidate qui dira clairement :
- que l'implémentation de nouvelles technologies ne pourra se faire sans abandon des postures de prédation économique coloniale sur les pays producteurs / exportateurs de minerais
- que ces technologies devront garantir le respect de l'intimité et de la dignité de chacun, et non favoriser la technopolice
- que des réseaux de recyclage des matériaux seront implantés pour favoriser et maximiser recyclage et réutilisation

Facile, non ?

La société de demain que je souhaite n'est pas une société avec ou sans 5G. C'est une société décoloniale, inclusive, juste, et horizontale. La 5G a la capacité d'aider à construire cette société, autant qu'elle a la capacité d'aider à construire l'inverse. Mais dans les deux cas, elle n'en a pas le pouvoir.

Sociologiquement et anthropologiquement, la tech modifiera le cours de nos vies, à chaque invention de la nouvelle *roue*. C'est donc vertigineux pour les sciences humaines et sociales, j'en conviens, parce que l'echo est profond, durable et puissant. Mais à quoi bon s'écharper entre nous ?
*In fine* ce qui primera c'est le déroulement plus ou moins démocratique de l'action politique. Le déroulement du choix qui sera fait par les dirigeants sera ce gouvernail qui orientera l'écho d'un coté ou de l'autre de toutes les problématiques embarquées.

Un moratoire sur la 5G ? A quoi bon ? Si on veut faire bouger les choses et limiter l'accélération de cette société catastrophique, les moratoires ne devraient-ils pas plutôt porter sur les 3 sujets mentionnés plus haut ?
