---
layout: post
title: J'ai testé Signal, et je l'ai adopté !
date: '2018-11-07 07:07:00'
desc: Il ne me donne pas spécialement l'haleine fraiche puisque je ne parlerai pas du dentifrice, mais de l'application de messagerie. Retour sur mon expérience et ce que j'ai appris de cet app ces quelques derniers mois !
---

Il ne me donne pas spécialement l'haleine fraiche puisque je ne parlerai pas du dentifrice, mais de l'application de messagerie.

Il y a pléthore d'app de sms/messagerie sur téléphone, mais peu égale Signal, qui fait un strike en tout point. [J'avais parlé de Riot](https://circa1984.net/jai-teste-riot-im-et-je-lai-adopte/) (qui est toujours aussi parfait our le travail en équipe), Signal a les mêmes standards, mais s'applique à être le couteau suisse parfait de la messagerie mobile. Et pas que.

Retour sur mon expérience et ce que j'ai appris de cet app ces quelques derniers mois !

* * *

### Signal : Silence + FB Messenger + Whatsapp en une seule app, et mieux !

Jusqu'à il y a un gros 6 mois, j'utilisais donc Silence (anciennement SMS Secure) pour les sms, et Whatsapp majoritairement. Et puis, en suivant les recommandations de beaucoup sur Mastodon, et en fouinant/trouvant les avis de personnes disons de confiance (coucou Edward Snowden), je me suis retrouvé à installer Signal à la place de SIlence. J'avais déjà utilisé Signal, mais je ne sais pas pourquoi, je lui préférais Silence... Bref, j'ai persevéré dans mon test, et j'ai tellement bien fait !

Signal est un outil open source développé depuis 2014 par [Open Whisper Systems](https://en.wikipedia.org/wiki/Open_Whisper_Systems), une organisation construite et leadée par [Moxie Marlinspike](https://en.wikipedia.org/wiki/Moxie_Marlinspike). Cette organisation a développé de nombreux logiciels et protocoles de sécurité et notamment de chiffrement, utilisés entre autres par Whatsapp, FB, TW et Google Allo. Autrement dit, lorsque nous utilisons ces services propriétaires dont on ne sait rien du code ni de comment les données sont utilisées, nous utilisons quelque chose qui est accessible dans une app qui s'occupe mieux de ces services, qui a inventé les protocoles de sécurité, et qui est open source ! Quand on réalise cela, et qu'on est sain d'esprit, normalement, on fonce...

Parce que oui : Signal fait tout mieux, parce qu'elle s'occupe de chaque service en une seule app. On retrouve donc la faculté d'avoir des discussions de groupe (Messenger / Whatsapp), un client desktop comme Whatsapp, nos sms, et la possibilité d'envoyer des messages éphémères. Tout ça, en même temps, open source et chiffré. Leur [site web](https://www.signal.org/) est clair, [leur blog est extrêmement intéressant](https://www.signal.org/blog/), et [la doc est rudimentaire mais honnête](https://support.signal.org/hc/en-us) !

Signal est donc également chiffré ! D'où tout ce déferlement d'amour de la part de Snowden et d'autres à son sujet. Et Signal chiffre **tout** , éliminant même les données sensibles de vos fichiers (par exemple, l'EXIF, GPS, de vos photos). Et ça, c'est quand même vachement top hein.

* * *

### A quoi ça ressemble en vrai ?

Au delà de l'aspect service, Signal est aussi séduisant par sa simplicité esthétique. D'abord l'application mobile, disons tout à fait classique à coté des autres. Mais surtout simple à configurer et comprendre. Toutes les forces de Signal sont présentées de façon compréhensibles. Et on retrouve évidemment les features habituelles.

Ensuite l'application desktop, que j'adore et qui franchement, rendra la vie grandement facile pour celles et ceux d'entre vous qui bossent beaucoup devant un écran ! On retrouve ses conversations évidemment, les contacts qui ont Signal (et uniquement eux !), la possibilité donc d'envoyer des messages éphémères, le mode "sombre" qu'on a déjà dans l'app mobile, etc.

En bref, il n'y a vraiment aucune raison de ne pas être sur Signal lorsqu'on utilise FB Messenger + Whatsapp + SMS. Aucune. Alors moi j'y suis, et je vais essayer de passer le mot. Signal est un outil à la ligne éditoriale nécessaire, et qui conjugue à cela l'exacte réponse à nos besoins ! VAMOS !!!
