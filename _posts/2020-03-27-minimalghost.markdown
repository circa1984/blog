---
layout: post
title: Responsive Web Design certifié
date: '2020-03-27 18:55:01'
desc: Grâce à un pote, je prenais connaissance de l'existence de [FreeCodeCamp](https://www.freecodecamp.org/), et hier, j'obtenais ma première certification, en Responsive Web Design eheh ! J'en ai profité pour revoir intégralement (pour la énième fois) le template de ce blog. Et voici donc le pourquoi et le comment.
---

Ces derniers jours, pendant le confinement que nous connaissons toutes et tous, j'ai travaillé à certifier et approfondir les quelques connaissances que j'ai en webdesign et en code.

Grâce à un pote, je prenais connaissance de l'existence de [FreeCodeCamp](https://www.freecodecamp.org/), et hier, j'obtenais ma première certification eheh ! J'en ai profité pour revoir intégralement (pour la énième fois) le template de ce blog. Et voici donc le pourquoi et le comment.

## Responsive design

Pendant la formation, les points que je ne connaissais pas correspondaient exactement à ce à quoi j'aspire en terme de société : de l'inclusion et de l'accessibilité. Or, mon blog, avant cette refonte, n'était clairement pas au point. Dans la refonte, je me suis donc imposé de faire de mon mieux sur le coté _responsive_ du design (qu'il soit donc accessible sur tous les écrans avec la même aisance de lecture / parcours).

L'existant n'était pas pire en soit. En terme de "beauté" c'était imparfait mais mes envies étaient présentes. Il suffisait d'un peu de connaissances approfondies et un brin de clarté dans la façon de faire, et je suis arrivé à mes fins en quelques jours.

Pour se faire, j'ai commencé par [installer une instance Ghost en local](https://ghost.org/docs/api/v3/ghost-cli/install/). Puis, j'ai attaqué le CSS de mon template en cours. Pour les plus noobs d'entre nous, j'ai principalement remplacé les unités en _px_ par des _rem / vh-vm / vmin-vmax_, en controlant que tout roulait grâce à Firefox Dev. Les dimensions de chaque chose affichées à l'écran sont donc proportionnelles à la taille de l'écran lui-même, et non absolues. J'ai également fait un peu de ménage dans le CSS en virant ce qui ne servait pas (et il doit encore en rester encore - l'inconvénient de ne pas partir de 0).

J'ai ensuite réaménagé le HTML. Pour le coup, j'ai conçu le template sur les besoins purs et simples de mon blog. Pas certain que ce soit aussi concluant sur un blog classique, utilisant des images, etc. L'idée était de replacer le menu _social_ (les liens vers les réseaux sociaux quoi...) et de dégager la barre des tags qui permettait de naviguer dans les tags que je jugeais les plus pertinents. Le sous-titre du blog est également parti (RIP petit ange !). Toutes ces choses étaient inutiles, en fait. En tout cas pour moi.

Enfin, retour au CSS, pour un travail de fond sur les polices (les _fonts_) : la taille d'abord (toujours sans _px_), ensuite le _letter-spacing_ (la taille de l'espace entre les lettres), et pour finir l'épaisseur du trait (_font-weight_). Parce que ça pour le coup, ca fait tout en terme de design et de _beauté_. Et hop !

## Accéssibilité

C'est la prochaine étape, mais c'est la motivation première. Prochaine étape parce qu'il s'agit de rendre accessible tout le contenu du site aux personnes malvoyantes, juste avec le code. Il fallait donc préalablement le préparer ce code. Le revitaliser, l'écrémer, et évidemment mieux le comprendre !

Donc les prochaines étapes vont en ce sens, et sont donc :

- rendre audible tout le contenu du blog
- travailler sur le contraste
- rendre ce template utilisable sur n'importe quelle instance Ghost

Il y a en effet un internet audible pour les malvoyants, et ce blog doit en faire parti. Le contraste peut aussi les aider à accéder au contenu d'un site web, et j'espère bien que ce sera le cas pour ici !

Enfin, produire cet effort sur le code et sur le template de ce blog ne vaudrait pas grand chose s'il n'était pas finalement disponible pour tout le monde. J'ai donc créé un dépôt git pour mettre à disposition le travail effectué au jour le jour, et éventuellement profiter des connaissances d'une communauté plus à l'aise que moi sur ces choses enssentielles du web !

[Le dépôt est ici, prenez, utilisez, forkez, et partagez !](https://github.com/gregoiremarty/minimalghost)
