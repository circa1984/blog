---
layout: post
title: Chimamanda Adichie, Datas, Justin Trudeau… Revue de net épisode 2 !
date: '2017-02-15 16:43:00'
desc: Épisode 2 de la revue de net, avec cette fois ci deux vidéos (un TED et un docu) et un article…
---

Épisode 2 de la revue de net, avec cette fois ci deux vidéos (un TED et un docu) et un article…

* * *

### [VIDÉO] Chimamanda Adichie : le danger d’une histoire unique

Nigériane, romancière (elle se présente comme _story-teller_), Chimamanda Adichie déconstruit avec simplicité combien il est nécessaire de rompre avec le reflexe de coller une réalité à partir d’une histoire unique. Elle dissèque comment l’amalgame est créé, et comment il crée des réalités fantaisistes aux conséquences parfois (souvent, trop) compliquées…

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/D9Ihs241zeg?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>
* * *

### [VIDÉO] Democracy : la ruée vers les datas

Un docu en replay via [Arte+7](http://tinyurl.com/jeu3b37), donc dépêchez vous, sinon c’est VOD !Une plongée dans la rude négociation de quelque chose qu’il semble fou de négocier : la vie privée. Une vie privée mise à mal sur internet dans le monde entier, et ici il est question de l’UE. [Cliquer ici pour la video](http://tinyurl.com/jeu3b37).

* * *

### [ARTICLE] Justin Trudeau is not your friend (en anglais)

Enfin ! Enfin un article remettant dans une perspective plus objective et moins affective le socle dont est fait Justin Trudeau, la réalité de ses idées, de son entourage politique, et de sa politique. Un article qui sert de symbole que je trouve valable et extensible à bien des noms de la gauche social-démocrate dans le monde (et je ne suis pas de droite…). Je vous laisse reconnaître ceux pour qui ce portrait est valable également.

<!--kg-card-begin: html--><quote>" […] He called himself a feminist and assembled a gender-balanced cabinet. He appointed a significant number of people of color to cabinet positions, including “badass” defense minister Harjit Sajjan. Sajjan’s record as a Canadian intelligence officer in Afghanistan who turned prisoners of war over to the Afghan forces to be tortured gets left out of most media coverage, which instead focuses on Trudeau’s multicultural administration. […]

    […] In fact, as Bell points out, Canadian arms dealers enjoy more — not less — freedom under Trudeau. The previous laws forbade arms exports that could be “diverted to ends that could threaten the security of Canada, its allies, or other countries or people.” This language stopped Harper from making certain deals with the Saudis, thanks to their massacres of civilians in Yemen and Bahrain. Trudeau and his pals removed the reference to “other countries or people” and replaced it with “civilians,” limiting the prohibition to Canadian citizens and thereby opening up trade. The fact that no opposition campaign, no peace movement, not even a peep from those within the NDP or the Liberals, who have traditionally opposed Canada’s role as an arms-dealing state, has emerged testifies to the Trudeau team’s ability to manufacture consent. […]"</quote><!--kg-card-end: html-->

**[ARTICLE COMPLET ICI](https://www.jacobinmag.com/2016/09/justin-trudeau-unions-environment-arms-saudi-arabia/)**

* * *

Et voilà pour cette revue de net, en attendant la prochaine !
