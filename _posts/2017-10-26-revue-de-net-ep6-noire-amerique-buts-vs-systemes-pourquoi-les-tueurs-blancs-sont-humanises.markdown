---
layout: post
title: 'Revue de Net ep6 : Noire Amérique + Buts vs Systèmes + Pourquoi les tueurs
  blancs sont humanisés ?'
date: '2017-10-26 15:25:00'
desc: Je le dis tout de suite, les trois liens n’ont pas grande relation entre eux. Quoique le premier et le dernier sans doute. Celui du milieu, non.
---

Je le dis tout de suite, les trois liens n’ont pas grande relation entre eux. Quoique le premier et le dernier sans doute. Celui du milieu, non.

Après quelques articles très « concepts », je reviens à mes revues de net (je me les aime bien…). Pocket, cette extension Firefox magnifique qui me permet de sauvegarder et lire plus tard des tas de choses passionnantes (ma passion, 90% de mon temps libre…). En l’occurrence une série documentaire chez Arte, et deux articles qui, ne m’en voulez pas, sont en anglais. C’est parti !

* * *

### Noire Amérique : le grand 8 d’Arte !

J’ai de nombreuses fois vu passé le lien ça et là entre les différents réseaux sociaux où je suis… Il n’y a donc pas vraiment de la découverte, mais cette série tombe à point nommé dans le contexte actuel américain. Et puis elle n’est pas non plus sans une grande résonance dans le contexte français… En 8 épisodes de grosso modo 5 minutes chacun, à voir. _([cliquer ici](https://www.arte.tv/fr/videos/RC-014284/noire-amerique/) pour accéder à la série sur le site d’Arte)_

* * *

James Clear : Oublions de nous fixer des buts, créons des systèmes !

Un excellent article de James Clear. le genre d’article dont raffole la communauté Medium, mais c’est bien sur le site de M. Clear qu’on lira comment zapper nos buts pour leur préférer des systèmes. En d’autres termes, comment arrêter de fonctionner aux objectifs, en essayant de composer des modes de fonctionnement qui permettent la progression dans le sens voulu. Un article sur l’individu et sa motivation, mais facilement extensible, à mon sens, au fonctionnement de groupe. L’article est [lisible ici !](https://jamesclear.com/goals-systems)

* * *

### Rebecca Traister (The Cut) : Pourquoi nous rendons humains les tueurs blancs ?

Il faut noter que cette dame écrit parfois de sacrés articles. Celui-là en est un autre, que j’apprécie autant que [celui-ci](https://www.thecut.com/2016/12/identity-issues-are-economic-issues.html). Cette fois, il s’agit de poser les pieds dans le plat. Une nouvelle fois, un article en anglais, discutant de la société américaine, mais dont le questionnement et la problématique peuvent très bien servir ici, en France, tant la situation est similaire (et pourtant, je n’aime pas la transposition automatique des problèmes d’ailleurs ici).

R. Traister se demande bien pourquoi les tueurs blancs sont toujours présentés différemment des autres, et pourquoi leur portrait dressé par les médias est-il toujours a minima compassionnel, quand il n’est pas humanisant ou relativisant. [Ça se lit ici](https://www.thecut.com/2015/12/white-male-murderers-planned-parenthood-robert-dear.html), et c’est essentiel !

* * *

Et comme je ne peux pas me résoudre à finir cet article par les portraits de ces deux terroristes, je vous laisse !
