---
layout: post
title: Bye Google, part 1
date: '2017-01-04 17:35:00'
desc: Plus j'en apprenais sur Google, plus ça me dégoutait de dépendre d'eux. Alors j'ai décidé de quitter Google, et de chercher des alternatives sérieuses et honnêtes.
---

### Pourquoi ?

D’abord parce que plus j’en apprenais sur Google, plus ça me dégoutait de dépendre autant d’une entreprise comme celle-ci. A ce sujet, et s’il vous manque des billes, sachez juste que je tiens à mes données personnelles, je ne suis pas du tout pro-intrusion dans ma vie privée ou pro, surtout quand la collecte des données sert d’autres desseins que les miens (et encore plus quand il s’agit de choses à l’opposée de mes valeurs…).

Pour autant, je ne me voyais pas du tout quitter Google du jour au lendemain, d’un coup, entièrement : j’y avais une partie de mon activité professionnelle, un grand nombre de données stockées ça et là chez eux sous diverses formes et que je devais récupérer proprement et avec organisation, et également parce que c’était encore le meilleur moyen de se retrouver chez un concurrent qui finalement a la même politique.

* * *

### Phase 1 – Le Moteur de Recherche

En regardant bien, je me suis rendu compte que l’utilisation d’un moteur de recherche était quasiment centrale dans mon comportement sur internet. Je suis plutôt quelqu’un de très curieux, qui va très profond dans ses recherches, et j’ai souvent besoin de sourcer de façon très hétérogène le sujet que je fouille.  
Google a cette mauvaise habitude de boucle fermée, faisant apparaitre en résultats de recherche des sites qu’on visite habituellement… On se sort jamais de sa zone de confort, on ne découvre plus rien de nouveau, ça devient planplan pour les curieux comme moi…

Je me suis donc mis en quête d’un moteur qui ne profilait pas mes entrées / recherches. Et après quelques mois d’utilisation de [Duckduckgo](https://duckduckgo.com/), tout à fait honnête et correspondant à mes besoins stricts (interface proche de Google, expérience d’utilisation quasi identique), je me suis rendu compte que j’avais besoin de changer MON utilisation du net. Et de quitter le conditionnement Google finalement…

Je suis tombé sur le blog de [Korben](http://korben.info/), que je suis avec attention tant il parle bien de tout ce qui est difficile à comprendre à propos de sécurité internet, internet, logiciels, etc. Lui même avait fait cette démarche et opté pour Qwant [(cf ce post)](https://korben.info/qwant-mon-retour-apres-1-mois-de-test.html). Sa conclusion m’avait encouragé à suivre mon instinct.

* * *

### Qwant – merci pour la vie !

J’ai (re)découvert [Qwant](https://www.qwant.com/?l=fr). Je l’avais testé juste avant le canard, mais son interface était radicalement trop changeante pour moi… Et c’est pour cette raison que j’y suis revenu in fine.

Quelques minutes de configs plus tard, je me trouvais face à un truc qui me correspondait en tout et pour tout. Fait rare pour moi, surtout sur internet !  
– l’interface est nickel, pratique et intuitive  
– les résultats sont pertinents et je n’ai pas besoin de faire une contre recherche sur Google  
– cocorico ! Qwant est français, et j’aime bien ça !  
– (détail qui a son importance pour moi) le/la CM qui gère le compte twitter fait un boulot remarquable, drôle et disponible, et surtout montrant que l’équipe se démène pour que Qwant avance dans le sens de ses utilisateurs (et non l’inverse)  
– il y a une [version pour les enfants](https://www.qwantjunior.com/), et ça aussi j’aime bien !

Quelques mois après avoir opté pour cette option, j’apprenais la collaboration étroite entre Qwant et Mozilla… Signe d’un alignement des planètes !

C’était un premier pas hors de Google. Sans aucun regret, au contraire. J’essaie de convertir autour de moi, mes proches et mes connaissances, à changer de moteur de recherche.  
J’ai d’ailleurs appris en discutant que le moteur de recherche [Ecosia](https://www.ecosia.org/) faisait du bon boulot, sur une orientation plus écologiste : vos recherches financent la plantation d’arbre ! Ça me parle, mais (désolé… le rabat-joie de service), Ecosia utilise Bing, le moteur de Microsoft… Et ne mentionne nulle part ses positions à propos d’un internet libre.

A ce titre, il n’est pas impossible que je reparle d’Ecosia ici. Leur démarche est réellement intéressante. Si on s’y met à plusieurs, genre un milliard quoi, calmement, on pourrait éventuellement leur dire de quitter Bing et de s’associer à Qwant plutôt ?

A suivre : boite mail, Gapps sur android, …
