---
layout: post
title: Le jour où j’ai décidé de devenir locavore, part 1
date: '2017-01-11 17:56:00'
desc: Manger local est une de mes préoccupations principales. Le locavorisme est un objectif de société qui, à mon sens, tient debout à tout point de vue. Parlons en.
---

Le végétarisme et le véganisme (végétalisme en français, non ?) sont des mouvements qui gagnent beaucoup d’ampleur en ce moment, beaucoup de visibilité aussi, par le jeu de celles et ceux qui les adoptent et défendent, et par celui de celles et ceux qui le trashent… Malheureusement, il y a _in fine_ assez peu de critiques, encore moins de bonnes.  
L’alimentation et la nutrition sont des trucs qui me tiennent à cœur depuis l’aube de ma vingtaine… et qui après une petite pause, reprennent leur place dans mon quotidien, à 32ans.

* * *

### La pénibilité de la culpabilisation quand ce sujet est abordé

Bon je l’admets, je voulais titrer cet article « le jour où je ne suis pas devenu vegan », mais les clickbaits sont insupportables et je n’avais pas envie de faire de la provoc’ pour rien…  
Parce que finalement, si le sujet est hyper pertinent (le véganisme, le végétarisme, ont des bases revendicatrices et théoriques difficilement attaquables quand on est un peu sensible au futur de la planète), les discussions à son propos le sont beaucoup moins : trop de leçons de morale, de culpabilisation, pas ou très peu de concret du quotidien ([Rémi Gaillard](https://www.facebook.com/gaillardremi) fait office d’exception au rayon individuel, [Sea Shepperd](http://www.seashepherd.fr/) fait office de HÉROS au rayon collectif), aucune once de pédagogie, un ton accusatoire quasi systématique si on fait PILE ce qu’il faut.  
C’est pénible…

Jamais la culpabilisation à outrance n’a formé le cerveau des gens à apprécier une démarche, fusse-t’elle la plus gracieuse et la plus pure qui soit… Au mieux cela créé des clivages, des clans, des oppositions, et forcément à la fin : que des perdants ! (offre valable également pour les sujets politiques : le vote, l’orientation politique, etc.)

Et puis il y a quelques longs mois maintenant, une amie a écrit un excellent article sur comment elle était devenu végétarienne (le titre de mon article est d’ailleurs inspiré du sien !). Je vous recommande la lecture (rapide), parce que justement, elle parvient avec brio à parler naturellement et avec humour de sa décision, et à éviter tout ce qui est usuellement fatigant quand le sujet est abordé :

> **[Le jour où je suis devenue végétarienne](http://www.monkeydose.com/le-jour-ou-je-suis-devenue-vegetarienne/)**   
> _Je suis alsacienne et à l’état embryonnaire je devais déjà me gaver de bidoche. J’adore la tarte flambée, un apéro ne se fait pas sans un bon sauciflard, et je ne refuse jamais un bon steak et encore moins le sandoc merguez des puces de Maubeuge…_ – Par Nadia Wicker

* * *

### **De quoi on parle déjà ?!**

En France, depuis la seconde guerre mondiale, le régime alimentaire de base est un régime très proche de celui d’une période de manque. Nous sommes pourtant en 2017 maintenant, en surproduction sur quasiment tous les produits… Le « manque » est passé… L’industrie agro-alimentaire est obsolète, mais l’obsolescence, ça rapporte quand même.

Idéalement, nous devrions rebattre les cartes et adapter notre consommation en fonction de nos réels besoins biologiques et physiologiques (qui différent non seulement à l’échelle individuelle mais aussi à des échelles liées aux différents climats, reliefs, etc), de ce que nous sommes capables et à-même de produire, en respectant l’environnement qui nous sert à la fois d’habitat, de surface de culture et de grenier.  
MAIS NON ! Les décisions politiques ne vont pas en ce sens, et petit à petit, des actions et des réactions se sont construites autour de la défense des animaux, d’un retour au naturel, et d’autres concepts qui relèvent clairement du bon sens.

Le végétarisme d’abord puis carrément le végétalisme, que l’américanisation de tout a aujourd’hui nommé véganisme (si j’ai bien compris hein, je ne demande d’ailleurs qu’à bien comprendre). Si le premier refuse de manger des cadavres d’animaux, il accepte tout de même l’élevage de ceux-ci pour en récolter d’autres sources alimentaires, notamment le lait et les œufs. Le second lui refuse catégoriquement que l’homme se fournissent en quoi que ce soit chez l’animal (quelque soit le projet), et dans l’assiette, puisque c’est ce dont j’ai envie de parler dans cet article, ne compose son alimentation que de sources rigoureusement végétales.

Ajoutez à ces deux courants la grande démarche vers le bio, le retour au naturel et une grande défiance vis à vis des OGM, des chimies agro-alimentaires, et autres, et vous obtenez une volonté à peu près unanime de manger plus sainement, sans salir la planète, sans abimer l’écosystème, sans tuer personne.  
A JUSTE TITRE : il est pour moi incompréhensible de ne pas aller dans ce sens !

* * *

### « Ouais mais ton avoine bio végan, il est produit à 20 000 bornes de chez toi non ? »

Si je comprends et partage l’intégralité des questions soulevées par ces mouvements, il y a des détails dans les concrétisations et les solutions proposées ou en place, qui me chiffonnent grandement. Et je résumerais simplement en disant qu’il est tout de même super dur aujourd’hui de ne pas heurter l’écosystème dont nous dépendons, dans sa diversité et sa richesse, quand bien même on soit vegan ou végétarien et bio…  
Super dur parce que même si notre menu respecte à la lettre toutes les conditions pour appartenir au veganisme, l’origine des produits ne garantit pas que hors de l’assiette, notre empreinte écologique ne soit pas une catastrophe.  
Autrement dit, un sac de boulgour bio et un tote bag plein de tofu n’empêcheront pas qu’ils puissent provenir de partout dans le monde, que leur culture soit catastrophique pour l’écologie locale et globale, et que leur transport laisse la même empreinte carbone que l’ensemble de la carrière de Gainsbourg chez Gauloise.

Je vous conseille cet article donc, clairement segmentant, mais qui a le mérite de poser des réflexions dénuées de nombrilisme :

> **[Trier, manger bio, prendre son vélo… ce n’est pas comme ça qu’on sauvera la planète](http://bibliobs.nouvelobs.com/idees/20161229.OBS3181/trier-manger-bio-prendre-son-velo-ce-n-est-pas-comme-ca-qu-on-sauvera-la-planete.html)**  
> _La culpabilisation des individus occulte les véritables causes de la destruction de la planète: le capitalisme et les Etats-Nations._ Par [Slavoj Zizek](https://fr.wikipedia.org/wiki/Slavoj_%C5%BDi%C5%BEek).

Je ne suis pas un inconditionnel de l’auteur (certains points de désaccord, par exemple dans cet article sur le rôle des États-Nations… mais c’est un autre sujet), mais il a le mérite de poser un tabou sur la table (et ça j’aime bien) : la culpabilisation des uns et des autres en tant qu’individus est la manière la moins pertinente de mener le débat.  
Questionner la responsabilité des politiques, en récapitulant notamment les choix qui ont été faits en la matière, et les actions/inactions comptabilisables, me semble déjà plus raisonnable.

* * *

### **N’empêche, moi à mon échelle je fais quoi ?**

C’est une question que je garde depuis une grosse dizaine d’années en tête quand il s’agit de nutrition, d’alimentation, d’écologie. Si je suis persuadé que la responsabilité principale n’est pas celle du citoyen, de l’individu, je suis également certain qu’encourager des initiatives en me comportant d’une certaine façon notamment dans l’assiette, peut à tout le moins influencer ma santé, celles de mes proches, celles de mes futurs enfants.

Parce que finalement, j’en reviens toujours, quelque soit le sujet, aux générations futures… Et donc à l’éducation et aux habitudes que nous allons leur léguer.

De cette question là nait une multitude d’angles d’attaque de la problématique, et c’est en faisant ma formation de fromager que j’y ai vu plus clair, et où toutes les petites idées séparées que j’avais depuis un moment se sont amalgamées et imbriquées parfaitement.  
J’ai effectivement compris que nous les humains, comme les autres espèces, nous dépendons du terroir où nous vivons, c’est à dire le très local ou au max le régional si on en revient à l’échelle individuelle (moi, je fais quoi à mon échelle ?). Et que la seule responsabilité qui pouvait nous revenir c’était de comprendre notre rôle dans cette échelle là, de le prendre à cœur et d’embrasser les initiatives les plus à-même de rendre ce terroir riche et fertile pour les générations futures. Je n’ai absolument rien inventé : c’était comme ça avant les guerres industrielles et mondialisées, qui ont changé bien des choses, au delà des frontières et des charniers.

Aujourd’hui il m’est donc compliqué d’envisager l’écologie (et bien d’autres choses) en le faisant de manière plus étendue que le local / régional : je suis humain, mes bras ne sont pas infinis, ma capacité d’action ne dépasse pas cette échelle, et quand elle le fait, elle s’arrête au territoire français. Un engagement politique auprès d’un bord ou d’un autre pour agrandir ces capacités ? Si leur volonté de peser dans la balance était là, la COP21 n’existerait pas. Et avant d’être un citoyen du monde, je vais au mieux essayer d’être un citoyen local. Et ce sera déjà pas mal non ?

* * *

### Le jour où j’ai décidé de devenir… locavore

Je ne sais pas si on devient du jour au lendemain quelque chose. Ou alors, la radicalité du changement nous pousse dans des retranchements tels qu’on se retrouverait au fin fond d’une campagne, à vivre en complète autonomie. Et je dis cela sans aucune ironie. Mais cela demande tout de même une franche rupture avec l’esprit bourgeois et l’amour du confort (une vraie drogue dure) dans lequel nous sommes toutes et tous (malheureusement) infusés…  
C’est vrai non ? Comment se fait-il que nous puissions débattre sur internet avec des végans ultras qui se défendent de faire du mal au règne animal quand la totalité de leur équipement technologique lié à internet est une catastrophe écologique en soi, avec en cerise sur le gâteau du travail d’enfant dans les parages ?  
Un végan abouti est un végan qu’on ne croise pas. Encore moins sur Youtube.

Cela dit, il ne s’agit pas de renier la sincérité de la démarche de quiconque. Mais de remettre le terme _démarche_ à sa place : puisqu’une démarche est par essence un mouvement d’un point vers un autre, je crois que tant que nous ne sommes pas arrivé au dernier point, la gargarisation n’est pas justifiable. Quant à la culpabilisation de l’autre, elle n’est ni nécessaire, ni digne. Quand bien même on le trouve beauf et qu’on ait le sentiment d’avoir la conscience tranquille parce qu’on va chez Naturalia.

Je ne suis donc pas DEVENU locavore, mais j’ai décidé d’essayer de mon max de le devenir, et faire mon possible pour défendre les initiatives qui vont dans ce sens. Mais tout ça, j’en parlerai dans l’épisode 2.
