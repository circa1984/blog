---
layout: post
title: Guillaume Meurice + Edward Snowden = Revue de Net ep4 !
date: '2017-06-12 15:55:00'
desc: Deux vidéos à regarder sans ordre précis, mais jusqu’au bout, qui transpirent tout ce que je me dis en ce moment sur les deux thèmes évoqués.
---

Une fois n’est pas coutume, retour sur quelques vidéos que j’ai croisé et regardé ces derniers temps sur internet. Deux cette fois ci (habituellement trois liens dans les « revues de net », mais on s’en fiche non ?).

Des vidéos qui me sont apparues d’une importance et d’une pertinence égales, par les temps qui courent. Avec les bureaux de votes encore ouverts ce week-end, les pronostics, les positionnements, et ce qui en découle : les jugements, les procès (y compris d’intention), chacun contre les autres… Et puis un gouvernement qui va se lancer dans ses ordonnances, pour réformer rapidement, sèchement, notamment sur la « sécurité », l’État d’Urgence, et autres…

Deux vidéos donc, à regarder sans ordre précis, mais jusqu’au bout, qui transpirent tout ce que je me dis en ce moment sur ces deux thèmes là.

* * *

### Square Idée – Meeting Snowden (Arte, comme d’hab…)

« Le temps d’une rencontre clandestine, Edward Snowden, Lawrence Lassig et Birgitta Jónsdóttir, figures de la lutte pour les libertés, s’interrogent sur l’avenir de la démocratie. »

C’est ce que dit le descriptif de la vidéo sur le site d’Arte. Une prof américaine de droit à Harvard, une députée islandaise et un lanceur d’alerte se rencontrent et discutent de la démocratie. Passionnant ! _([cliquer ICI](http://www.arte.tv/fr/videos/072258-000-A/square-idee)pour accéder à la vidéo)_

* * *

### Guillaume Meurice x Thinkerview

Bon, c’est pas la surprise du siècle hein, je suis très très client de [cette chaîne youtube](https://www.youtube.com/channel/UCQgWpmt02UtJkyO32HGUASQ), qui abat un travail considérable: le choix des intervenants, celui des questions, le rapport à la communauté internet fédérée autour de la chaîne, …

En décalage avec les invités actuels, généralement plutôt pointus, [Guillaume Meurice](https://www.facebook.com/PageGuillaumeMeurice/) (avec grande humilité d’ailleurs) intervient comme humoriste. Si les questions essaient de le faire sortir de ce rôle et de récolter sa pensée profonde sur l’état de la presse ou du journalisme, le résultat de l’interview est surprenant : Meurice, en filigrane, témoigne de son incapacité (ou de sa volonté) à juger celles et ceux qui l’entourent, quelques soient leurs choix, leurs positions, fussent-ils à l’opposé des siens. Et je trouve cette intégrité remarquable. Beaucoup de sympathie, de fait, pour ce type, que j’ai en plus eu le plaisir de croiser sur un marché dans le cadre de mon boulot de jour (et lui du sien). Guillaume, si tu nous regardes (voix de Michel Drucker) =)

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/CQ-nzRlDnns?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>
* * *

Bon visionnage !
