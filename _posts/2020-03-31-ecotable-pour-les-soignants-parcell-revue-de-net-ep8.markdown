---
layout: post
title: Ecotable pour les soignants, Parcel, ... revue de net ep8
date: '2020-03-31 11:36:50'
desc: Confinement oblige, nous vivons un temps suspendu et curieux où la société semble plus serrée et reliée. Alors voici 3 liens, qui ont, entre beaucoup d'autres, marqué ma quatorzaine.
---

Confinement oblige, internet est un compagnon qu'on aime plus que jamais. Auprès de lui, on peut [apprendre des trucs](/minimalghost/), on peut communiquer avec des gens, on redécouvre des instincts solidaires, partageurs, libres. Un temps suspendu où la société semble plus serrée et reliée, où les injonctions habituelles du modèle capitaliste n'ont plus de poids, et où on se rend compte que, finalement, c'est plutôt cool. Alors voici 3 liens, qui ont, entre beaucoup d'autres, marqué ma quatorzaine.

## ECOTABLE ET LES SOIGNANTS

Ecotable est une communauté qui aspire à rassembler et mettre en lumière le travail des restaurateurs à la cuisine écro-responsable. Un super taf de recensement, de valorisation, et de labelisation des tables vertueuses. Et même au delà du label, on retrouve des membres d'Ecotable dans des conférences sur le miam, et ca c'est très très cool.

Pour accompagner les efforts incroyables du personnel soignant pendant la crise sanitaire, Ecotable a créé une page Kickstarter pour permettre la livraison de repas préparés par des bêtes de chef-fe-s. Si ca c'est pas merveilleux !

[Vous trouverez le lien juste ICI !](/p/de01e713-056e-412a-9ab6-d8dc77050ee4/Alors%20voici%203%20liens,%20qui%20ont%20marqu%C3%A9%20la%20quatorzaine.) Participez, relayez, partagez !

## LE PAIN AU LEVAIN DE CELINE

On est toutes et tous cloitré.e.s à la casa ou presque. Et avec ce phénomène est né quasi instantanément un partage monstrueux de recettes et de tips culinaires sur Instagram. TOUT LE MONDE partage ses astuces, son savoir-faire, pour faire de cette période de confinement un tremplin formidable vers une prise de poids assurée et assumée.

Céline Maguet, membre de l'Agence Soif, que je connais pour son expertise en vin nature, a partagé sa recette de pain au levain maison. Alors oui, des recettes de pain au levain maison, y'en a 100 000. Clairement. Mais là, c'est un vrai tuto le truc. Pas à pas, avec vidéo à l'appui vu qu'il s'agit d'une (longue et très bien foutue) story IG.

[Voilà la recette, régalez vous !](https://www.instagram.com/stories/highlights/17845759022006818/)

## PARCEL

Parcel est une application web de calcul d'impact écologique et économique des modes de production et de consommation alimentaire. En gros, Parcel évalue ce que le local et le bio produirait comme impact sur l'emploi et le CO², sur le territoire de son choix.

Vachement plus simple que ma présentation, l'outil web permet de visualiser en quelques clics combien le local/bio serait vertueux pour les collectivités locales. je vous invite donc à[tester l'outil juste ici](https://parcel-app.org/), et à partager sur [twitter](https://twitter.com/circa1984net) ou [mastodon](https://mastodon.social/@gregoiremarty) vos résultats (et vos variables !).

Bon courage à toutes et tous pour la suite du confinement. Restez chez vous, prenez soin des votres, et mangez bien ! A bientôt !
