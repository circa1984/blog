---
layout: post
title: '100 jours sans supermarché (M. Golla) : lu et apprécié'
date: '2018-03-21 12:48:00'
desc: Il fallait bien ça, un bouquin qui permette de coller aux réalités et aux urgences actuelles en racontant simplement des solutions rencontrées, et les gens qui les proposent ou les font évoluer, perdurer.
---

Parmi les premiers articles sur ce blog, il y a évidemment la trilogie ([un](https://circa1984.net/le-jour-ou-jai-decide-de-devenir-locavore-part-1/), [deux](https://circa1984.net/2017/01/18/le-jour-ou-jai-decide-de-devenir-locavore-part-2/), et [trois](https://circa1984.net/le-jour-ou-jai-decide-de-devenir-locavore-part-3-quelques-mois-plus-tard/)) sur comment j’essaie de devenir locavore. Et au delà du comment, [j’ai aussi abordé le pourquoi](https://circa1984.net/mais-pourquoi-locavore-alors/).

Des articles parus un peu avant l’été l’an dernier. A cette époque, dans le but d’échanger avec celles et ceux qui écriv(ai)ent aussi à ce propos, j’avais contacté [Mathilde Golla, journaliste économie au Figaro](http://plus.lefigaro.fr/page/mathilde-golla), sans savoir vraiment qui elle était, ni que nous avions des contacts en commun (marrant comme histoire d’ailleurs). Accusant réception de mon email, et m’assurant la lecture de mes articles, la discussion n’a néanmoins pas connu de suite… Pour le moment en tout cas.

Je continuais cela dit de suivre ses aventures via FB et [Twitter](https://twitter.com/Mathgolla) notamment. Et j’étais donc curieux et enthousiaste à la sortie de son bouquin, 100 jours sans supermarché, en ce début d’année 2018. D’autant que cette sortie fut bien appuyée médiatiquement, promenant Mathilde d’émissions en émissions sur à peu près tous les plateaux radios de Paris. Et pour une fois qu’un tel sujet est abondamment discuté sur de grosses antennes, ne comptez pas sur moi pour m’en plaindre.

* * *

### Un témoignage qui se lit tout seul

Gros avantage pour moi, le bouquin se lit tout seul, parce que le ton et le style employé vont à l’essentiel. Loin de moi l’idée de faire une critique littéraire ici, mais lire est quelque chose de compliqué pour moi (pour des questions de concentration…). J’étais donc très heureux de dévorer les pages de celui-ci.

La couv' annonce un guide, mais je parlerais plutôt d’un témoignage. Parce que M. Golla part de son intuition, de sa sensibilité, et de ses analyses personnelles pour entamer son aventure. Et file ensuite l’histoire de ces 100 jours sans supermarché. Une histoire racontée simplement, où elle transmets la parole des producteurs, distributeurs, bénévoles qui travaillent ensemble en circuit-court.

Et il fallait bien ça, un bouquin qui permette de coller aux réalités et aux urgences actuelles en racontant simplement des solutions rencontrées, et les gens qui les proposent ou les font évoluer, perdurer. Une sorte de rampe d’accès pour celles et ceux qui ont des a priori sur ces modes alternatifs de consommation, qui ne sauraient pas s’y prendre, ou qui sont déphasé-e-s (et il y a de quoi) par toute la novlangue et la com qui en émane parfois.

* * *

### Creuser son constat et son intuition

L’auteure part donc d’une intuition après un constat de terrain auprès d’agriculteurs rencontrés. La même que celle que je développe dans la rubrique locavore de ce blog. Cependant, locavore est un terme qui n’est jamais utilisé dans le bouquin (si j’ai bien tout lu…). Rien de grave hein, parce qu’on retrouve les notions de circuits courts, de consommation locale, directe, et qu’elles sont définies simplement, par d’avantage d’exemples que de philosophie complexe. Ça a le mérite de créer une cohérence d’engagement entre celles et ceux qui viennent à cela plutôt par la logique et les autres plutôt par la sensibilité (et toutes les options possibles entre les deux).

Parce que c’est bel et bien une intuition qui amène Mathilde Golla a entamer son jeûne de supermarché. L’avant-propos détaille le cheminement intellectuel en question : sa rencontre avec des producteurs qui ne s’en sortent plus en passant par les circuits conventionnels, quelques chiffres (livrés et expliqués avec clarté), quelques questions publiques posées via TW, et de la déduction :

> « Ces constats m’incitent à me tourner vers ces nouveaux modes de distribution, à trouver des alternatives aux supermarchés, salut d’une agriculture aux abois. »« Une personne estime même que la solution à la crise agricole serait d’ « encourager les petits producteurs et de boycotter les supermarchés ». Cette recommandation m’apparait alors comme une évidence. Et si je tentais l’expérience… »

* * *

### Trouver des solutions, choisir et partager

Sorti de ce constat initial, le livre entre ensuite dans le vif du sujet, et relate, semaine après semaine les solutions testées, approuvées. Et c’est là que cela devient intéressant. Parce que, hormis l’incidence que l’expérience provoque sur son budget (10% de moins) et son timing (un peu plus long), M. Golla partage surtout des tas et des tas de petites infos, de détails qui nous promènent avec elle, avec confiance. On en apprend sur les gens qui sont derrière ces solutions, et sur les effets que cela a sur sa santé, son appétit, sa gourmandise, et aussi sa gamberge. C’est réellement nécessaire pour voir se généraliser un pareil mode de consommation.

Une sorte de chaleur humaine qui remplace le marketing des supermarchés, une proximité qui s’installe, entre nous le lecteur, et ces gens que Mathilde rencontre au fil de son aventure. Tantôt des producteurs, tantôt des entrepreneurs, des start-ups, des bénévoles, des consommateurs, partout en France, de Paris à la Normandie.

Je suis du reste persuadé de longue date que c’est par ce moyen là que ces solutions s’installeront durablement dans le quotidien de nos sociétés. Et qu’il est nécessaire pour chacun d’entre nous qui en possédons quelques unes, de les partager d’une façon ou d’une autre. J’ai fait ce blog pour cela, M. Golla fait un livre, d’autres ont des comptes IG ou Mastodon. Tous les moyens sont bons pour partager et fédérer à ce sujet.

* * *

### Un premier étendard grand public

Ce bouquin est comme un premier étendard auprès du grand public. Si nous sommes un certain nombre à connaitre des adresses sur internet ou dans la vie, le grand public est quand même d’avantage connecté à ses supermarchés qu’à ses alternatives. Et il m’est toujours difficile de le blâmer à ce titre (ce que Mathilde Golla ne fait jamais non-plus dans son livre). Il fallait que ces alternatives prennent d’avantage la lumière, et que le cliché de leur complexité d’accès soit enfin absorbé ou démonté de la meilleure des façons : en donnant des solutions pour tout cas de figure.

Avec cet ouvrage, n’importe qui peut comprendre combien ces alternatives sont vertueuses. Et dans quelles mesures : du portefeuille du producteur au sien, de l’écologie au lien social, de la gourmandise à la santé. Rien dans le système conventionnel de la grande distribution n’arrive à la cheville des effets positifs des circuits courts et directs.

Que nous reste-t’il à faire maintenant pour nous, bloggers, journalistes, qui engageons notre réflexion en ce sens ? Continuer de recenser les solutions, et de les partager par tous les outils possibles et imaginables. Pourvu qu’ils soient agiles, modernes et qu’ils évitent les travers d’un mauvais marketing ou d’une novlangue à la con. Et puis continuer de réfléchir et discuter de l’organisation de ces alternatives. Et puis définir une sémantique, des mots et des termes clairs. Des définitions qui le sont tout autant. J’ai super hâte de participer à ce travail, et de la partager ici ! D’ici là, procurez vous ce bouquin, j’ai pour ma part opté pour la fantastique [Librairie Gourmande à Paris](http://www.librairiegourmande.fr/recherche?controller=search&orderby=position&orderway=desc&search_query=mathilde+golla&submit_search=Go).
