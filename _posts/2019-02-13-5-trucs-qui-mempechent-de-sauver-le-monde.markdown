---
layout: post
title: 5 trucs qui empêchent de sauver le monde
date: '2019-02-13 15:10:53'
desc: Ils sont là, dans mon quotidien. du matin au soir. Et contrairement à ma conscience militante ou politique, ils ne s'amusent pas, ils ne se reposent pas, ils ne profitent pas, ils ne dorment pas.
---

Voilà, un top... J'aurais décidément tout fait sur ce blog ahah ! Cela dit, X mois après n'avoir rien publié, on pourra pas accuser ce top, et ce blog, de vouloir faire du clic ! En plus, pour le coup, je suis très honnête avec ce top 5. Les trucs dont je vais parler sont vraiment ceux qui sont sur le chemin de mes raisonnements révolutionnaires. Ceux qui me rendent moins vigilent, moins actif, moins opiniâtre, que ma ligne politique ne l'exige.

Ils sont là, dans mon quotidien. du matin au soir. Et contrairement à ma conscience militante ou politique, ils ne s'amusent pas, ils ne se reposent pas, ils ne profitent pas, ils ne dorment pas.

### la procrastination / la flemme

Clairement, c'est le number one, et je suis loin d'être le seul concerné. La procrastination est un des maux du siècle des écrans. Ce terrible élan vers rien d'autre que des activités chronophages. Un aimant, un magnétisme terrible qui cloue l'action au pilori de la flemme. La flemme.

La flemme c'est la meilleure amie de ton pire ennemi politique. La flemme, c'est elle qui te pousse à descendre chez Franprix acheter des mauvais lardons et de la mauvaise crème quand tu as oublié-repoussé de faire des vraies courses de vrais produits pendant toute la semaine.

J'ai presque honte en écrivant. C’était un peu de le projet de l'article d'ailleurs. Personne n'est parfait, et je suis le P majuscule. L'engagement au quotidien est aussi fait d'échecs. Avec le temps et l'expérience, je me dis que le mieux que je puisse faire, c'est d'en limiter le nombre...

### la passion

LA passion, pour un cerveau comme le mien, c'est toujours synonyme de risque. Alors j'ai appris petit à petit à prévoir, construire quelques parachutes et filets de secours pour limiter ces risques.

Lorsqu'un truc me passionne, c'est assez soudain, grandiose, nerveux, intense, mais temporaire. Je ne renie pas cette passion ensuite, c'est juste qu'autre chose est passé par là. Chez moi, les passions se succèdent, ne se ressemblent pas, s'assemblent souvent. Mais le rythme intense des cycles freine bien souvent la construction d'un engagement stable, durable et effectif.

Il n'y a pas assez d'heures par jour et de jours par semaine. Et j'ai un métier prenant, sans doute autant que vous. Alors tant que quelque chose m'anime, je le pousse à fond. Quelque soit le sujet. Ensuite ? eh bien j'en détermine des axes d'action à produire... Et je ne fournis sans doute que très rarement tous les efforts dans le temps imparti pour mener à bien ce plan. Parce qu'entre temps, j'ai entamé autre chose, construit un autre plan, défriché un autre domaine. Et ainsi de suite...

### les impératifs

On y est toutes et tous là. S'il y a bien quelque chose de commun à nous toutes et tous, ce sont ces impératifs, inhérents à chacun de nos ages, de nos générations, chacune de nos situations de couple ou de famille, chacune de nos professions, de nos activités.

On est là à bosser, parfois pour nous, souvent pour quelqu'un d'autre. Ça nous paye un loyer, ou un prêt. Ça remplit le frigo, et les armoires... Ces impératifs inhérents à la construction de société dans laquelle nous évoluons. Ils ont une empreinte absolument énorme sur nos vies, tentaculaires tant ils touchent de points névralgiques de notre bien-être : stabilité amoureuse, financière, géographique, familiale... L'horlogerie est plus ou moins bien huilée mais elle doit fonctionner au mieux pour que nous nous sentions bien.

Sinon, difficile de construire une direction politique, de militer par l'action. Indéniablement, si je peux écrire et faire ce que je fais aujourd'hui, c'est parce que j'ai un toit, un bon salaire, une femme extra, et un groupe affectif (famille + ami-e-s) stable et serein. Mais c'est aussi cela qui me retient de prendre des décisions fortes, radicales même, qui sont pourtant celles qui suivent directement ma pensée, mes analyses... Et cela introduit parfaitement le prochain point...

### l'emprise du confort

Alors là, même si je le critique et en parle en continu sur ce blog, je plaide coupable, sans sourciller, malgré moi, et un peu triste, pour délit d'esprit bourgeois. Parce que même si je lui fais la guerre au quotidien, dans ma tête et dans un maximum de mes attitudes et habitudes, il reste là. Un peu comme un mauvais rhume mal soigné, dont on a pas tué les microbes les plus tenaces, et qu'on renifle encore beaucoup trop en Mai.

Le confort est une drogue dure. vous avez déjà lu cette phrase sur ce blog. On est toutes et tous drogué-e-s. Si je devais suivre mes élans militants, je vivrais de mon mieux, en autonomie maximale, dans une maison étudiée pour, produisant un maximum de choses par moi même, si ce n'est de la pollution.

Mais non. Je vis dans le 92, je bosse à Paris, je fais un métier qui repose sur la chaine du froid, et je deviens propriétaire... waw... just waw.

### le monde lui-même

Je n'ai pas commencé par ce point, parce que c'est une trop bonne excuse, lorsqu'on ne fait pas d'abord sa propre introspection, sa propre critique. C'est une excuse qui devient louche et abusive lorsqu'elle est la première à sortir. Elle cache toutes les autres, dans le discours de celles et ceux qui n'assument pas encore suffisamment le reste de ce top (et qui comme moi, y arriveront sans doute un jour !).

Mais peut-on oublier à quel point TOUT se construit autour de nous de façon à limiter la puissance de nos actions ? Comment se dire, aussi, que solo, on va arriver à renverser l'insubmersible système à l'action aujourd'hui ? Comment réussir à imaginer des solutions personnelles qui vont, non pas sauver l'actuel - je ne suis pas un effondriste / collapso pur et dur... - mais permettre une reconstruction massivement réussie ?

Ça me semble tout de même assez compliqué. Cette noyade est un risque que ne prennent que les plus courageux-ses d'entre nous. Chapeau ! Il faut du cran pour avancer sans sourciller, sans être soutenu-e, parfois dans l'oubli le plus complet. C'est aussi ce constat là qui me pousse à dire si souvent que le plus important pour celles et ceux qui tentent des choses (quelque soit l'échelle), c'est de documenter et diffuser.

Voilà, j'en suis là. Mon seul objectif c'est comme tout le monde : me démerder avec tout ca et faire en sorte que le type que je vais devenir puisse répondre la tête haute à celui que j'aurais dû être. On verra bien !
