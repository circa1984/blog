---
layout: post
title: 'La Diversité comme clef de voûte, part 2 : la sapio-diversité'
date: '2017-03-06 18:09:00'
desc: Bim bam boom, on attaque direct la part 2 en définissant la "sapio-diversité". Zébardi !
---

J’ai commencé à en parler [dans cet article](http://circa1984.net/2017/02/08/la-diversite-comme-clef-de-voute-part-1/), la diversité est la clef de voûte de ma réflexion… Et alors là, avec ce deuxième article, ça devient pointu… Parce que j’attaque en proposant (je n’ai pas trouvé d’autres références…) un terme dès le titre. (On a qu’une vie hein !)

Si la biodiversité est la variable _sine qua non_ de la vie sur cette planète, et qu’en cela elle symbolise toute la réflexion qu’on pourrait avoir sur la diversité au sens large, il existe aussi d’autres types de diversités, dont l’appréhension et la compréhension sont, à mon humble avis, des clefs dès lors qu’on essaie de construire un demain plus épanouissant.

Ce que j’appelle sapio-diversité en fait partie. Elle me paraît du reste abordée partout et tout le temps en ce moment, jamais nommément, par les étiquettes des profils psychologiques plus ou moins nouvellement identifiés (dont parle pas mal [cet article de M. Swar](https://medium.com/france/je-ne-suis-ni-surdou%C3%A9e-ni-hyper-sensible-ni-multi-potentielle-c86f4010cde7#.a0mjrbkr1) via Medium), et à un concept plus complexe qu’est la neuro-diversité (cf [Wikipédia](https://fr.wikipedia.org/wiki/Neurodiversit%C3%A9)).

* * *

### La sapio-diversité : qu’est-ce que c’est ?

Étymologiquement, le _sapio_ latin renvoie généralement au concept de sagesse ou de savoir. Par extension dans la formation de ce terme _sapio-diversité_, je le définis comme la sagesse et le savoir appris, compris, et mis en œuvre. C’est à dire la façon dont le cerveau de chacun a ingéré, digéré, transformé et/ou utilisé l’ensemble des « informations » qui lui ont été transmises, par exemple de la naissance à un jour J.

Le concept de sapio-diversité fait donc écho au fonctionnement du cerveau de chacun et de tous, dans toute la complexité que ça comprend : nature de l’apprentissage, contextes, vecteurs, temporalités, sources, codes, pour ce qui est de l’étape d’ingestion ; liberté, connexions, synthèses, et autres pour l’étape digestion ; réceptacles, vecteurs, épanouissement, transformations éventuelles, adaptations, transmissions, pour l’étape d’utilisation.

Et en effet, ne serait-ce qu’à l’échelle d’une salle de classe d’école, pour un cours d’une semaine avec contrôle au bout du compte, la variabilité des résultats en fonction des élèves rend compte d’un arbre de sapio-diversité immense. Imaginons ce que cela donne sur des échelles plus grandes encore !

Il ne s’agit donc pas de la construction biologique du cerveau (qui n’est ici qu’un facteur parmi d’autres), mais de celle, moins « anatomique », de l’esprit savant. Celle qui est recherchée par les employeurs, par exemple, par le biais de nos CV (qui témoignent de l’académisme de notre apprentissage et de notre expérience de terrain, entendons par là les phases d’ingestion et d’utilisation), mais aussi celle qu’on peut-être content de trouver chez quelqu’un dans sa vie sociale (« il/elle pense comme moi »…).

* * *

### De quoi dépend la sapio-diversité ?

Elle se construit avec la multitude de profils uniques que nous représentons. Et ces profils naissent et grandissent avec plusieurs facteurs :

- la construction biologique du cerveau, notre patrimoine génétique, ainsi que tout ce qui va venir jouer sur l’anatomie de notre » hardware » et sur son fonctionnement. (Là, on est dans la neuro-diversité)
- l’instruction, à savoir la somme des connaissances que nous allons stocker d’une facon ou d’une autre, quelque soit la mémoire invoquée.
- l’éducation, les fondements moraux, sociaux, familiaux, qui vont conduire nos comportements, participer à notre caractère, façonner nos élans, nos goûts, …
- des facteurs sociologiques et géographiques, qui ont à voir par exemple avec les interactions sociales qui nous façonnent, les technologies a notre portée et dont nous nous emparons ou non, le milieu dans lequel nous évoluons.
- le temps, variable immuable, qui fait mûrir, décanter, selon qu’il s’étire pour les idées de longues haleines ou se compresse avec les fulgurances.

Ces facteurs s’amalgament, avec une influence certaine les uns sur les autres, avec au bout du compte nos capacités, nos compétences, notre savoir, notre savoir-faire, et notre caractère. Les variables sont très grandes, très fines, ce qui m’oblige à sortir un joli poncif : nous sommes parfois semblables et différents en même temps, toujours uniques, mais nous ne devrions jamais nous sentir isolés (ça c’est la société dans laquelle nous vivons et qui détestent la diversité, qui s’occupe faire fleurir l’isolement).

### Uniques et semblables, étiquettes ou non ?

Comme je le mentionnais plus haut, l’article de M. Swar critique avec justesse toutes les étiquettes qu’on voit passer en ce moment dans nos feeds… Le couche-tard est un génie, le touche-à-tout est un multi-potentialiste, celui là même serait une sorte de chéneau manquant avec l’autisme, etc etc. Il y a du bon à toutes ces étiquettes, elles montrent un intérêt croissant pour la sapio-diversité, en tentant d’y mettre du clair et de s’y retrouver. Mais il y a aussi du moins bon : les raccourcis sont nombreux, les images d’Épinal aussi, le clickbait n’est jamais loin, …

Pour ma part, je ne les questionne que comme des objets linguistiques ne pouvant résumer, même avec les meilleures intentions du monde, toute la richesse dont il est question. Je ne les utilise non pas pour expliquer mais pour me faire comprendre, pour jeter un schéma sur lequel il est en suite possible d’approfondir ce dont on parle, comme j’essaie de le faire avec cet article.

Je m’en suis également servi pour assimiler le fait qu’il n’y finalement rien d’anormal à être ce qu’on est ([j’en parle dans cet article](http://circa1984.net/obsessionnel/)). Que c’est une question de « fit » (pour employer de l’anglais, une fois n’est pas coutume) avec le milieu dans lequel chacun évolue. Et _in fine_, d’avoir la chance de se trouver au bon moment au bon endroit pour développer notre épanouissement.

Je suis sans doute un multi-potentialiste, mais comme je le dis en description, je commence à peine à l’assumer, à saisir ce que mon « sapio » m’offre de contraintes et d’atouts, de possibilités, dans un mouvement continu. C’est d’ailleurs devenu super exaltant !

_Si vous avez connaissance de ce concept de sapio-diversité, même nommé autrement, n’hésitez pas ! Je ne suis pas le premier à en parler si longuement j’imagine, mais les rares références que j’ai trouvées sur internet ne se sont pas montrées très bavardes…_
