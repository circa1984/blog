---
layout: post
title: 'Revue de net : épisode 1'
date: '2017-02-05 16:59:00'
desc: Quelques liens de ce que j’ai vu et lu sur les autoroutes de l’information ces derniers temps… Pas forcément nouveaux, mais que je trouve toujours super pertinents.
---

Quelques liens de ce que j’ai vu et lu sur les autoroutes de l’information ces derniers temps… Pas forcément nouveaux, mais que je trouve toujours super pertinents.

* * *

### [VIDEO] He Spent 40 Years Alone in the Woods…

C’est l’histoire d’un type qui vit depuis 40ans dans les bois en plein Colorado, dans un des endroits les plus froids des US. Mais au delà de cela, c’est le récit court (5min de vidéo) d’un monsieur en lien avec les scientifiques pour sa récolte incessante de données sur les tombées de neige depuis 40ans.

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/L72G8TLtTCk?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>
* * *

### [VIDEO] Ken Robinson nous dit en quoi l’école tue la créativité…

Dans un TEDx, Sir Ken Robinson présentera en un quart d’heure comment nos systèmes éducatifs contraignent la créativité (et a posteriori l’épanouissement) des enfants. Et il le fait avec un humour british impeccable !

<figure class="kg-card kg-embed-card"><iframe width="459" height="344" src="https://www.youtube.com/embed/iG9CE55wbtY?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>
* * *

### [ARTICLE] Cette offre d’emploi qui n’existe pas (encore)

[Anne-Laure Fréant](https://medium.com/@annelaure.freant?source=post_header_lockup) propose une offre d’emploi, une vraie, mais dans une annonce hors du commun et pour un poste hors catégories. A mon sens, elle décrit l’avenir du monde du travail (qui ferait bien d’être le présent d’ailleurs…). Morceaux choisis :

<!--kg-card-begin: html--><quote>"

    Potentiels multiples et inexploités
    Pensée arborescente, rapide et inventive,
    Formée par la vie à l’intelligence culturelle, ou intelligence de l’Autre.
    (si tu sais ce que c’est, c’est que c’est bon). […]

    Capacité d’apprendre n’importe quoi et son contraire en trois jours.
    Capacité de l’enseigner correctement le 4ème jour.
    Enormes capacités d’ingestion, digestion et tri de l’information.
    Sachant sachant jeter (et n’ayant pas peur de le faire). […]

    Conscience de sa position minoritaire dans la société qui nous entoure.
    Ni victime ni bourreau.
    Optimisme à toute épreuve.
    Capacités de création sans limite.
    PhD en Mac Gyverisme exigé
    (pour créer des chef d’œuvres avec un budget stylos, ou l’inverse). […]
    "</quote><!--kg-card-end: html-->

[Lecture intégrale de l'article ici.](https://medium.com/@annelaure.freant/cette-offre-demploi-qui-n-existe-pas-encore-67f14cee4f6c)

* * *

Et bon dimanche !
