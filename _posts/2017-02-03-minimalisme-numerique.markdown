---
layout: post
title: Minimalisme numérique
date: '2017-02-03 18:31:00'
desc: Depuis ce début d’année j’essaie de remodeler mon utilisation de la technologie. Petit tour d'horizon du pourquoi, du comment, et des solutions.
---

Depuis ce début d’année, et au delà de Google comme raconté [sur ce blog](http://circa1984.net/tag/alternatives/), j’essaie de remodeler mon utilisation de la technologie. J’étais gros consommateur de Facebook, Twitter, sur ordinateur ou sur téléphone, au milieu d’un océan d’autres sources de contenu et de logiciels de créations (musique, graphisme, vidéo)…

Seulement, j’ai récemment percuté que si j’étais très à l’aise avec tous ces trucs, je ne comprenais finalement pas vraiment la technologie dont je me servais (matérielle et logicielle), voire pas du tout… Une situation absolument anormale et qui m’apparaît avec le recul complètement hallucinante.

Et là, en me voyant moins produire de musique (voire pus du tout) et en voulant moins me disperser dans les autres domaines (vidéo, etc), c’était le bon moment pour une petite _tabula rasa_ en bonne et due forme.

* * *

### Minimalisme matériel, bien connu

En matière de minimalisme, je tire mes inspirations de bien des domaines différents… D’un certain mode de vie japonais à l’essentiel, en passant par des maximes et des proverbes incitant à ne pas trop accumuler, le minimalisme fait partie de mes influences depuis longtemps… J’ai donc (presque) toujours essayé de réduire mes acquisitions matérielles au minimum. Voyager léger, déménager rapide, bouger vite, et ne pas trop m’attacher à tout cela…

De même, en musique, les formules épurées, à l’essentiel, ont toujours été celles que j’ai poursuivies quand je produisais, et celles que j’admirais quand j’écoutais. La limitation des moyens, la recherche du strict minimum, de l’essence par la soustraction… Quand bien même je me sois de nombreuse fois tapé la tête sur les murs (les choses les plus simples sont les plus difficiles) à trop vouloir rajouter de trucs et de bidules !

Aujourd’hui un peu plus âgé, le minimalisme de l’essentiel matériel à tendance à passer la main, comme intégré, à une recherche de l’essentiel « de vie ». On frise avec la philosophie de comptoir et j’ai un peu peur d’être trop abstrait mais tant pis… Une quête tranquille d’un peu plus de « centres réels » d’équilibre, maîtrisés et conduits, peu conflictuels, vastes, calmes, sans pour autant être ni confortables, ni lobotomisants. Des endroits, des gens, des choses, qui sont au cœur de l’épanouissement. Et que je comprends, voire construis.

À ce titre, repositionner radicalement ma vie technologique s’avère quelque chose que j’aurais dû apprendre avant ! Même si je ne pense pas que j’aurais pu en comprendre toute l’importance sans avoir procédé autrement avant…

* * *

### Numérique minimal et essentiel

Je voulais donc recommencer et repenser mon usage de l’ordinateur de À à Z. J’avais du mal à passer des soirées agréables, je perdais le fil en naviguant d’un logiciel à l’autre, d’un onglet à l’autre, sans réel but, sans contenu de valeur… Le monstre des habitudes, celles qui ne fonctionnent plus quand on change un peu beaucoup de vie. J’ai donc commencé à soustraire en me posant quelques questions de base :  
– pourquoi j’ouvre mon logiciel de musique alors que je n’ai plus d’inspiration, plus de demande ou de projet réel, et plus d’envie ?  
– pourquoi je stocke autant de médias divers et variés alors que je n’en consulte / écoute / regarde que… 5% maximum, qui plus est déjà disponibles online ?  
– pourquoi est-ce que je conserve autant de logiciels qui ne me servent à rien ?

En quelques jours, j’ai supprimé 350go de musique de mon disque dur (YOLO), 150go de films, tous mes logiciels de musique, de photo, de vidéo, et de design, et voyant qu’il ne me restait plus rien ou presque, je me suis dit que finalement, mon envie était de me servir de mon ordinateur pour :  
– écrire mes articles sur ce blog (online donc)  
– regarder des docus, des films, et quelques rares séries  
– écouter de la musique mais sans avoir à chercher quoi  
– un peu de bureautique basique

Des conclusions qui n’avaient absolument RIEN à voir avec mon utilisation et mes habitudes passées, et qui finalement n’engagent à rien de définitif. Alors j’ai poussé le process un peu plus loin…

* * *

### Bye Windows !

A plus tard ! J’ai formaté ma machine et installé [Ubuntu](http://www.ubuntu-fr.org/), de Linux donc. Changer d’OS n’est pas chose facile quand on a passé toute sa vie chez Windows, mais si j’avais ne serait-ce qu’envisagé de loin la qualité des distributions actuelles chez Linux… WOAH !  
En tout cas pour ma nouvelle utilisation de l’ordinateur, c’est absolument parfait : léger, versatile, plein de possibilités, sans encombrement, et avec une énorme communauté qui a toujours trouvé et rédigé ce que vous cherchez exactement à faire ! Et sans oublier : open source et (vraiment) gratuit.

Un bureau épuré au maximum, les logiciels qui suffisent à mon utilisation quotidienne (Firefox, VLC, un client torrent, pour le principal..), avec quelques extensions aussi pratiques que canons (mention spéciale pour ce lecteur radio en haut à droite qui ouvre un petit menu de sélection de mes stations favorites – coucou [Le Mellotron](http://www.lemellotron.com/) et [Radio Campus Orléans](http://www.orleans.radiocampus.org/) ! – tout bête, parfait).

D’autre part, Linux incite à mettre la main dans le cambouis et à comprendre comment il fonctionne (on peut installer des logiciels via un terminal, en tapant du code / des commandes…). On apprends donc mieux à connaître ce qu’on a sous les yeux, et on construit son environnement de manière moins automatique (et robotique) que sous les autres OS (Windows et Mac).

Parallèlement, pas vraiment de sensation de manque… En gros amateur de typos et design, Illustrator me manque un peu (et je vais d’ailleurs devoir réinstaller un petit Windows quelque part pour finir un petit truc), mais on verra… Je ne crois pas que tout puisse se changer du jour au lendemain, et je trouve relativement sain qu’une bricole me manque, alors que tout le reste rafraîchit vraiment mon utilisation de l’ordinateur.

* * *

### Et au delà de l’ordinateur ?

J’ai arrêté de lire le contenu Facebook, en acceptant clairement ne pas être la bonne cible sorti de quelques contacts au feed intéressant. Facebook est une loupe grossissant (avec déformations) ce qui se passe partout dans le monde, avec un effet anxiogène incroyable… J’ai plutôt besoin de réfléchir calmement, et sur des sujets que je ne croise jamais là bas. La même chose est valable avec Twitter du reste…

Alors je prends plaisir à découvrir [Medium](https://medium.com/), une plateforme de rédaction d’article ouverte à tous, où on peut trouver (surtout en anglais) du contenu très haut de gamme, tant sur les sujets abordés que sur les angles et les perspectives prises par les membres de la plateforme. Pour ma part, j’y trouve complètement mon compte et je ne lis plus que ça le matin et le soir sur le chemin. De quoi largement brasser des idées, être impressionné par des actions menées à l’autre bout du monde ou à coté de chez soi…  
Et puis [Ello](http://www.ello.co), qui nourrit bien la partie de moi qui a toujours faim d’artwork, d’art, d’idées dingues, de créatif. Mieux foutu (parce que plus diversifiée sans son architecture de feed) que Tumblr ou Pinterest, je trouve cependant à Ello une froideur quasi-glaçante, qui m’empêche presque de commenter et de « vivre » sur cette plateforme. Uniquement consultatif donc…

###### (Je n’ai pas de fin pour cet article, merci !)
