---
layout: post
title: Bye Wordpress, Bonjour Ghost !
date: '2018-11-03 16:21:51'
desc: Avant j'étais chez Wordpress. Mais ça c'était avant. Il me fallait juste le temps. De voir où j'allais atterrir, comment, de A à Z. Il me fallait un plan.
---

Voilà, je viens tout pile de finir la migration de mon blog de Wordpress.com à [Ghost](https://ghost.org).

Il y a quelques détails qui me gênaient dès le début chez Wordpress. Chemin faisant, ces détails sont devenus incompatibles avec mes aspirations. Il me fallait juste le temps. De voir où j'allais atterrir, comment, de A à Z. Il me fallait un plan.

* * *

### Migrer son blog pour raisons politiques et pratiques

Avant de retenir Ghost comme option définitive, je suis passé par [une grande étape de réflexion](https://mamot.fr/@gregoiremarty/100967042345540550). D'abord je suis tombé sur [Kirby](https://getkirby.com/blog), et le _flat file_ de manière générale. J'ai regardé du coté de [Hugo](https://gohugo.io/) aussi... Mais il manquait un truc. C'est toujours la même histoire... Il manque toujours ce tout petit truc qui fait que... Un choix de service sur internet, quand on manque du savoir-faire nécessaire à tout monter soi-même, c'est toujours un compromis.

Ceci étant dit, Ghost s'est avéré comme le compromis le plus proche de mes engagements et de mes aspirations. D'abord parce que [Ghost est une organisation à but non-lucratif](https://ghost.org/fr/about/). Elle rend publics ses résultats de manière très lisible et compréhensible, et explique de la même façon sa politique, sa ligne éditoriale, et les directions prises. Ghost est également une plateforme riche en capacité, et comme je l'ai dit : copen source. [Tout est écrit ici !](https://ghost.org/fr/features/)

* * *

### Serveur / Domaine / Blog

Sauf que. Je ne voulais pas non plus souscrire à un modèle où l'administration de mon blog est faite par la plateforme qui le développe. Je voulais profiter de cette migration pour apprendre à installer un blog sur un serveur, maitriser les DNS d'un domaine et les redirections, et voir comment tout ceci pouvait cohabiter au plus près de mes aspirations.

J'ai donc commencé par apprendre à maitriser Ghost. J'ai donc dû l'installer en local, et je me suis aidé de leur doc, très bien faite : [How to Install Ghost locally...](https://docs.ghost.org/install/local/) Installer la plateforme localement sur son ordi, c'est pouvoir la prendre en main sans qu'elle soit publique. Travailler également l'interface, le thème, l'implémentation d'une chose ou une autre, sans dépendre d'autre chose que de son ordi à la maison. J'en ai donc profité pour développer mon propre thème (sur la base de leur thème initial nommé Casper).

Une fois e blog pris en main et apprivoisé, il me fallait trouver un serveur. J'en ai une chez OVH, pour un projet différent. La techno de partage de serveur, l'upload de Ghost dessus, et sa mise en route... tout ça était bien trop pénible pour moi. Et j'ai opté pour [un droplet chez Digital Ocean](https://www.digitalocean.com/products/droplets/). Un droplet, c'est une sorte de Machine Virtuelle qui fait office de serveur pour qui veut. Les avantages chez Digital Ocean sont le suivi de la consommation et la mensualisation du paiement (ras le bol des engagements annuels...), et les droplets avec des "applications" préinstallées dessus : Wordpress, Gitlab, Discourse, et donc pour ma part Ghost !

Très facile à mettre en route (tout est dans les mails qu'on reçoit, et la doc est méga bien foutue), mon blog était monté en 2 sec. Il m'aura juste fallu quelques temps pour bien comprendre comment certifier mon https mais là encore, tout est bien expliqué (je tournais autour du pot ahah !). Un certificat Let's Encrypt auto-généré dans l'interface de gestion du droplet, la config très bien accompagnée d'un LoadBalancer dans un onglet voisin, et en avant Guinguamp ! _(je vais peut-être quand même faire un tuto de toutes les étapes, à terme...)_

Il m'a juste fallut ensuite configurer les DNS chez OVH et tout roulait ! Bon, je fais le malin là maintenant que c'est fini, mais je me suis quand même bien pris la tronche avant que tout soit opérationnel... Le diable est dans les détails !

* * *

### Et maintenant ?

Eh bien maintenant on va retourner écrire des articles. J'ai été occupé à me marier cet été, et suis devenu responsable de boutique depuis la rentrée, donc autant vous dire que le temps n'a pas été alloué à l'écriture pour ce blog ces derniers mois.

Cela dit, beaucoup de choses sont en cours. Il va d'ailleurs falloir que je fasse un gros tri dans ces projets. Un article sur les projets délaissés serait d'ailleurs marrant tiens !

D'ici là, je vais essayer de parler d'avantage d’anthropocène et de résilience. Deux thèmes qu'il m'est nécessaire d'approfondir si je veux continuer de creuser mon petit chemin...

A très vite !
