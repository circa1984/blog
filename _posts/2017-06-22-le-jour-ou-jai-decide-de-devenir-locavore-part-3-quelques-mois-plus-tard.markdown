---
layout: post
title: Le jour où j’ai décidé de devenir locavore part 3, quelques mois plus tard…
date: '2017-06-22 20:10:00'
desc: Si je continue à dire "démarche", "essai", tant on part de loin quand on vit dans une très grande ville avec les habitudes qui sont les miennes, où en suis-je aujourd'hui ?
---

Quelques mois plus tard, où en suis-je ? En accord avec ma réflexion sur la société, sur notre avenir d’humains et sur mes valeurs, j’ai entamé il y a quelques temps la démarche du locavorisme. Et même maintenant, je continue à dire _démarche_, _essai_, tant on part de loin quand on vit dans une très grande ville avec les habitudes qui sont les miennes.​..

* * *

### Les bons points

Mes adresses sont les bonnes : ça, clairement, c’est le bon point ! Les adresses que j’ai trouvé et chez qui je me fournis, particulièrement en légumes et fruits, sont TOP. Fraîcheur, saisonnalité, idées, discussions, tout est au rendez-vous. [Au Bout du Champ](https://www.facebook.com/auboutduchamp1/) reste mon enseigne favorite… A tel point que j’aimerais même développer des trucs interprofessionnels avec eux. (Encore un projet, comme si j’avais le temps… Ahah)

Mes aliments sont bons : oui parce que bon… Manger local et de saison si c’est pour du fadasse : ciao bambino hein. Hors de question de rogner sur la qualité gustative (organoleptique pour les plus savants d’entre vous) des aliments ! De ce coté, une fois encore c’est bingo. Rien de surprenant d’ailleurs.

Mon cerveau bouillonne autour de tout cela : eh oui… Comme souvent… Pour bien des choses… Là, avec le locavorisme, je développe des idées, plus ou moins ambitieuses. De la conjugaison de cet engagement avec le futur de ma carrière pro (comprendre : trouver un job dans le domaine à plus ou moins long terme) jusqu’à des projets collaboratifs à grande échelle, tout y passe (mon sommeil y passe aussi… C’est une autre histoire). J’espère, vraiment très fort d’ailleurs, en reparler d’ici la rentrée, avec des accomplissements, même minimes…

* * *

### Les points difficiles

A vrai dire, il n’en manque pas. D’abord parce que je suis un satané perfectionniste et intégriste de la « totalité absolue », c’est à dire que je m’empare à 2000% d’un projet ou pas du tout (cf cet article). Ensuite parce que j’avais du stock à écouler avant par exemple de pouvoir avancer sur certains sujets du quotidien : les pâtes, le riz, les céréales ? Le café / thé ? Les savons et produits ménagers ? Comment rendre tout cela plus local, plus direct, plus réfléchi ? Pas facile du tout quand il reste 5kg de pâtes et de riz dans le placard, du café, du sucre, des thés, et autres.

Et puis n’oublions pas les terribles habitudes… Si je ne suis pas un hippie en sarouel qui joue au diabolo en faisant la manche mais en fumant du bon tabac Philip Morris dans des tongs fabriquées par des jeunes philippins, je suis tout de même un un bon bobo trentenaire aimant un peu trop les burgers et le thé glacé (ce qui en soit, par rapport au hippie en sarouel, n’est mieux que sur l’hygiène corporelle…). Terribles habitudes. Qu’il faut vaincre pour avancer !

* * *

### Le futur locavore

Parce que je ne vais pas non plus faire 300 articles sur le sujet à proprement dit, je préfère poser ici les axes de mon futur de locavore, un peu comme un manifeste personnel.

Il est évident qu’il va falloir que je remette en cause des habitudes tenaces. Souvent liées à des mondanités ou en tout cas à ma vie sociale, ce qui les rend d’autant plus accrocheuses. Mais c’est peut-être en allant plus loin dans le projet ? Dans sa réflexion ?

J’ai d’ailleurs pour projet de développer un outil pour les locavores. Un outil qui permettrait la fédération d’autres personnes sur cette ligne. Toutes et tous désireux d’avancer et de faire évoluer les choses à notre échelle personnelle et fédérée, l’outil galvaniserait ces énergies et les mobiliserait de façon efficiente, productive, et plus virale que juste des articles.

Ce projet là, bien en place dans ma tête, et déjà techniquement plutôt bien avancé, doit d’abord trouver un refuge sur internet (en l’occurrence un site bien à lui) où se développera sa propre vie, et son extension que j’espère vive et sereine.

Il y a aussi un second projet, plus global et lié de plus près à l’ensemble de ce blog. Mais comment pourrais ne pas essayer de mieux théoriser ma démarche, d’en étoffer l’argumentaire présenté sommairement lors des deux premiers articles sur le sujet ? Il me semble ultra nécessaire de mobiliser tous les arguments possibles pour protéger mon engagement, le soutenir, ne serait-ce déjà que pour rompre avec mes fâcheuses habitudes plus facilement !

Un outil fédérateur, et une théorisation augmentée, et tout ça sur des supports autres que ce blog. Voilà l’avenir de ma démarche d’apprenti locavore. Et d’autres surprises aussi j’espère !
