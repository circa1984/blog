---
layout: post
title: Le concept de l'Allié.e
date: '2020-03-12 08:33:32'
desc: Il est grand temps de parler de ce gros malaise que j'ai avec le concept de l'allié.e. Oui oui. Trop omniprésent, trop vertueux, trop cachotier sur la réalité des choses.
---

Vous l'avez sans doute compris si vous avez vu certains des derniers articles du blog, j'ai profité de cette deuxième moitié finissante de 2019 pour parler d'un truc majeur : la lutte des races sociales.
Je l'abordais autant que possible par la déconstruction et en évitant la morale et la culpabilisation. Inutiles et complètement pas fertiles à terme.
Inutile à nouveau, de préciser que **nous parlons d'un fait sociologique** et non d'une réalité biologique qui, disons le une énième fois, n'existe pas. La *race sociale* est une conséquence du *[Racisme d'Etat](https://fr.wikipedia.org/wiki/Racisme_d%27%C3%89tat)*, dit racisme systémique, qui classe les gens en fonction de leur couleur de peau avec l'attribution d'une "race" sociale : ils et elles, concerné.e.s, se sont donc attribué le terme de **racisés** pour se définir dans ce contexte de *[racisation](https://fr.wikipedia.org/wiki/Racisation)*.

Ceci étant posé, et 2020 étant là, il est important pour moi d'aborder un truc qui me chiffonne depuis quelques temps maintenant : l'omniprésence du concept de l'allié.e parmi les blanc-he-s solidaires des combats menés par les racisé.e.s. Allez, je mets les pieds dans le plat !

## Auto-attribution du statut d'allié.e
La première chose qui me pose problème, c'est l'auto-attribution trop récurrente de cette étiquette. Alors oui, ce n'est pas automatique, et ce terme là n'appartient pas intégralement aux concerné.e.s. Mais lorsque se définissent comme tel.le un certain nombre de personnes blanc.he.s dont la motivation semble de fait être le soutien aux combats menés par les racisé.e.s, sans que personne ne leur ait jamais indiqué combien leur apport à la lutte des races sociales était louable, eh bien là est mon "contentieux".
Un phénomène sans aucun doute manifeste de toutes les bonnes intentions du monde. Mais l'enfer (de la lutte des races sociales) ne déroge pas à cette règle : il est aussi pavé de bonnes intentions.

Difficile de la blâmer, cette bonne intention. On part quand même d'une idée chouette à la base : essayer de faire en sorte que le monde soit plus juste, parce qu'on a compris quelques morceaux de fonctionnement de notre société, et qu'on cherche à les corriger. Nous sommes nombreux à cette étape là, à vouloir rogner et casser nos privilèges.
En effet, l'allié.e c'est aussi cette personne qui a déjà démarré la déconstruction. C'est vraissemblamement cette personne qui entame un travail, à commencer par un travail sur elle-même. Et quand on est capable de mesurer combien ce travail est difficile, on est aussi en position de comprendre pourquoi il faut aller plus loin encore.

Pire, on est capable de saisir à quel point ce statut d'allié est rigoureusement difficile, dans sa notion la plus pure et intacte ! De fait, j'ai une grande réticence au sujet de ce statut auto-attribué. Qui suis-je pour juger si oui ou non je suis un allié, un bon allié même. Je fais des efforts, ok, mais allié n'est pas un terme en l'air, dans cette cause comme dans d'autres.

Rapporté à la lutte des classes (par exemple), un patron "social", *allié*, reste et restera celui qui soumet ses employés à une corvée dont il s'abstrait, et dont le résultat n'est rien d'autre que son intérêt individuel. On peut réduire autant que possible les inégalités de poids dans cette balance, la balance elle-même est complètement absurde ! Quelque soit le nombre de sourires, de primes, de tickets restos ou de semaines de vacances offertes ! Peu importe la volonté même du dit patron !

## Chacun sa lutte, et la Lutte pour tous
Je ne crois pas vraiment au projet de lutte commune. Au sens propre du terme, une lutte commune aurait tendance à amoindrir la lutte de certains pour la noyer dans la lutte de beaucoup. Ce qui serait extrêmement dommageable pour ces luttes et tout le mérite qu'elles ont d'exister et les effets qu'elles ont sur notre société.

La lutte commune c'est donc un vieux rêve, sans doute un peu marxiste, que j'ai remplacé il y a quelques années par *les luttes intersectionnelles*. Cette agglomération de luttes qui partagent certains points communs, mais qui divergent sur d'autres. Pour moi, c'est donc chacun sa lutte.

Ainsi, chaque classe sociale, chaque race sociale, chaque groupe de genre, développe ses luttes. Mais ce qui est également certain, c'est que lutter est désormais un dénominateur commun. Et en cela, la posture auto-proclamée de l'allié me paraît trop passive.
Clairement, les luttes de races sociales sont des luttes contre les préjudices vécus et contre les privilèges des autres. À juste titre. Si la posture de l'allié est d'encourager ces luttes et de faire effet de masse en les relayant, en rester là voudrait dire que nous, les blancs, n'avons rien d'autre à faire.

## Du pain sur la planche pour nous aussi
Une lutte c'est un combat. Violent. Plus le déséquilibre subit est grand, plus grande est la violence. Nous, les blancs, nous vivons ces préjudices par sensibilité, par une sorte de procuration. Mais nous ne les vivons pas dans notre chair. Et de fait, comme le dit justement l'etymologie, nous ne pouvons pas les incarner.

Mais nous pouvons incarner une lutte. Nous devons même. La lutte contre ce qui cloche dans notre balance. Et ce n'est évidemment pas, dans le contexte des races sociales, la somme des préjudices que nous vivons. C'est celles de nos privilèges. Une drogue dure comme le confort. Que nous devons endiguer avec opiniâtreté.

Clairement cela commence par passer de l'état de vecteur du racisme systémique à celui de relais des réalités des combats. Mais au delà de cela, au delà du statut auto-proclamé d'allié, c'est aussi faire le job sur ses enfants, ses proches, avec tout ce que ça comporte d'intransigeance. Le *social justice warrior* ne suffit plus. L'allié qui fait masse non plus. Si nous voulons une société plus juste, nous devons commencer par la construire.
