---
layout: post
title: Non merci, je ne manifesterai pas non plus
date: '2017-06-19 19:47:00'
desc: La manifestation. Ce rassemblement urbain où une cohorte de personnes tentent de faire entendre leur désaccord avec les positions, les perspectives, les lois du gouvernement et de l’État. Non, merci, sans moi.
---

Après avoir confié [mon abstentionnisme](https://circa1984.net/non-merci-je-vais-mabstenir/), et en voyant les résultats des élections présidentielles puis législatives, il fallait obligatoirement que je parle de ce qui va suivre irrémédiablement : la manifestation (au pluriel en fait…). Ce rassemblement urbain où une cohorte de personnes tentent de faire entendre leur désaccord avec les positions, les perspectives, les lois du gouvernement et de l’État.

Avant de commencer, deux choses… Un petit historique personnel d’abord : si je me suis toujours abstenu, je n’ai en revanche pas toujours été un non-manifestant. J’étais sur les piquets de grève de l’Université du Mirail à Toulouse pendant mes études. Et aujourd’hui, je ne sais plus pourquoi j’y étais. Cela résume d’une certaine façon ce qui va suivre dans cet article…

D’autre part, je ne vais parler ici que des manifestations politiques ou syndicales, les défilés, pour être précis, liées directement à des projets de loi ou ce genre de trucs… Il ne s’agit pas du tout des manifestations d’indignation ou d’hommage, comme le sont par exemple celles qui suivent des bavures policières, des attentats, ou n’importe quel événement dramatique… On s’est compris. Je parle ici d’un des présumés terrains de la négociation peuple (travailleurs) / État (législatif et exécutif).

* * *

### Manifester, c’est se faire entendre ?

OK, descendons dans la rue. Vociférons notre mécontentement quant à cette loi ou cette reforme, toujours plus liberticide, toujours plus dure avec la protection des travailleurs. Sur le papier, super. Un élan solidaire, quoique parfois trop jacobins et parisiens. Basé sur des forces vives, souvent prêtes à presque tout : paralyser un pays, des artères de circulation dans une ville, des boulevards entiers…

Se faire entendre avec du slogan, et de la bannière : humour, jeux de mots, chants, qui font parler le collectif ou lui donnent une voix. Le rassemblent, l’amalgame, autour d’une éventuelle cause commune qui en devient presque noble, dépassant la simplicité et la normalité évidente de sa revendication initiale.

Alors pour cela, on arpente les rues, les avenues, à plein derrière une phrase bien trouvée. Lycéens, étudiants, travailleurs, brassés tous ensemble ou presque, quoique regroupés par affinités et groupes aux leaderships concertés mais disparates. Chacun à sa place dans un grand tout. Et on s’échange les sans étiquettes (dont j’ai fait partie un temps comme je le disais plus haut) entre groupes.

* * *

### Manifester oui, mais dans les clous

Parce que prendre la rue, d’accord. Se réunir pour se faire entendre, bien sûr. Mais là, pour les manifs dont je parle, il s’agit majoritairement de déambuler dans un circuit vu et corrigé par les instances dirigeantes, l’administration préfectorale, l’autorité de l’État.

Ni plus ni moins : contester l’oppression dans le cadre donné par celui qui nous opprime. Incroyable mais vrai.

La trajectoire des manifestations est choisie, après conception, retouchable au besoin. On évite autant que faire se peut deux choses : les lieux de pouvoir stratégiques, et les manifestations sauvages. C’est à dire grosso modo, les deux accès du vrai danger, les deux premières conditions d’un éventuel climax. Bastille, République, Nation, invariablement ou presque…

La manifestation est donc un outil dans les clous. Bien rangé. Je ne parle des manifestants, qui eux sont, je n’en doute pas, animés d’un esprit de combat, de lutte, de revanche, d’honneur. Mais les dés sont super pipés non ? Dans la mesure où le maillon qui oppresse est celui qui dirige la contestation, rien n’est potentiellement fertile, porteur de changements concrets, profonds, et j’allais même dire radicaux.

Pourtant, n’est-ce pas là le sens de la lutte ? La concrétisation du changement ? Ou bien, comme peut le dire Francis Cousin, le syndicat n’est-il que la balance de la légitimation de la constante réduction des droits à mesure du grandissement agressif du capitalisme (là on est dans du gros marxisme, yolo) ?

* * *

### Reprendre au mot

Manifester. Redéfinissons le proprement : porter le manifeste de ses idées. Dans son verbe et dans son action. Dans son cœur et ses gestes. En étant le seul maître de ses phrases, et le seul maître de ses mouvements. Établissant de fait une relation strictement horizontale et égale avec les autres, tous.

Il ne peut y avoir à quelconque moment, dans la manifestation, la validation de son projet, un consensus quant à son déroulé, avec celui contre qui nous manifestons. Il ne peut pas, ne doit pas, savoir, comprendre, encore moins orchestrer. Il doit subir. Puisqu’il s’agit de rendre par le nombre ce qu’il donne par la puissance. « Horizontaliser » le rapport de force.

La manifestation est dans l’idéal la justice concrète rendue par le peuple. Sa violence est proportionnelle à la nature de sa formation. Son impact proportionnel à l’universalité et la fraternité des combats en question. Son endurance proportionnelle à l’invisibilité de la souffrance aux yeux de tous. Plus l’opprimé aura souffert, en silence, longtemps, plus la manifestation sera profonde, lourde, galvanisante, et porteuse de changement.

Ces manifestations là, que je fantasme en écrivant, elles n’existent pas. Je ne sais pas si elles ont existé du reste. Je n’ai pas l’info, je ne les ai pas vécues. En tout cas, en 2017, alors que le pays baigne dans un élan jacobiniste néo-libéral, où une poignée décide pour un océan, elles ont tout pour se monter.

_Nuit debout_ est le parfait exemple à ne pas suivre(tout respect dû à toutes les réflexions qui ont germé chez les participants depuis ces rassemblements). Et on apprend toujours mieux d’erreurs que de succès.

* * *

### Quid de la grève ?

Simple : pourquoi aller dans la rue alors qu’on peut faire gréve chez soi, avec le même taux de paralysie (hors chauffeurs routiers) ? A quoi bon se faire soi même de la chair à saucisse pour policier chien de garde à la matraque zélée, quand on peut griller ses merguez à la maison ?

La gréve est avant-tout la mise en pratique de l’intention de suspendre la production de richesse pour peser dans la négociation. C’est l’arrêt de la production qui coûtent cher, d’avantage que l’occupation de les boulevards de République à Nation…

Et si on veut mettre la pression sur son patron ? Eh bien il y a d’autres moyens non ? J’en dis pas plus, mais vous mêmes vous savez non ?

« Le présent est à la lutte, l’avenir est nous. »
