---
layout: post
title: J’ai testé Riot.im, et je l’ai adopté
date: '2018-02-12 12:31:00'
desc: Whatsapp, Messenger, Trello, Slack, Telegram, et autres clients d’organisation et/ou de messagerie, j’avais fait le tour d’à peu près tous sans jamais trouver sneakers à mon pied. Et puis Riot (désormais Element.io)
---

Whatsapp, Messenger, Trello, Slack, Telegram, et autres clients d’organisation et/ou de messagerie, j’avais fait le tour d’à peu près tous sans jamais trouver sneakers à mon pied.

Et puis pour les besoins d’un projet spécial, l’équipe que j’ai rejoint m’a (en réalité ré)introduit à [Riot.im](https://about.riot.im/). Un outil d’échange dont les caractéristiques et la ligne éditoriale s’oriente pile dans le sens que je cherchais : option chiffrement, implémentation d’outils externes bien foutue, app mobile parfaite, design séduisant…

### Trouver le bon outil pour discuter et s’organiser

Whatsapp est atrocement moche, et si Slack est très beau, il est tout de même très loin d’être discret ou encore open source… Ne parlons même pas de Messenger (Facebook) hein. Je cherchais un outil open source, beau et pratique. Je cherchais aussi une solution capable d’être et rester fluide, d’évoluer rapidement et « flexiblement », de répondre aux enjeux numériques actuels (notamment en terme de vie privée), tout en étant connecté facilement à d’autres outils. Et bien Riot réussit mieux à tout ça que le leader Slack. Voici leur vidéo de présentation (en anglais) :

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/BoVhbX5v5kE?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>

Son usage s’oriente donc aussi bien pour celles et ceux qui bossent ensemble sur des projets communs, mais aussi pour des conversations plus quotidiennes. Il est évidemment gratuit, libre, et très bien suivi par ses développeurs. J’avais du mal à y croire, mais c’est vraiment l’alternative parfaite.

* * *

### La version desktop

Il n’y a finalement aucune différence majeure entre la version desktop à installer sur son ordi, et la version app pour smartphones. Et c’est peut-être un détail pour vous, mais pour moi ça veut dire beaucoup. La version desktop offre un client bien organisé/organisable, et une navigation super facile. Contacts, salons de discussions, fichiers, paramètres… tout est facile à trouver, tout est facile à utiliser.

On débarque sur l’accueil de Riot, qui propose un tas de salons. Vous pouvez évidemment créer les votres, ainsi que votre communauté, etc. Dans le menu latéral de gauche, seront répertoriés ceux auxquels vous participez (ou que vous avez quitté), ainsi que vos contacts.

Dans le menu en bas à gauche, la navigation perso : retourner à l’acceuil, lancer une discussions, salons, discussions, et paramètres. Quand vous êtes dans une discussion (échange entre deux personnes) ou un salon (échange à minimum trois), deux menus apparaissent ; d’abord en haut à droite : avec l’accès aux outils intégrés (de Spotify à Slack, de Github à Giphy), les paramètres de l’échange (c’est là qu’on décidera ou non de chiffrer la discussion, entre autres), un outil de recherche, puis la liste des contacts, des fichiers échangés, et des notifications ratées. Enfin, le menu en bas à droite, avec la mise en page de votre message, l’appel télé/vidéo conférence, la liste des applications utilisées dans la discussion, et un bouton pour ajouter quelqu’un dans la discussion.

C’est bien pensé, sobre et efficace : PAR-FAIT.

* * *

### Application mobile :

Là encore, une franche réussite ! Puis-qu’aucun des éléments qui font la réussite de la version desktop n’est oublié dans la version mobile. Sans non plus rogner sur leur accessibilité ou leur praticité d’usage.

On va donc retrouver la possibilité de rejoindre / créer des discussions / salons. La capacité d’envoyer et recevoir des fichiers, de chiffrer une discussion, d’organiser et prioriser ses échanges, et de rechercher du contenu.

L’application existe évidemment sur iOS et Android (Play Store et F-Droid). Gratuitement bien sûr !

Je vous recommande donc furieusement de vous y mettre, et de faire passer le mot. L’adaptabilité de [Riot.im](https://about.riot.im/), son niveau de détail qui n’entrave pas sa simplicité d’usage en font un outil vraiment plaisant et efficace. C’est quand vachement rare !

---
UPDATE : Riot s'appelle désormais Element.io !
