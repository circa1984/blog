---
layout: post  
title: le fromage fermier est-il mieux ?  
date: '2021-02-06 09:00:00'  
desc: Meilleur impact écologique, meilleur bien-être animal, meilleur goût... Le fromage fermier est-il mieux ?
---

C'est une question qui revient souvent dans mon métier du quotidien. Mes clients et clientes veulent bien souvent s'assurer de la provenance d'un produit, de quel type de structure... La célèbre légende du "petit fermier" qui fabrique son petit fromage dans un environnement on ne peut plus rustique. Un cliché de la ruralité qui a clairement son succès en milieu ultra-urbain déconnecté des réalités agricoles et paysannes.

Mais c'est aussi une question dont la dimension politique et anthropologique est assez incroyable. Dans l'histoire du fromage comme produit, ancienne et contemporaine, existe-t'il un indicateur précis et fiable susceptible de dire que la ferme est le meilleur endroit pour produire un fromage ?  
Meilleur goût, meilleur impact écologique, meilleur bien être animal, meilleures conditions de vie de l'humain qui le fabrique... Le fromage fermier est-il mieux ?

## DEJA, C'EST QUOI "FERMIER" ?

C'est un terme avec lequel on ne transige pas. Il est réglé comme une horloge, celle du droit et de la Loi. Un fromage fermier est un fromage produit avec le lait d'un seul troupeau, transformé par la même structure administrative, au même endroit.  
Autrement dit, une seule entreprise, un seul troupeau, un seul lait.  
La structuration fermière de la production de fromage a son pendant collectivisé : la coopérative, dite aussi laiterie, ou encore fruitière. Ici, les laits de différents troupeaux sont mis en commun dans une structure tierce qui s'occupe de la transformation en fromage.

le premier reflex qu'on a instinctivement en lisant ces précisions, c'est de se dire que les abus sont plus faciles dans le schéma de la laiterie que dans celui de la ferme. De quels abus parle-t'on ? Les traitements physiques ou thermiques sur le lait, l'ajout de levures ou de bactéries exogènes, l'industrialisation et la sur-mécanisation de la production, la courbe des prix du lait à la défaveur de ses producteurs et productrices ?  
Est-ce si réel que cela ?

Est-ce que finalement, ce ne serait pas une question d'échelle plutôt qu'une question administrative ?  
Un fromage fermier, avec la définition donnée ci-dessus, pourrait très bien être produit dans une méga-ferme de 50 000 bêtes, la traite pourrait y être entièrement mécanisée, de même que le stockage et toutes les étapes de la fabrication du fromage, dans un grand hangar bien équipé et rutilant. Et celui-ci serait bel et bien fermier non ?

## FROMAGE ET VERTU

L'appelation fermière est donc strictement adminitrative. Elle communique une image, un cliché, celui d'une ruralté authentique, rustique, ancestrale, mais elle ne garantit rien de tout cela. De même que ce folklore marketing ne garantit rien de nécessairement "bon" ou "mieux".

La vertu d'une fabrication fromagère repose avant tout sur les humains. C'est une réalité intangible qui dépasse les aspects administratifs, et qui dépassent aussi, avec davantage de difficultés peut-être, les échelles.  
C'est précisement une histoire humaine qui fait qu'un modèle choisi quelque part sera meilleur que son homologue fermier ou laitier, ailleurs. Dans un écosystème harmonieux, où toutes les parties rassemblées par l'action de fabriquer du fromage sont traitées de la meilleure façon possible, le facteur décideur est éminament humain *in fine*.

Si un fromage fabriqué à la ferme l'est alors que les animaux ne sont pas bien traités, est-il vraiment *mieux* ? Personellement, je n'en suis pas certain hein.

Dépasser cette étiquette purement cliché permet aussi de se rendre compte des vertus potentielles d'un fromage de coopérative / laiterie. L'exemple du Comté est parfait : il ne peut être fermier, est donc forcément le fruit d'une coopération, qui s'organise autour de la protection des dynamiques locales (le lait n'est récolté que dans une zone très limitée autour de la coopérative dite *fruitière*), et dans l'objectif de le valoriser au mieux.
Historiquement, le fromage Comté permettait aux populations de créer un aliment riche, mise en commun, pour passer l'hiver. N'est-ce pas une vision au top de ce que peuvent être des coopératives / laiteries ?

### Conclusion ?

L'appelation "fermière" ne permet pas selon de déterminer strictement si un produit est mieux qu'un autre. Le facteur humain est trop grand dans ce cas précis.
De la même façon, les AOP sont trop différentes de l'une à l'autre pour émettre la conclusion générale de leur qualité *sine qua non*.
Enfin, la certification HVE, Haute Valeur Environementale n'est clairement pas assez rigoureuse pour que je puisse (mais chacun.e fait ce qu'il veut hein...) être certain qu'elle serve à quelque chose d'autre que du marketing vert.
Comment faire alors, lorsqu'on cherche à consommer du *mieux* pour s'y reconnaitre ? Je n'ai pas vraiment de méthode infaillible à partager. Mais en fonction des produits, on peut vérifier différentes choses. Pour le fromage, je vérifie trois points :
- lait cru, parce que fabriquer un fromage au lait cru demande une exigeance plus haute sur quasiment tout le process de production que n'en demande un fromage au lait standardisé (thermisé / pasteurisé).
- de saison, notamment avec les brebis et chèvres, qui ont des rythmes de reproduction précis, et dont le respect indique une orientation positive de l'humain vis à vis de son troupeau.
- la politique générale de la boutique : chercher à comprendre, sur le moyen terme, comment réflechit et fonctionne les gens chez qui j'achète ces produits, pour savoir si la valorisation du *mieux* est aussi une de leur prérogatives.

Il n'y a aucune fin à cet article. Merci !
