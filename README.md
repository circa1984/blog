Minimal-Jekyll | grid template for Jekyll blogs
===============================================

Jekyll theme ideal for projects; displays posts in a grid. Based on [jekyll-grid](https://github.com/femmebot/jekyll-grid)

---
Requires jekyll-sitemap:
```
gem install jekyll-sitemap
```

---
