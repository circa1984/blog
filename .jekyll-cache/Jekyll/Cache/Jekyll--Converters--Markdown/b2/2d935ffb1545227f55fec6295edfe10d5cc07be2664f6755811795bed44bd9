I"}<p>L’abstentionnisme est souvent ce chiffre dont on ne sait pas quoi dire (à commencer par les présentateurs des émissions politiques) le soir des élections. Un chiffre qu’on prend apparemment en compte, qu’on recense, mais qui pourtant n’est jamais inclus dans quelconque réflexion de la part des dirigeants ou des médias.</p>

<p>On pourrait donc presque croire qu’il représente une partie déconnectée, non opérative, molle, morte. Rares sont les questionnements sur sa nature, encore plus rares celles qui abordent sa complexité.</p>

<p>Je fais parti de cette masse. Pourtant je ne me sens ni déconnecté, ni incapable, ni mou, et encore moins mort.</p>

<hr />

<h3 id="je-nai-jamais-voté">Je n’ai jamais voté</h3>

<p>Jamais. J’ai toujours su expliquer pourquoi mais les arguments ont évolué petit à petit. De ceux d’un post-ado très en colère à ceux d’un adulte un peu plus en paix avec sa réflexion. Et si les turbulences se sont un peu estompées, la radicalité de mon engagement à ne pas voter, elle, ne fait que se renforcer.</p>

<p>C’est par analyse structurelle (sans doute inconsciente à l’époque, bien consciente aujourd’hui) de l’organisation de la vie politique et décisionnaire que s’abstenir m’a paru être la solution la plus logique d’abord, et la plus digne maintenant.</p>

<p>Je ne comprenais pas à l’époque comment le système de vote pouvait être ainsi construit de manière à donner le droit de vote à un étudiant paumé qui vit chez ses parents et touche des bourses et non à un travailleur immigré qui turbine tous les jours… L’injustice.</p>

<p>Je ne comprends pas aujourd’hui comment on peut encore croire à la providence (candidats) ou tomber dans le chantage (vote utile), après autant d’années de mensonge systémique. Je comprends encore moins dès lors qu’on creuse au delà de celles et ceux qui sont en première ligne (toujours les candidats) et qu’on se rend compte du fonctionnement de tout cela. A ce titre, ce documentaire résume bien les choses :</p>

<figure class="kg-card kg-embed-card"><iframe width="480" height="270" src="https://www.youtube.com/embed/uzcN-0Bq1cw?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></figure>
<hr />

<h3 id="un-problème-de-santé-publique">Un problème de santé publique</h3>

<p>Avec les élections présidentielles actuelles arrivant, je pensais avoir, comme chaque fois, un sursaut d’interrogations : peut-être que cette fois oui ? Peut-être que par ce moyen oui ?</p>

<p>J’étais notamment tombé sur le site de <a href="https://laprimaire.org/">LaPrimaire.org</a>. L’initiative et les modalités m’ont plu et attiré. Je m’y reconnaissais un peu, jusqu’à ce que je me rappelle la synthèse de mon abstention : éviter de participer au cirque des fous.</p>

<p>Parce que vraiment, c’est un cirque, rempli de folie au sens médical du terme. La schizophrénie absolue des candidats, la frénésie des partisans, l’amnésie des commentateurs, et ce sentiment de perdition terrible dans les foules. Et j’en passe, les symptômes ne manquent pas… De l’extérieur, à regarder comme on lirait La Ferme des Animaux, c’est non seulement extraordinaire mais aussi férocement malsain.</p>

<p>Et nous traitons cela comme une normalité admise, digérée, logique, devenue rationnelle et validée par le geste du vote. Et surtout, il s’agit (s’agirait !) d’une réalité dont on ne peut se défaire : il n’y aurait pas d’alternative, ou bien elle serait vite taxée soit d’utopie, soit d’antidémocratique. Sinon les deux. Sans jamais oser appeler un chat un chat, comme l’ont fait pourtant si bien quelques auteurs avant nous :</p>

<blockquote>
  <p>« <em>Je suis profondément convaincu que le vrai fascisme est ce que les sociologues ont trop gentiment nommé la société de consommation, définition qui paraît inoffensive et purement indicative. Il n’en est rien. Si l’on observe bien la réalité, et surtout si l’on sait lire dans les objets, le paysage, l’urbanisme et surtout les hommes, on voit que les résultats de cette insouciante société de consommation sont eux-mêmes les résultats d’une dictature, d’un fascisme pur et simple.</em> » – <strong>Pier Paolo Pasolini</strong></p>
</blockquote>

<blockquote>
  <p>« <em>L’attente du Messie et le culte du génie […] sont seulement […] une misérable couverture d’impuissance. La révolution se révèlera terrible, mais anonyme.</em> » – <strong>Amadeo Bordiga</strong></p>
</blockquote>

<blockquote>
  <p>« <em>You will not be able to stay home, brother.</em><br />
<em>You will not be able to plug in, turn on and cop out.</em><br />
<em>You will not be able to lose yourself on skag and</em><br />
<em>Skip out for beer during commercials,</em><br />
<em>Because the revolution will not be televised</em>. » – <strong>Gil Scott Heron</strong></p>
</blockquote>

<h3 id="action--réaction">Action / Réaction</h3>

<p>Cela étant dit, il m’est difficile de baisser les bras. Je n’y parviens pas, quand bien même j’y trouverais sans doute d’avantage de quiétude… Je me rappelle trop souvent ces citations, qui une fois connectées entre elles, me donnent envie de participer au changement. Parce que ce n’est pas une simple alternance, ou un remodelage sous la tutelle d’un ancien « maître » (6ème République…) qu’il faut voir naître. Mais bien un changement, dans toute la radicalité que la définition du mot implique.</p>

<p>Et je crois que malheureusement une grande partie d’entre nous vit encore dans un confort suffisamment anesthésiant pour qu’un réel engouement au changement profond se fasse à grande échelle. Cette drogue dure, le confort, a encore une trop grande empreinte sur nous. Ce changement là, il ne pourra donc à mon sens venir que de celles et ceux dont le confort est le plus réduit, le plus restreint, et dont l’effet sédatif n’est plus aussi prégnant. D’elles et d’eux viendront les premières secousses, et ce sera sans doute à nous de suivre et de poursuivre ensuite…</p>

<p>Mais d’ici à ce que cela se fasse, et vu le peu de temps que nous avons devant nous (urgences écologiques, urgences humanitaires), nous avons en revanche le devoir de commencer le développement des solutions d’avenir, avec les ingrédients que nous jugeons impératifs : il s’agit du legs que nous laisserons aux suivants, rien de moins. Et pour ma part, si vous avez lu mes précédents articles, j’ai commencé (ou recommencé) à choisir mes actions, à échelle humaine. Parce que comme le dit tonton Karl :</p>

<blockquote>
  <p>“<em>C’est dans la pratique qu’il faut que l’homme prouve la vérité.</em>” – <strong>Karl Marx</strong></p>
</blockquote>

<p>Vos initiatives personnelles ou collectives sont évidemment bienvenues en commentaire !</p>
:ET